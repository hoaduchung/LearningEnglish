package com.eup.learningenglish.chathead.chat;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.chatroom.ChatRoomActivity;
import com.eup.learningenglish.activity.fisrtlogin.adapter.MyDividerItemDecoration;
import com.eup.learningenglish.adapter.ConversationAdapter;
import com.eup.learningenglish.adapter.MyAdapter;
import com.eup.learningenglish.chathead.ui.ChatHead;
import com.eup.learningenglish.chathead.ui.ChatHeadManager;
import com.eup.learningenglish.chathead.ui.ChatHeadViewAdapter;
import com.eup.learningenglish.chathead.ui.MaximizedArrangement;
import com.eup.learningenglish.chathead.ui.MinimizedArrangement;
import com.eup.learningenglish.chathead.ui.container.DefaultChatHeadManager;
import com.eup.learningenglish.chathead.ui.container.WindowManagerContainer;
import com.eup.learningenglish.model.chat.MyConversation;
import com.eup.learningenglish.model.chat.MyMessage;
import com.eup.learningenglish.prefence.MPrefence;
import com.flipkart.circularImageView.CircularDrawable;
import com.flipkart.circularImageView.TextDrawer;
import com.flipkart.circularImageView.notification.CircularNotificationDrawer;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class ChatHeadService extends Service {

    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();
    private DefaultChatHeadManager<String> chatHeadManager;
    private int chatHeadIdentifier = 0;
    private WindowManagerContainer windowManagerContainer;
    private Map<String, View> viewCache = new HashMap<>();
    private FrameLayout frameLayout;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private DatabaseReference database;
    private MPrefence mPrefence;
    private ConversationAdapter conversationAdapter;
    private List<String> arrIdConversation = new ArrayList<>();
    private boolean checkFirst= true;
    private String keyDevice="";
    private String checkKey="";

    private String key_conversation, url;
    private EmojIconActions emojIconActions;
    private ConstraintLayout constraintLayout;
    private EmojiconEditText emojiconEditText;
    private ImageView imageButton;
    private ImageView emojiButton;
    private RecyclerView recyclerViewConver;
    private List<MyMessage> arrData;
    private LinearLayoutManager linearLayoutManager;
    private MyAdapter myAdapter;
    private String android_id;
    private boolean checkExit;
    public static OnAddChatHead onAddChatHead;
    private List<MyConversation> arrConver = new ArrayList<>();
    private String key_conver="";
    private String keyChat= "";
    private List<String> arrKey = new ArrayList<>();
    private List<String> arrIdDeviceY = new ArrayList<>();
    private List<String> arrImage = new ArrayList<>();



    public int getIndex(){
        return chatHeadIdentifier;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("test", "Received start id " + startId + ": " + intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return Service.START_REDELIVER_INTENT;
    }
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
    @Override
    public void onCreate() {
        super.onCreate();

        mPrefence = new MPrefence(getBaseContext(),MPrefence.NAME_PREF_MAIN);

        windowManagerContainer = new WindowManagerContainer(this);
        chatHeadManager = new DefaultChatHeadManager<String>(this, windowManagerContainer);
        chatHeadManager.setViewAdapter(new ChatHeadViewAdapter<String>() {
            @Override
            public View attachView(final String key, final ChatHead chatHead, final ViewGroup parent) {
                database = FirebaseDatabase.getInstance().getReference();
                View cachedView = viewCache.get(key);
                checkExit = false;
                Log.e("test","cachedView "  + key+ "sss"+ chatHeadIdentifier);
//                if (cachedView == null) {
                    if(Integer.valueOf(key)==1){
                        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                        View view = inflater.inflate(R.layout.fragment_chathead, parent, false);
                        cachedView = view;
                        viewCache.put(key, view);
                        recyclerView  =  view.findViewById(R.id.recyclerview);
                        recyclerView.setHasFixedSize(true);
                        // use a linear layout manager
                        mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(mLayoutManager);
                        OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
                        recyclerView.addItemDecoration(new MyDividerItemDecoration(getBaseContext(), DividerItemDecoration.VERTICAL, 36));
                        //lấy dữ liệu từ trước
                        arrConver.clear();
                        arrConver = new Gson().fromJson(mPrefence.getStringWithName(MPrefence.my_conversation, "")
                                , new TypeToken<List<MyConversation>>() {
                                }.getType());
                        conversationAdapter = new ConversationAdapter(arrConver,getBaseContext(),null,false);
                        recyclerView.setAdapter(conversationAdapter);
                        conversationAdapter.notifyDataSetChanged();
                    }
                    else {
                        Log.e("test","url :"+url+ "  "+key_conversation);
                        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                        final View view = inflater.inflate(R.layout.activity_conversation, parent, false);
                        cachedView = view;
                        viewCache.put(key, view);

                        arrData = new ArrayList<>();
                        recyclerView=view.findViewById(R.id.recyclerView);
                        imageButton=view.findViewById(R.id.imgSend);
                        emojiconEditText=view.findViewById(R.id.edtInput);
                        emojiButton = view.findViewById(R.id.ivIcon);
                        constraintLayout=view.findViewById(R.id.conTraint);

                        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        Log.e("test","id ="+arrIdDeviceY.get(Integer.valueOf(key)-1));
                        myAdapter = new MyAdapter(arrData,arrIdDeviceY.get(Integer.valueOf(key)-1),arrImage.get(Integer.valueOf(key)-1),getBaseContext());
                        recyclerView.setAdapter(myAdapter);
                        emojIconActions = new EmojIconActions(getApplicationContext(),constraintLayout, emojiconEditText, emojiButton);
                        emojIconActions.ShowEmojIcon();

                        // get ID Device
                        android_id="\""+ MPrefence.getIDDevice(getBaseContext())+"\"";
                        database.child("MessageChatBoxss").child(arrKey.get(Integer.valueOf(key)-1)).addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                    if(!keyChat.equals(dataSnapshot.getKey())){
                                        MyMessage myMessage = new Gson().fromJson(dataSnapshot.getValue().toString(),MyMessage.class);
                                        arrData.add(myMessage);
                                        myAdapter.notifyDataSetChanged();
                                        if(arrData.size()!=0){
                                            recyclerView.smoothScrollToPosition(arrData.size()-1);
                                            recyclerView.scrollToPosition(arrData.size()-1);
                                        }
                                        keyChat = dataSnapshot.getKey();
                                    }
                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                        imageButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String message="\""+emojiconEditText.getText().toString()+"\"";
                                if(!message.equals("\"\"")){
                                    Calendar c = Calendar.getInstance();
                                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd-MM-yyyy");
                                    String strDate = "\""+sdf.format(c.getTime())+"\"";
                                    MyMessage myMessage = new MyMessage(message,android_id,strDate);
                                    database.child("MessageChatBoxss").child(arrKey.get(Integer.valueOf(key)-1)).push().setValue(myMessage);
                                    emojiconEditText.setText("");
                                }

                            }
                        });

                    }


//                }

                onAddChatHead = new OnAddChatHead() {
                    @Override
                    public void getAddChatHead(String key, String urlImage,String idDeviceY) {
                        final List<String> arrConOpenning=new ArrayList<>();
                        List<String> test = new Gson().fromJson(mPrefence.getStringWithName(MPrefence.conversationOpenning,"") ,new TypeToken<List<String>>() {
                        }.getType());
                        arrConOpenning.clear();
                        if(test!= null)
                            arrConOpenning.addAll(test);

                        boolean checkOpened = false;
                        int index = 0;
                        for(int i=0;i<arrConOpenning.size();i++){
                            if(arrConOpenning.get(i).equals(key)){
                                checkOpened = true;
                                index = i+2;
                            }
                        }
                        // chưa mở
                        if(checkOpened == false){
                            arrConOpenning.add(key);
                            mPrefence.setStringWithName(MPrefence.conversationOpenning,new Gson().toJson(arrConOpenning));
                            addChatHead(key,urlImage,idDeviceY);
//                            toggleArrangement();
                        }
                        else {
                            selectChatHead(index);
                        }
                    }
                };


                parent.addView(cachedView);
                return cachedView;
            }

            @Override
            public void detachView(String key, ChatHead<? extends Serializable> chatHead, ViewGroup parent) {
                View cachedView = viewCache.get(key);
                checkExit = false;
                Log.e("test","detachView " );
                if(cachedView!=null) {
//                    viewCache.remove(key);
                    parent.removeView(cachedView);
                }
            }

            @Override
            public void removeView(String key, ChatHead<? extends Serializable> chatHead, ViewGroup parent) {
                Log.e("test","removeView "+key );
                checkExit = true;
                View cachedView = viewCache.get(key);
                if(cachedView!=null) {
                    viewCache.remove(key);
                    parent.removeView(cachedView);
                    // thay thế id remove bằng ""
                    final List<String> arrConOpenning=new ArrayList<>();
                    List<String> test = new Gson().fromJson(mPrefence.getStringWithName(MPrefence.conversationOpenning,"") ,new TypeToken<List<String>>() {
                    }.getType());
                    arrConOpenning.clear();
                    if(test!= null) arrConOpenning.addAll(test);
                    if(!key.equals("1")){
                        arrConOpenning.set(Integer.valueOf(key)-2,"");
                        mPrefence.setStringWithName(MPrefence.conversationOpenning,new Gson().toJson(arrConOpenning));
                    }

                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(checkExit){
                            removeAllChatHeads();
                            arrConver.clear();
                            mPrefence.setStringWithName(MPrefence.conversationOpenning,"");
                            arrKey.clear();
                            arrIdDeviceY.clear();
                            arrImage.clear();
                        }
                    }
                },200);
            }

            @Override
            public Drawable getChatHeadDrawable(String key) {
                Log.e("test","getChatHeadDrawable " );

                return ChatHeadService.this.getChatHeadDrawable(key);
            }
        });

//        addChatHead();
        chatHeadManager.setArrangement(MinimizedArrangement.class, null);
        moveToForeground();

    }

    private Drawable getChatHeadDrawable(String key) {
        Random rnd = new Random();
        int randomColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        CircularDrawable circularDrawable = new CircularDrawable();
        circularDrawable.setBitmapOrTextOrIcon(new TextDrawer().setText("C" + key).setBackgroundColor(randomColor));
        int badgeCount = (int) (Math.random() * 10f);
        circularDrawable.setNotificationDrawer(new CircularNotificationDrawer().setNotificationText(String.valueOf(badgeCount)).setNotificationAngle(135).setNotificationColor(Color.WHITE, Color.RED));
        circularDrawable.setBorder(Color.WHITE, 3);
        return circularDrawable;

    }

    private void moveToForeground() {
        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification_template_icon_bg)
                .setContentTitle("Springy heads")
                .setContentText("Click to configure.")
                .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, ChatRoomActivity.class), 0))
                .build();

        startForeground(1, notification);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent){
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);
    }

    public void addChatHead(String key,String urlImage, String idDeviceY) {
        arrIdDeviceY.add(idDeviceY);
        arrKey.add(key);
        arrImage.add(urlImage);
        Log.e("test","addChatHead " +key+" size id :"+arrKey.size()+" size image :"+arrImage.size()+" size idY :"+arrIdDeviceY.size());
        key_conversation = key;
        url = urlImage;
        chatHeadIdentifier++;
        chatHeadManager.addChatHead(String.valueOf(chatHeadIdentifier), false, true,chatHeadIdentifier,urlImage);
        chatHeadManager.bringToFront(chatHeadManager.findChatHeadByKey(String.valueOf(chatHeadIdentifier)));
    }

    public void removeChatHead() {
        Log.e("test","removeChatHead " );

        chatHeadManager.removeChatHead(String.valueOf(chatHeadIdentifier), true);
        chatHeadIdentifier--;
    }

    public void selectChatHead(int key){
        chatHeadManager.selectChatHead(String.valueOf(key));
    }

    public void removeAllChatHeads() {
        Log.e("test","removeAllChatHeads " );

        chatHeadIdentifier = 0;
        chatHeadManager.removeAllChatHeads(true);
    }

    public void toggleArrangement() {
        Log.e("test","toggleArrangement " );

        if (chatHeadManager.getActiveArrangement() instanceof MinimizedArrangement) {
            chatHeadManager.setArrangement(MaximizedArrangement.class, null);
        } else {
            chatHeadManager.setArrangement(MinimizedArrangement.class, null);
        }
    }

    public void updateBadgeCount() {
        Log.e("test","updateBadgeCount " );

//        chatHeadManager.reloadDrawable(String.valueOf(chatHeadIdentifier),chatHeadIdentifier);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        windowManagerContainer.destroy();
    }

    public void minimize() {
        chatHeadManager.setArrangement(MinimizedArrangement.class,null);
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public ChatHeadService getService() {
            // Return this instance of LocalService so clients can call public methods
            return ChatHeadService.this;
        }
    }
}