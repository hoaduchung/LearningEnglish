package com.eup.learningenglish.firebase.module;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {
    public String linkImg;
    public String username;
    public Integer userId;
    public int score;

    // Default constructor required for calls to
    // DataSnapshot.getValue(UserChat.class)
    public User() {
    }

    public User(String username, Integer userId, String linkImg, int score) {
        this.username = username;
        this.userId = userId;
        this.linkImg = linkImg;
        this.score = score;
    }
}