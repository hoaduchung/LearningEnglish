package com.eup.learningenglish.firebase.module;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;

/**
 * Created by linh on 2017/09/01.
 */

@IgnoreExtraProperties
public class GameState {
    public static final String playing = "playing";
    public static final String waiting = "waiting";
    public static final String end = "end";
    public static final String cancelled = "cancelled";

    public List<Integer> answers;
    public Integer currentQuestion;
    public String state;
    public String code;
    public Long created;
    public String name;
    public Integer numberQuestion;
    public String owner;
    public Integer ownerId;

    // Default constructor required for calls to
    // DataSnapshot.getValue(UserChat.class)
    public GameState() {
    }

    public GameState(List<Integer> answers, Integer currentQuestion, String state, String code, String name,
                     Integer numberQuestion, String owner, Integer ownerId) {
        this.answers = answers;
        this.currentQuestion = currentQuestion;
        this.state = state;
        this.code = code;
        this.created = System.currentTimeMillis();
        this.name = name;
        this.numberQuestion = numberQuestion;
        this.owner = owner;
        this.ownerId = ownerId;
    }
}