package com.eup.learningenglish.firebase.listener;

/**
 * Created by linh on 2017/09/02.
 */

public interface OnCreateCodeListener {
    void success(String code, String id);
}
