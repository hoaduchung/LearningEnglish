package com.eup.learningenglish.firebase.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.firebase.module.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {
    private List<User> userList;
    private int userId;
    private Activity activity;
    private List<Integer> rank;

    public DashboardAdapter(List<User> setsHintList, int userId, Activity activity, List<Integer> rank) {
        this.userList = setsHintList;
        this.activity = activity;
        this.userId = userId;
        this.rank = rank;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_arena_user_result, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if (position < userList.size()) {
            User user = userList.get(position);

            if (userId == user.userId) {

                holder.tvUserName.setTypeface(Typeface.DEFAULT_BOLD);

            } else {

                holder.tvUserName.setTypeface(Typeface.DEFAULT);
            }

//            if (user.linkImg != null) {
//                // set avatar
//                PicassoUtil.loadLinkImg(activity, user.linkImg, holder.ivAvatar);
//            } else {
//                PicassoUtil.loadLinkImg(activity, "", holder.ivAvatar);
//            }

            String score = activity.getString(R.string.arena_score);

            holder.tvScore.setText(user.score + " " + score);

            //set username
            holder.tvUserName.setText(user.username);

            // hien thi anh huy chuong
            if (rank.size() > 0 && user.score == rank.get(0)) {

                holder.ivMedal.setImageResource(R.drawable.ic_trophy_flat);
                holder.ivMedal.setVisibility(View.VISIBLE);
                holder.tvUserName.setTextColor(activity.getResources().getColor(R.color.colorCorrect));

            } else if (rank.size() > 1 && user.score == rank.get(1) && user.score != 0) {

                holder.ivMedal.setImageResource(R.drawable.ic_medal_2);
                holder.ivMedal.setVisibility(View.VISIBLE);
                holder.tvUserName.setTextColor(activity.getResources().getColor(R.color.colorAccent));

            } else if (rank.size() > 2 && user.score == rank.get(2) && user.score != 0) {

                holder.ivMedal.setImageResource(R.drawable.ic_medal_3);
                holder.ivMedal.setVisibility(View.VISIBLE);
                holder.tvUserName.setTextColor(activity.getResources().getColor(R.color.colorError));

            } else {
                holder.tvUserName.setTextColor(activity.getResources().getColor(R.color.colorTextLv1));
//                    holder.tvUserName.setTextSize(Utils.sp2px(activity.getResources(), 6));
                holder.ivMedal.setVisibility(View.INVISIBLE);
            }

        }

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvUserName)
        public TextView tvUserName;

        @BindView(R.id.tvScore)
        public TextView tvScore;

//        @BindView(R.id.ivAvatar)
//        public CircleImageView ivAvatar;

        @BindView(R.id.ivMedal)
        public CircleImageView ivMedal;

        public MyViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }
    }

}