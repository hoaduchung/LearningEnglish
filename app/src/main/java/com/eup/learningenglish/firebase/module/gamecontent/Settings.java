package com.eup.learningenglish.firebase.module.gamecontent;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by linh on 2017/09/01.
 */
@IgnoreExtraProperties
public class Settings {

    public boolean choice;
    public boolean enableAudio;
    public boolean enableFeedback;
    public boolean enableTerm;
    public boolean truefalse;
    public boolean write;
    public Integer numberQuestion;

    public Settings() {
    }

    public Settings(Boolean choice, Boolean enableAudio, Boolean enableFeedback, Boolean enableTerm, Boolean truefalse, Boolean write, Integer numberQuestion) {
        this.choice = choice;
        this.enableAudio = enableAudio;
        this.enableFeedback = enableFeedback;
        this.enableTerm = enableTerm;
        this.truefalse = truefalse;
        this.write = write;
        this.numberQuestion = numberQuestion;
    }
}
