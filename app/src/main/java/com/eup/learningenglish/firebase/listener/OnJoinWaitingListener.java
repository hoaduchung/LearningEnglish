package com.eup.learningenglish.firebase.listener;

/**
 * Created by linh on 2017/09/11.
 */

public interface OnJoinWaitingListener {
    void join(String code);
}
