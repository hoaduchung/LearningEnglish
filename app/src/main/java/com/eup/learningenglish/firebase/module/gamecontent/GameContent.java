package com.eup.learningenglish.firebase.module.gamecontent;

import com.eup.learningenglish.firebase.util.TimeAgoUtil;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;


/**
 * Created by linh on 2017/09/01.
 */
@IgnoreExtraProperties
public class GameContent {
    public String code;
    public String created;
    public String id;
    public String name;
    public Integer numberQuestion;
    public List<Question> questions;
    public Settings settings;

    public GameContent() {
        this.created = TimeAgoUtil.getCurrentDate();
    }

    public GameContent(String code, String created, String id, String name, Integer numberQuestion, List<Question> questions, Settings settings) {
        this.code = code;
        this.created = TimeAgoUtil.getCurrentDate();
        this.id = id;
        this.name = name;
        this.numberQuestion = numberQuestion;
        this.questions = questions;
        this.settings = settings;
    }
}
