package com.eup.learningenglish.firebase.viewholder;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.firebase.module.User;


public class UserViewHolder extends RecyclerView.ViewHolder {

    public TextView tvUserName;
//    public CircleImageView ivAvatar;

    public UserViewHolder(View itemView) {
        super(itemView);

        tvUserName = (TextView) itemView.findViewById(R.id.tvUserName);
//        ivAvatar = (CircleImageView) itemView.findViewById(R.id.ivAvatar);
    }

    public void bindToPost(User user, int userId, Activity activity) {
        if (userId == user.userId) {

            tvUserName.setTypeface(Typeface.DEFAULT_BOLD);

        } else {

            tvUserName.setTypeface(Typeface.DEFAULT);
        }

//        if (user.linkImg != null) {
//            BaseAppCompatActivity baseAppCompatActivity = (BaseAppCompatActivity) activity;
//
//            // set avatar
//            PicassoUtil.loadLinkImg(baseAppCompatActivity, user.linkImg, ivAvatar);
//        }

        //set username
        tvUserName.setText(user.username);

    }
}
