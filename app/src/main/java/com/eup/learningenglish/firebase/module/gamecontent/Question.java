package com.eup.learningenglish.firebase.module.gamecontent;

import com.eup.learningenglish.model.MWord;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by linh on 2017/09/01.
 */
@IgnoreExtraProperties
public class Question {
    // cac kieu cau hoi
    public static final String choice = "choice";
    public static final String write = "write";
    public static final String match = "match";

    public Integer id;
    public String quest;
    public List<String> answer;
    public String result;
    public String type;

    public Question(Integer id, String quest, List<String> answer, String result, String type) {
        this.id = id;
        this.quest = quest;
        this.answer = answer;
        this.result = result;
        this.type = type;
    }

    public Question() {
    }

    public Question(MWord item, List<MWord> listItem, String type) {
        this.id = item.getId();
        this.quest = item.getMean();
        this.result = item.getWord();
        this.type = type;

        // lay list 3 dap an nhieu
        List<String> listResult = getRandomListOther(listItem, item);
        // them dap an dung
        listResult.add(item.getWord());
        // tron
        Collections.shuffle(listResult);

        this.answer = listResult;
    }

    /**
     * lay ra 3 tu khac de lam dap an khac
     *
     * @return
     */
    private List<String> getRandomListOther(List<MWord> list, MWord itemTerm) {
        List<MWord> items = new ArrayList<>();
        List<String> listResult = new ArrayList<>();

        while (list.size() > 3 ? items.size() < 3 : items.size() < list.size() - 1) { // neu chua du 3 phan tu van tim tiep
            int random = new Random().nextInt(list.size());

            // kiem tra neu tu tim duoc khac tu dang hien
            if (list.get(random).getId() != itemTerm.getId()) {
                // kiem tra xem da ton tai trong list duoc chon chua
                boolean isMatch = false;

                for (MWord item : items) {
                    if (item.getId() == list.get(random).getId()) { // neu trung id
                        // thi gan la co ton tai roi
                        isMatch = true;

                        break;
                    }
                }

                if (!isMatch) {
                    items.add(list.get(random));
                    listResult.add(list.get(random).getWord());
                }
            }
        }

        return listResult;
    }
}
