package com.eup.learningenglish.firebase.util;

import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by linh on 2017/08/09.
 */

public abstract class TimeAgoUtil {
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final int WEEK_MILLIS = 7 * DAY_MILLIS;
    private static final int MONTH_MILLIS = 4 * WEEK_MILLIS;
    private static final int YEAR_MILLIS = 12 * MONTH_MILLIS;

    public static String getTimeAgo(String date) {
        String str = "Vừa xong";

        if (date == null) return str;

        try {
            long ts = System.currentTimeMillis();
            Date localTime = new Date(ts);
            String format = "yyyy/MM/dd HH:mm:ss";
            SimpleDateFormat sdf = new SimpleDateFormat(format);

            // Convert Local Time to UTC (Works Fine)
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date gmtTime = new Date(sdf.format(localTime));

            long now = gmtTime.getTime();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date convertedDate = dateFormat.parse(date);

            long diff = now - convertedDate.getTime();

            long status;

            if (diff < MINUTE_MILLIS) {
                return str;
            } else if (diff < 2 * MINUTE_MILLIS) {
                return "Một phút trước";
            } else if (diff < 50 * MINUTE_MILLIS) {
                return "Vài phút trước";
            } else if (diff < 90 * MINUTE_MILLIS) {
                return "Một giờ trước";
            } else if (diff < 5 * HOUR_MILLIS) {
                return "Vài giờ trước";
            } else if (diff < 24 * HOUR_MILLIS) {
                return "Hôm nay";
            } else if (diff < 48 * HOUR_MILLIS) {
                return "Hôm qua";
            } else if (diff < WEEK_MILLIS) {
                status = DateUtils.DAY_IN_MILLIS;
            } else if (diff < MONTH_MILLIS) {
                status = DateUtils.WEEK_IN_MILLIS;
            } else if (diff < YEAR_MILLIS) {
                return "Năm nay";
            } else if (diff < 2 * YEAR_MILLIS) {
                return "Năm ngoái";
            } else {
                return "Vài năm trước";
            }

            CharSequence relavetime1 = DateUtils.getRelativeTimeSpanString(
                    convertedDate.getTime(),
                    now, status
            );

            str = String.valueOf(relavetime1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return str;
    }

    public static String getCurrentDate() {

        long ts = System.currentTimeMillis();
        Date localTime = new Date(ts);
        String format = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        // Convert Local Time to UTC (Works Fine)
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        return sdf.format(localTime);
    }

    public static int getCurrentId() {
        String idStr = String.valueOf(System.currentTimeMillis());

        return Integer.parseInt(idStr.substring(4));
    }
}
