package com.eup.learningenglish.firebase.controller;

import android.util.Log;

import com.eup.learningenglish.firebase.listener.OnCreateCodeListener;
import com.eup.learningenglish.firebase.listener.OnGameCodeListener;
import com.eup.learningenglish.firebase.listener.OnGameStateListener;
import com.eup.learningenglish.firebase.listener.OnLoadGameContentListener;
import com.eup.learningenglish.firebase.listener.OnLoadGameMemberListener;
import com.eup.learningenglish.firebase.module.GameState;
import com.eup.learningenglish.firebase.module.User;
import com.eup.learningenglish.firebase.module.gamecontent.GameContent;
import com.eup.learningenglish.firebase.module.gamecontent.Question;
import com.eup.learningenglish.firebase.module.gamecontent.Settings;
import com.eup.learningenglish.model.MWord;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import java.util.List;
import java.util.Random;



/**
 * Created by linh on 2017/09/01.
 */

public class FirebaseUtil {
    public static final String game_code = "game_code";
    public static final String game_content = "game_content";
    public static final String game_member = "game_member";
    public static final String game_state = "game_state";
    public static final String state = "state";
    public static final String answers = "answers";
    public static final String currentQuestion = "currentQuestion";
    public static final String created = "created";

    private DatabaseReference mDatabase;

    /**
     * khoi tao firebase database
     */
    public FirebaseUtil() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }


    /**
     * lay string id firebase game content theo game code
     *
     * @return
     */
    public void getGameContentKeyWithCode(final String code, final OnGameCodeListener onGameCodeListener) {
        mDatabase.child(game_code).child(code).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String id = dataSnapshot.getValue(String.class);
                Log.e("test","id :=="+id);

                onGameCodeListener.singleValueEvent(id);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * kiem tra trang thai phong theo key content
     */
    public void checkGameStateWithContentKey(final String key, final OnGameStateListener onGameStateListener) {
        mDatabase.child(game_state).child(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("test","dataSnapshot :=="+dataSnapshot);

                GameState gameState = dataSnapshot.getValue(GameState.class);

                onGameStateListener.stateChange(gameState);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    /*
    =================================================================================================
                        |       create room and question       |
    =================================================================================================
    */
    public GameContent createGameContent(List<MWord> listTerm, User userItem, String id, String code, String name) {

        // tao game content
        GameContent gameContent = new GameContent();

        gameContent.id = id; // set id game content
        gameContent.code = code; // ma phong
        gameContent.numberQuestion = listTerm.size();
        gameContent.name = name;

        // tao settings: hien tai mac dinh chi co choice
        Settings settings = new Settings();
        settings.choice = true;
        settings.numberQuestion = listTerm.size();

        // gan vao game content
        gameContent.settings = settings;

        // tao question
        List<Question> questionList = new ArrayList<>();
        for (MWord item : listTerm) {
            questionList.add(new Question(item, listTerm, Question.choice));
        }

        gameContent.questions = questionList;

        // set value tren server firebase
        mDatabase.child(game_content).child(id).setValue(gameContent);

        // clear old member
        clearGameMember(id);

        // set game member
        joinGameMember(id, userItem);

        // create game state
        GameState gameState = new GameState(new ArrayList<Integer>(), 0, GameState.waiting, code, name,
                listTerm.size(), userItem.username, userItem.userId);

        // set game state waiting
        mDatabase.child(game_state).child(id).setValue(gameState);

        return gameContent;
    }

    /**
     * tao game code khong trung voi cac cai dang play hoac waiting
     */
    public void createRandomGameCode(final OnCreateCodeListener onCreateCodeListener) {

        Random random = new Random();

        int codeInt = random.nextInt(1000000 - 1) + 1;

        String codeStr = String.valueOf(codeInt);

        if (codeStr.length() < 6) {
            String missingCode = "";

            for (int i = 0; i < 6 - codeStr.length(); i++) {
                missingCode += "0";
            }

            if (missingCode != null && !missingCode.isEmpty()) {
                codeStr = missingCode.concat(codeStr);
            }
        }

        // kiem tra xem da co ma code nay chua
        final String finalCodeStr = codeStr;

        getGameContentKeyWithCode(codeStr, new OnGameCodeListener() {
            @Override
            public void singleValueEvent(String id) {
                // neu da ton tai, kiem tra xem phong nay co con dang choi hoac dang doi khong
                if (id != null && !id.isEmpty()) {
                    // kiem tra game state
                    checkGameStateWithContentKey(id, new OnGameStateListener() {
                        @Override
                        public void stateChange(GameState gameState) {
                            // nếu code đã được sử dụng nhưng ở trạng thasi chờ hoặc đang chơi thì phải tạo mới
                            // neu ma code nay dang duoc su dung
                            if (gameState.state.equals(GameState.waiting) || gameState.state.equals(GameState.playing)) {
                                // tao lai ma code khac
                                createRandomGameCode(onCreateCodeListener);
                            } else {
                                // thành công
                                // push len de lay key cua game content
                                String idGameContent = mDatabase.child(game_content).push().getKey();
                                // push game code
                                mDatabase.child(game_code).child(finalCodeStr).setValue(idGameContent);
                                // thanh cong nhung o trong luong khac????
                                onCreateCodeListener.success(finalCodeStr, idGameContent);
                            }
                        }
                    });
                } else {
                    // push id phòng lên trong game content
                    // push len de lay key cua game content
                    String idGameContent = mDatabase.child(game_content).push().getKey();

                    // push game code
                    mDatabase.child(game_code).child(finalCodeStr).setValue(idGameContent);

                    // thanh cong nhung o trong luong khac????
                    onCreateCodeListener.success(finalCodeStr, idGameContent);
                }
            }
        });
    }

    /**
     * tai du lieu noi dung game content
     *
     * @param gameId
     */
    public void loadGameContentWithId(String gameId, final OnLoadGameContentListener onLoadGameContentListener) {
        mDatabase.child(game_content).child(gameId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GameContent gameContent = dataSnapshot.getValue(GameContent.class);

                onLoadGameContentListener.sucess(gameContent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * lay cau lenh truy van member de lay list nguoi dung theo id game content
     */
    public Query getUsersQuery(String id) {
        return mDatabase.child(game_member).child(id);
    }

    /**
     * huy phong game
     *
     * @param id
     */
    public void cancelGameState(String id) {
        mDatabase.child(game_state).child(id).child(state).setValue(GameState.cancelled);
    }

    /**
     * clear old game member
     */
    public void clearGameMember(String id) {
        mDatabase.child(game_member).child(id).removeValue();
    }

    /**
     * join vao phong
     */
    public void joinGameMember(String id, User user) {
        mDatabase.child(game_member).child(id).child(String.valueOf(user.userId)).setValue(user);
    }

    /**
     * jthoat khoi phong
     */
    public void quitGameMember(String id, User user) {
        mDatabase.child(game_member).child(id).child(String.valueOf(user.userId)).removeValue();
    }

    /**
     * bat trang thay thai doi cua game state .state
     */
    public void addOnStateChangeListener(String id, final OnGameStateListener onGameStateListener) {
        mDatabase.child(game_state).child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GameState gameState = dataSnapshot.getValue(GameState.class);

                onGameStateListener.stateChange(gameState);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * bat dau game = cach thay doi game state = play
     */
    public void startGame(String id) {
        mDatabase.child(game_state).child(id).child(state).setValue(GameState.playing);
    }

    /**
     * kiem tra xem co phai la cau tra loi som nhat khong
     */
    public void checkGameContentAsnwer(final String id, final int pos, final int userId, final int max) {
        mDatabase.child(game_state).child(id).runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                GameState gameState = mutableData.getValue(GameState.class);

                if (gameState.answers == null) gameState.answers = new ArrayList<>();

                if (pos == gameState.answers.size()) {

                    // them nguoi tra loi dung
                    gameState.answers.add(userId);

                    // neu la cau cuoi thi ket thuc
                    if (pos == max - 1) {
                        gameState.state = GameState.end;
                    } else { // khong thi next cau tiep theo
                        gameState.currentQuestion = pos + 1;
                    }

                    // set id
                    mutableData.setValue(gameState);

                }

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                // Transaction completed
//                Log.d("aaa", "postTransaction:onComplete:" + databaseError);
            }
        });
    }

    /**
     * lay ve danh sach nguoi choi
     *
     * @param mId
     */
    public void getListUser(String mId, final OnLoadGameMemberListener onLoadGameMemberListener) {
        mDatabase.child(game_member).child(mId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<User> list = new ArrayList<User>();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    User user = postSnapshot.getValue(User.class);
                    list.add(user);
                }

                onLoadGameMemberListener.success(list);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * lay danh sach phong cho
     */
    public Query getListWaitingRoom(){
        return mDatabase.child(game_state).orderByChild(state).equalTo(GameState.waiting);
    }
}
