package com.eup.learningenglish.firebase.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.login.LoginActivity;


/**
 * Created by linh on 2017/10/11.
 */

public abstract class DialogUtil {

    public static ProgressDialog showLoadingDialog(final Activity activity, final String message) {
        final ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setIndeterminate(true);
        dialog.setMessage(message);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(false); // khong the tat dialog nay duoc

        dialog.show();

        return dialog;
    }

    public static AlertDialog showAlertNeedLogin(final Activity activity, final String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(activity.getString(R.string.login_facebook), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // chuyen sang man hinh dang nhap
                        Intent intent = new Intent(activity, LoginActivity.class);
                        activity.startActivity(intent);
                    }
                })
                .setNegativeButton(activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // finish acitity
                        activity.finish();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();

        return dialog;
    }

    /************************************************************************************************************/
    public static void showAlertDialogExit(final Activity activity, String message, String positive, final AlertDialogCallBack callBack) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // chuyen sang man hinh dang nhap
                        callBack.actionDone();
                    }
                })
                .setNegativeButton(activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // finish acitity
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }


    /**
     * crete and show alert dialog confirm
     */
    public static void showAlertDialogNotifyWithCallBacks(final Activity activity, String message, String positive,
                                                          final AlertDialogCallBack callBack, boolean isCancelable) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message)
                .setCancelable(isCancelable)
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        callBack.actionDone();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    public interface AlertDialogCallBack {
        void actionDone();
    }

}
