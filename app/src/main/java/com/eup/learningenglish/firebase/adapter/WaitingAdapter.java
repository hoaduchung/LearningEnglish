package com.eup.learningenglish.firebase.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.firebase.listener.OnJoinWaitingListener;
import com.eup.learningenglish.firebase.module.GameState;

import java.util.List;




public class WaitingAdapter extends RecyclerView.Adapter<WaitingAdapter.MyViewHolder> {
    private List<GameState> waitingRooms;
    private Activity activity;
    private OnJoinWaitingListener onJoinWaitingListener;

    public WaitingAdapter(final List<GameState> waitingRooms,final Activity activity,final OnJoinWaitingListener onJoinWaitingListener) {
        this.activity = activity;
        this.waitingRooms = waitingRooms;
        this.onJoinWaitingListener = onJoinWaitingListener;
    }

    public void setWaitingRooms(List<GameState> waitingRooms) {
        this.waitingRooms = waitingRooms;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_arena_watiting_room, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if (position < waitingRooms.size()) {
            final GameState gameState = waitingRooms.get(position);

            //set username
            holder.tvUserName.setText(gameState.owner);

            // ten phong
            holder.tvRoomName.setText(gameState.name);

            // so cau hoi
            holder.tvRoomQuestion.setText(gameState.numberQuestion + "");

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onJoinWaitingListener != null)
                        onJoinWaitingListener.join(gameState.code);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return waitingRooms.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvUserName;
        public TextView tvRoomName;
        public TextView tvRoomQuestion;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvUserName =  itemView.findViewById(R.id.tvUserName);
            tvRoomName =  itemView.findViewById(R.id.tvRoomName);
            tvRoomQuestion =  itemView.findViewById(R.id.tvRoomQuestion);
        }
    }
}