package com.eup.learningenglish.firebase.listener;

/**
 * Created by linh on 2017/07/27.
 */

public interface OnTTSEnd {
    void finish();
}
