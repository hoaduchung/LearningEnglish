package com.eup.learningenglish.firebase.adapter;

import android.support.v7.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.Query;

/**
 * Created by linh on 2017/09/15.
 */

public class CustomFirebaseAdapter<T, VH extends RecyclerView.ViewHolder> extends FirebaseRecyclerAdapter {

    private Class<T> mModelClass;

    /**
     * @param modelClass      Firebase will marshall the data at a location into
     *                        an instance of a class that you provide
     * @param modelLayout     This is the layout used to represent a single item in the list.
     *                        You will be responsible for populating an instance of the corresponding
     *                        view with the data from an instance of modelClass.
     * @param viewHolderClass The class that hold references to all sub-views in an instance modelLayout.
     * @param ref             The Firebase location to watch for data changes. Can also be a slice of a location,
     *                        using some combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     */
    public CustomFirebaseAdapter(Class modelClass, int modelLayout, Class viewHolderClass, Query ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);

        mModelClass = modelClass;
    }


    @Override
    protected Object parseSnapshot(DataSnapshot snapshot) {
        Object obj  = null;

        try {
            obj = snapshot.getValue(mModelClass);
        }catch (DatabaseException e){

        }

        return obj;
    }

    @Override
    protected void populateViewHolder(RecyclerView.ViewHolder viewHolder, Object model, int position) {

    }
}
