package com.eup.learningenglish.firebase.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.firebase.module.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.MyViewHolder> {

    private List<User> userList;
    private int userId;
    private Activity activity;
    private int currentAddPointUserId = 0;

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            notifyDataSetChanged();
        }
    };

    public MemberAdapter(List<User> setsHintList, int userId, Activity activity) {
        this.userList = setsHintList;
        this.activity = activity;
        this.userId = userId;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_arena_user_choice, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if (position < userList.size()) {
            User user = userList.get(position);

            if (userId == user.userId) {

                holder.tvUserName.setTypeface(Typeface.DEFAULT_BOLD);

            } else {

                holder.tvUserName.setTypeface(Typeface.DEFAULT);
            }

//            if (user.linkImg != null) {
//                // set avatar
//                PicassoUtil.loadLinkImg(activity, user.linkImg, holder.ivAvatar);
//            } else {
//                PicassoUtil.loadLinkImg(activity, "", holder.ivAvatar);
//            }

            //set username
            holder.tvUserName.setText(user.username + ": " + user.score);

            if (currentAddPointUserId == user.userId) {
                holder.tvUserName.setTextColor(Color.parseColor("#2EB16F")); // mau xanh dung

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        holder.tvUserName.setTextColor(Color.parseColor("#3B4C51")); // mau text lv1
                    }
                }, 2000);

            } else {
                holder.tvUserName.setTextColor(Color.parseColor("#3B4C51")); // mau xanh dung
            }
        }

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    /**
     * update member
     */
    public void updateMember(List<User> list) {

        this.userList = list;

        notifyDataSetChanged();
    }

    /**
     * cong diem cho nguoi dung
     */
    public void addPointMember(int userId) {
        currentAddPointUserId = userId;

        for (User user : userList) {
            if (userId == user.userId) {
                user.score += 1;

                break;
            }
        }

        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 200);
    }

    /**
     * cong diem cho nguoi dung
     */
    public void mergePointMember(int userId) {
        int i = 0;

        for (User user : userList) {
            if (user.userId == currentAddPointUserId) {
                user.score -= 1;

                i++;
            }

            if (userId == user.userId) {
                user.score += 1;

                i++;
            }

            if (i == 2) break;
        }

        currentAddPointUserId = userId;

        handler.removeCallbacks(runnable);
        handler.post(runnable);
    }

    public List<User> getUserList() {
        return userList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvUserName)
        public TextView tvUserName;

//        @BindView(R.id.ivAvatar)
//        public CircleImageView ivAvatar;

        public MyViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }
    }
}