package com.eup.learningenglish.firebase.listener;

import com.eup.learningenglish.firebase.module.User;

import java.util.List;

/**
 * Created by linh on 2017/09/02.
 */

public interface OnLoadGameMemberListener {
    void success(List<User> list);
}
