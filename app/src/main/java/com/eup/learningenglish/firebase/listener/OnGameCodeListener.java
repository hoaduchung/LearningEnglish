package com.eup.learningenglish.firebase.listener;

/**
 * Created by linh on 2017/09/01.
 */

public interface OnGameCodeListener {
    void singleValueEvent(String id);
}
