package com.eup.learningenglish.firebase.controller;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by linh on 2017/10/11.
 */

public class FirebaseAuthUtil {
    private static final String TAG = "FIRE_BASE";


    public static void signIn(String mCustomToken, Activity activity) {
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();

        mAuth.signInWithCustomToken(mCustomToken)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCustomToken:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.d(TAG, user.getUid());
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCustomToken:failure", task.getException());
                        }
                    }
                });
    }

    public static void signOut() {
        FirebaseAuth.getInstance().signOut();
    }

    public static boolean isSigned() {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }
}
