package com.eup.learningenglish.firebase.listener;


import com.eup.learningenglish.firebase.module.GameState;

/**
 * Created by linh on 2017/09/01.
 */

public interface OnGameStateListener {
    void stateChange(GameState gameState);
}
