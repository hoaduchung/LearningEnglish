package com.eup.learningenglish.activity.fisrtlogin.adapter.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.fisrtlogin.adapter.MyDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by HoaDucHung on 2/1/2018.
 */

public class SeectLanguageAdapter extends RecyclerView.Adapter<SeectLanguageAdapter.MyView>  {

    private List<String> arrLanguage;
    private List<Integer> arrCo;
    private Activity activity;
    private LinearLayoutManager linearLayoutManager;


    public SeectLanguageAdapter(List<String> arrLanguage, List<Integer> arrCo, Activity activity) {
        this.arrLanguage=arrLanguage;
        this.activity = activity;
        this.arrCo=arrCo;
    }



    @Override
    public SeectLanguageAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_language, parent, false);
        return new SeectLanguageAdapter.MyView(itemView);
    }

    @Override
    public void onBindViewHolder(SeectLanguageAdapter.MyView holder, final int position) {
        holder.txt.setText(arrLanguage.get(position).toString());
        holder.img.setImageResource(arrCo.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    final List<String> arrLanguage= new ArrayList<String>();
                    arrLanguage.add("Bắt Đầu");
                    arrLanguage.add("Sơ Cấp");
                    arrLanguage.add("Trung Cấp");
                    arrLanguage.add("Cao Cấp");
                    List<Integer> arrImage= new ArrayList<>();
                    arrImage.add(R.drawable.ic_batdau);
                    arrImage.add(R.drawable.ic_socap);
                    arrImage.add(R.drawable.ic_trungcap);
                    arrImage.add(R.drawable.ic_caocap);

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
                    LayoutInflater inflater = activity.getLayoutInflater();
                    View convertView =  inflater.inflate(R.layout.layout_dialog_selectlanguage, null);
                    alertDialog.setView(convertView);
                    alertDialog.setTitle("Chọn trình độ");
                    RecyclerView recyclerView =  convertView.findViewById(R.id.rv_course);
                    SelectLevelAdapter adapter = new SelectLevelAdapter(arrLanguage,arrImage,activity,position);
                    linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setAdapter(adapter);
                    recyclerView.addItemDecoration(new MyDividerItemDecoration(activity, DividerItemDecoration.VERTICAL, 36));

                    alertDialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });


                    alertDialog.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrLanguage.size();
    }


    public class MyView extends RecyclerView.ViewHolder {

        private CircleImageView img;
        private TextView txt;

        public MyView(View view) {
            super(view);
            img=view.findViewById(R.id.img);
            txt=view.findViewById(R.id.tvLanguage);

        }
    }
}
