package com.eup.learningenglish.activity.arena.prepare;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.arena.play.ArenaActivity;
import com.eup.learningenglish.model.Course;
import com.eup.learningenglish.model.Lesson;
import com.eup.learningenglish.prefence.MPrefence;
import com.eup.learningenglish.ui.NumberPicker;
import com.eup.learningenglish.utils.Utils;
import com.google.gson.Gson;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by HoaDucHung on 8/31/2017.
 */

public class PrepareCreateFragment extends Fragment implements NumberPicker.OnValueChangeListener, AdapterView.OnItemSelectedListener   {

    @BindView(R.id.number_picker)
    NumberPicker numberPicker;
    @BindView(R.id.spinnerLesson)
    Spinner spinner;

    private boolean isStarted = true;
    private MPrefence mainPref;
    private List<Lesson> lessons;
    private ArrayAdapter<Lesson> dataAdapter;

    public PrepareCreateFragment() {
        // Required empty public constructor
    }

    public static PrepareCreateFragment newInstance() {
        PrepareCreateFragment fragment = new PrepareCreateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       mainPref = new MPrefence(getActivity(),MPrefence.NAME_PREF_MAIN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_arena_prepare_create, null);
        ButterKnife.bind(this, v);
        new initDataAndView().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        return v;
    }

    private class initDataAndView extends AsyncTask<Void, Void, Integer[]> {
        @Override
        protected Integer[] doInBackground(Void... voids) {

//            // get last id subject
            String idSubject = mainPref.getStringWithName(MPrefence.last_subject, "");
            // get arena number question
            int last_number_arena = mainPref.getIntWithName(MPrefence.last_number_arena, 5);

            String course = mainPref.getStringWithName(MPrefence.stringSubjectOfCourse,"");
            Course courses= new Gson().fromJson(course,Course.class);
            lessons = courses.getArrLesson();

            int max = 10;
            int position = 0;
            for (int i=0;i<lessons.size();i++) {
                if (idSubject.equals(lessons.get(i).getId()) || idSubject.equals("")) {
                    position=i;
                    max = lessons.get(i).getTotal();
                    break;
                }
            }

            // setup spiner
            dataAdapter = new ArrayAdapter<Lesson>(getActivity(), R.layout.my_spinner_style, lessons) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);
                    ((TextView) v).setTextSize(16);
                    ((TextView) v).setGravity(Gravity.RIGHT);
                    return v;
                }
            };

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            return new Integer[]{max, last_number_arena <= max ? last_number_arena : max, position};
        }

        @Override
        protected void onPostExecute(Integer[] integers) {
            super.onPostExecute(integers);
            // cap nhat list bai hoc
            spinner.setOnItemSelectedListener(PrepareCreateFragment.this);
            // attaching data adapter to spinner
            spinner.setAdapter(dataAdapter);
            spinner.setSelection(integers[2]);

            // number picker
            numberPicker.setMaxValue(integers[0]);
            // gan vi tri hien tai cho number picker
            numberPicker.setValue(integers[1]);
            // bat su kien thay doi gia tri so cau hoi trong phan test
            numberPicker.setOnValueChangedListener(PrepareCreateFragment.this);

            // co the tao arena
            isStarted = false;
        }

        // neu ca 3 kieu lam bai deu bi uncheck thi disable button start
        //        checkCanStart();
    }

    // change lesson
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//        // On selecting a spinner item
//        String item = adapterView.getItemAtPosition(i).toString();
//
//        // Showing selected spinner item
//        Toast.makeText(getActivity(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    /**
     * on click listener
     */
    @OnClick({R.id.btnStart})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.btnStart:
                // bat dau kiem tra
                onStartPressed();

                break;
        }
    }

    public void onStartPressed() {
        int pos = spinner.getSelectedItemPosition();
        if(lessons.size()==0){
            Toast.makeText(getContext(), "Bạn không thể tạo đấu trường với khóa học này !", Toast.LENGTH_SHORT).show();
        }
        else if(lessons.get(pos).getTotal()==0){
            Toast.makeText(getContext(), "Bạn không thể tạo đấu trường với bài học "+lessons.get(pos).getName()+" !", Toast.LENGTH_SHORT).show();
        }
        else if (!isStarted) { // chi cho click 1 lan
            // create arena

            Lesson lesson = lessons.get(pos);
            if (lesson != null) {
                // set limit
                lesson.setTotal(numberPicker.getValue());
                // set name room arena
//                String courseName = mainPref.getStringWithName(MinderPref.course_title, "");
                lesson.setName(lesson.getName());

                // chuyen sang Arena activity de tao phong va cau hoi
                Intent intent = new Intent(getActivity(), ArenaActivity.class);
                String json = new Gson().toJson(lesson);
                intent.putExtra(Utils.SEND_JSON, json);
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), getString(R.string.arena_creating_room_failed), Toast.LENGTH_SHORT).show();
            }

            isStarted = true;
        }
    }

    /**
     * impliment onvalue change listener
     *
     * @param picker The NumberPicker associated with this listener.
     * @param oldVal The previous value.
     * @param newVal The new value.
     */
    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        mainPref.setIntWithName(MPrefence.last_number_arena, newVal);
    }
}
