package com.eup.learningenglish.activity.fisrtlogin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.fisrtlogin.adapter.MyDividerItemDecoration;
import com.eup.learningenglish.activity.fisrtlogin.adapter.dialog.SeectLanguageAdapter;
import com.eup.learningenglish.activity.fisrtlogin.adapter.main.CourseMinderAdapter;
import com.eup.learningenglish.listener.OnClickLevel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

/**
 * Created by HoaDucHung on 2/1/2018.
 */

public class ListCourseFirstLogin extends AppCompatActivity{

    private int lang,level;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    ViewPager viewPager;
    @BindView(R.id.appBar)
    AppBarLayout appBarLayout;
    private LinearLayoutManager linearLayoutManager;
    private CourseMinderAdapter adapter;
    public static OnClickLevel getOnClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_first_login);
        ButterKnife.bind(this);

        Intent intent=getIntent();
        if (intent!=null){
            lang=Integer.parseInt(intent.getStringExtra("lang"));
            level=intent.getIntExtra("level",1);
        }

        init();


        adapter= new CourseMinderAdapter(getSupportFragmentManager(),this,lang);
        OverScrollDecoratorHelper.setUpOverScroll(viewPager);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setupWithViewPager(viewPager);


        // Iterate over all tabs and set the custom view
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(adapter.getTabView(i));
        }


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
//                sharedPreferences.edit().putInt("pos_tab", tab.getPosition()).apply();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.setCurrentItem(level-1);


        getOnClick = new OnClickLevel() {
            @Override
            public void getClickLevelLanguage(int lang, int level) {
                finish();
                Intent intent1 =new Intent(ListCourseFirstLogin.this,ListCourseFirstLogin.class);
                intent1.putExtra("lang",lang+"");
                intent1.putExtra("level",level);
                startActivity(intent1);
            }
        };

    }

    private void init() {

        CircleImageView circleImageView=findViewById(R.id.imgItem);
        TextView tvLanguague=findViewById(R.id.tvName);

        final List<String> arrLanguage= new ArrayList<String>();
        arrLanguage.add("Tiếng Anh");
        arrLanguage.add("Tiếng Nhật");
        arrLanguage.add("Tiếng Hàn");
        arrLanguage.add("Tiếng Trung");
        final List<Integer> arrImage= new ArrayList<>();
        arrImage.add(R.drawable.flag_en);
        arrImage.add(R.drawable.flag_jp);
        arrImage.add(R.drawable.flag_ko);
        arrImage.add(R.drawable.flag_cn);

        switch (lang){
            case 101:
                circleImageView.setImageResource(arrImage.get(0));
                tvLanguague.setText(arrLanguage.get(0));
                break;
            case 104:
                circleImageView.setImageResource(arrImage.get(1));
                tvLanguague.setText(arrLanguage.get(1));
                break;
            case 103:
                circleImageView.setImageResource(arrImage.get(2));
                tvLanguague.setText(arrLanguage.get(2));
                break;
            case 102:
                circleImageView.setImageResource(arrImage.get(3));
                tvLanguague.setText(arrLanguage.get(3));
                break;
        }


        LinearLayout linearLayout=findViewById(R.id.course_choose);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ListCourseFirstLogin.this);
                LayoutInflater inflater = getLayoutInflater();
                View convertView =  inflater.inflate(R.layout.layout_dialog_selectlanguage, null);
                alertDialog.setView(convertView);
                alertDialog.setTitle("Chọn ngôn ngữ");
                RecyclerView recyclerView =  convertView.findViewById(R.id.rv_course);
                SeectLanguageAdapter adapter = new SeectLanguageAdapter(arrLanguage,arrImage,ListCourseFirstLogin.this);
                linearLayoutManager = new LinearLayoutManager(ListCourseFirstLogin.this, LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(adapter);
                recyclerView.addItemDecoration(new MyDividerItemDecoration(ListCourseFirstLogin.this, DividerItemDecoration.VERTICAL, 36));

                alertDialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


                alertDialog.show();
            }
        });
    }


}
