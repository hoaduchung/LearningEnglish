package com.eup.learningenglish.activity.arena.prepare;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.arena.play.ArenaActivity;
import com.eup.learningenglish.firebase.adapter.WaitingAdapter;
import com.eup.learningenglish.firebase.controller.FirebaseUtil;
import com.eup.learningenglish.firebase.listener.OnGameCodeListener;
import com.eup.learningenglish.firebase.listener.OnGameStateListener;
import com.eup.learningenglish.firebase.listener.OnJoinWaitingListener;
import com.eup.learningenglish.firebase.module.GameState;
import com.eup.learningenglish.firebase.util.DialogUtil;
import com.eup.learningenglish.utils.NetWork;
import com.eup.learningenglish.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by HoaDucHung on 8/31/2017.
 */

public class PrepareJoinFragment extends Fragment {
    @BindView(R.id.edtCode)
    EditText edtCode;
    @BindView(R.id.tvSuggest)
    TextView tvSuggest;
    @BindView(R.id.recyclerView)
    RecyclerView mRecycler;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.layoutNull)
    View layoutNull;
    private View view;

    //    waiting room
    private FirebaseUtil firebaseUtil;
    private LinearLayoutManager mManager;
    private WaitingAdapter mAdapter;
    private List<GameState> waitingRooms = new ArrayList<>();


    public static PrepareJoinFragment newInstance() {
        PrepareJoinFragment fragment = new PrepareJoinFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_arena_prepare_join, container, false);

        // butter knife
        ButterKnife.bind(this, view);

        // setup suggest view
        setupSuggest();

        // khoi tao firebae instance
        firebaseUtil = new FirebaseUtil();

        return view;
    }

    private void setupSuggest() {
        tvSuggest.setText(Html.fromHtml(getString(R.string.suggest_game_arena)));

        // them action done de tham gia
        edtCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED
                        || (event != null && event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {

                    // neu nguoi dung chua nhap dap an thi khong lam gi ca
                    if (edtCode.getText().toString().isEmpty()) return true;

                    //do something
                    joinGame();

                    return true;
                }
                return false;
            }
        });
    }

    /**
     * click tham gia dau truong
     */
    @OnClick(R.id.btnJoinGame)
    void joinGame() {
        // kiem tra neu chuoi ma code co hop le khong
        if (!edtCode.getText().toString().isEmpty()) {

            // kiem tra ket noi mang
            if (!NetWork.isNetWork(getActivity())) {
                Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                return;
            }

            final ProgressDialog dialog = DialogUtil.showLoadingDialog(getActivity(), getString(R.string.arena_game_code_loading)); // dialog nen trang
            firebaseUtil.getGameContentKeyWithCode(edtCode.getText().toString(), new OnGameCodeListener() {
                @Override
                public void singleValueEvent(final String id) {

                    // ma phong khong ton tai
                    if (id == null || id.isEmpty()) {
                        if (dialog != null && dialog.isShowing())
                            // an dialog loading
                            dialog.dismiss();

                        if (getActivity() != null && !getActivity().isFinishing())
                            Toast.makeText(getActivity(), getString(R.string.arena_game_code_wrong), Toast.LENGTH_SHORT).show();
                    } else {
                        // thay doi text dang vao phong
                        dialog.setMessage(getString(R.string.arena_game_code_loading1));

                        // kiem tra ma phong do con waiting khong
                        firebaseUtil.checkGameStateWithContentKey(id, new OnGameStateListener() {
                            @Override
                            public void stateChange(GameState gameState) {

                                if (gameState != null) {
                                    switch (gameState.state) {
                                        case GameState.playing:
                                            if (dialog != null && dialog.isShowing())
                                                dialog.dismiss();

                                            // thong bao game da duoc bat dau
                                            if (getActivity() != null && !getActivity().isFinishing())
                                                Toast.makeText(getActivity(), getString(R.string.arena_game_state_playing), Toast.LENGTH_SHORT).show();

                                            break;

                                        case GameState.waiting:
                                            // chuyen sang activity arena play
                                            Intent intent = new Intent(getActivity(), ArenaActivity.class);
                                            intent.putExtra(Utils.SEND_ID_GAME_CONTENT, id);
                                            startActivity(intent);

                                            if (dialog != null && dialog.isShowing())
                                                dialog.dismiss();

                                            break;

                                        case GameState.end:
                                            if (dialog != null && dialog.isShowing())
                                                dialog.dismiss();

                                            // thong bao game da ket thuc
                                            if (getActivity() != null && !getActivity().isFinishing())
                                                Toast.makeText(getActivity(), getString(R.string.arena_game_state_end), Toast.LENGTH_SHORT).show();

                                            break;

                                        case GameState.cancelled:
                                            if (dialog != null && dialog.isShowing())
                                                dialog.dismiss();

                                            // thong bao game da bi huy
                                            if (getActivity() != null && !getActivity().isFinishing())
                                                Toast.makeText(getActivity(), getString(R.string.arena_game_state_cancel), Toast.LENGTH_SHORT).show();

                                            break;
                                    }
                                } else {
                                    if (dialog != null && dialog.isShowing())
                                        dialog.dismiss();

                                    if (getActivity() != null && !getActivity().isFinishing())
                                        Toast.makeText(getActivity(), getString(R.string.arena_game_code_wrong), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    /**
     * khoi tao rv theo firebase adapter
     */
    private void initRecyclerView() {
        mRecycler.setHasFixedSize(true);

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mRecycler.setLayoutManager(mManager);

        // Set up FirebaseRecyclerAdapter with the Query
        // lấy những trường hợp phòng ở trạng thái chờ
        Query usersQuery = firebaseUtil.getListWaitingRoom();

        // tao adapter phong cho
        mAdapter = new WaitingAdapter(waitingRooms, getActivity(), new OnJoinWaitingListener() {
            @Override
            public void join(String code) {
                if (code != null && !code.isEmpty()) {
                    edtCode.setText(code);
                    joinGame();
                }
            }
        });

        usersQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                waitingRooms.clear();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    // lay game state tu snapshot
                    GameState gameState = snapshot.getValue(GameState.class);

                    // kiem tra dieu kien phong cho <10 p đã tạo thì lấy
                    if (gameState.created != null && System.currentTimeMillis() - gameState.created < 600000) {
                        waitingRooms.add(gameState);
                    }
                }

                // hien thi layout chu thich danh sach rong
                if (waitingRooms.isEmpty() && layoutNull != null) {
                    layoutNull.setVisibility(View.VISIBLE);
                } else {
                    layoutNull.setVisibility(View.GONE);
                }

                mAdapter.setWaitingRooms(waitingRooms);
                // Bind Post to ViewHolder, setting OnClickListener for the star button
                mAdapter.notifyDataSetChanged();

                if (progressBar.getVisibility() == View.VISIBLE) {
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mRecycler.setAdapter(mAdapter);
    }


    @Override
    public void onResume() {
        super.onResume();

        // khoi tao danh sach phong cho
        initRecyclerView();
    }
}
