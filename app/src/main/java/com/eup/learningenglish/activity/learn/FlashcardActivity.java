package com.eup.learningenglish.activity.learn;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.eup.learningenglish.R;
import com.eup.learningenglish.fragment.learn.FlashcardBehind;
import com.eup.learningenglish.fragment.learn.FlashcardFront;
import com.eup.learningenglish.model.Course;
import com.eup.learningenglish.model.MWord;
import com.eup.learningenglish.prefence.MPrefence;
import com.google.gson.Gson;

import java.util.List;


/**
 * Created by HoaDucHung on 2/7/2018.
 */

public class FlashcardActivity extends AppCompatActivity implements View.OnTouchListener {
    private Toolbar toolbar;
    private FragmentManager fragmentManager;
    private int pos;
    private MPrefence mPrefence;
    public static List<MWord> arrData;
    // on swipe
    private float x1, x2;
    private final int MIN_DISTANCE = 50;
    private Button btn_no, btn_yes;
    private FrameLayout frameLayout;
    private int count = 0;
    private int id_course;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_card);
        mPrefence = new MPrefence(this,MPrefence.NAME_PREF_MAIN);
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        fragmentManager = getSupportFragmentManager();


        Intent intent = getIntent();
        pos = intent.getIntExtra("positionSubject",1);
        id_course = intent.getIntExtra("id_course",1);


        String course = mPrefence.getStringWithName(MPrefence.stringSubjectOfCourse,"");
        Course courses= new Gson().fromJson(course,Course.class);
        arrData = courses.getArrLesson().get(pos).getArrWord();
        //mac dinh
        replaceFragmentNext(FlashcardFront.newInstance(count, id_course));

        findViewById();


    }

    private void findViewById() {
        btn_no =  findViewById(R.id.btn_no);
        btn_yes =  findViewById(R.id.btn_yes);
        frameLayout =  findViewById(R.id.frameFlashCard);
        frameLayout.setOnTouchListener(this);
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNext();
            }
        });
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    onNext();
            }
        });


    }
    // chưa thuộc hay đã thuộc đều next
    private void onNext() {
        if (count < arrData.size() - 1) {
                count++;
                replaceFragmentNext(FlashcardFront.newInstance(count,id_course));
            }
        }



    // on swipe listener
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    if (x2 > x1) {
                        onSwipeLeft();
                    } else onSwipeRight();
                } else {
                    autoChangeFragment();
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    private void autoChangeFragment() {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameFlashCard);
            if (fragment instanceof FlashcardFront) {
                replaceFragmentMiddle(FlashcardBehind.newInstance(count,id_course));
            } else {
                replaceFragmentMiddle(FlashcardFront.newInstance(count, id_course));
            }
        }




    // khi vuốt sang phải
    private void onSwipeRight() {
        if (count < arrData.size() - 1) {
                count++;
                replaceFragmentNext(FlashcardFront.newInstance(count, id_course));
            }
    }

    // khi vuốt sang trái
    private void onSwipeLeft() {
        if (count > 0) {
                count--;
                replaceFragmentPre(FlashcardFront.newInstance(count, id_course));
            }
    }

    private void replaceFragmentPre(Fragment fragment) {
        if (FlashcardActivity.this != null && !this.isFinishing()) {
            int resIn = android.R.anim.slide_in_left;
            int resOut = android.R.anim.slide_out_right;
            fragmentManager.beginTransaction()
                    .setCustomAnimations(resIn, resOut, resIn, resOut)
                    .replace(R.id.frameFlashCard, fragment)
                    .commitAllowingStateLoss();
        }
    }


    private void replaceFragmentNext(Fragment fragment) {
        if (FlashcardActivity.this != null && !this.isFinishing()) {
            try {
                int resIn = R.anim.slide_in_right;
                int resOut = R.anim.slide_out_left;
                fragmentManager.beginTransaction()
                        .setCustomAnimations(resIn, resOut, resIn, resOut)
                        .replace(R.id.frameFlashCard, fragment)
                        .commitAllowingStateLoss();
            }catch (IllegalStateException e){}
        }
    }


    private void replaceFragmentMiddle(Fragment fragment) {
        if (FlashcardActivity.this != null && !this.isFinishing()) {
            int resIn = R.anim.from_middle;
            int resOut = R.anim.to_middle;
            fragmentManager.beginTransaction()
                    .setCustomAnimations(resIn, resOut, resIn, resOut)
                    .replace(R.id.frameFlashCard, fragment)
                    .commitAllowingStateLoss();
        }
    }
}
