package com.eup.learningenglish.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.fisrtlogin.adapter.MyDividerItemDecoration;
import com.eup.learningenglish.activity.game.GamesPlayActivity;
import com.eup.learningenglish.activity.learn.FlashcardActivity;
import com.eup.learningenglish.adapter.StudyAdapter;
import com.eup.learningenglish.model.Course;
import com.eup.learningenglish.model.MWord;
import com.eup.learningenglish.prefence.MPrefence;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;


/**
 * Created by HoaDucHung on 2/6/2018.
 */

public class StudyActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private MPrefence mPrefence;
    private RecyclerView recyclerView;
    private StudyAdapter studyAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private int pos;
    private int id_course;
    private List<MWord> arrData;
    private Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study);
        mPrefence = new MPrefence(this, MPrefence.NAME_PREF_MAIN);
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnStart = findViewById(R.id.btn_start);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StudyActivity.this, FlashcardActivity.class);
                intent.putExtra("positionSubject",pos);
                intent.putExtra("id_course",id_course);
                startActivity(intent);
            }
        });

        Intent intent =getIntent();
        pos = intent.getIntExtra("positionSubject",1);
        id_course = intent.getIntExtra("id_course",1);

        recyclerView  = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);

        initData();
    }

    private void initData() {
        arrData = new ArrayList<>();
        String course = mPrefence.getStringWithName(MPrefence.stringSubjectOfCourse,"");
        Course object=  new Gson().fromJson(course,Course.class);
        arrData = object.getArrLesson().get(pos).getArrWord();


        studyAdapter = new StudyAdapter(arrData, id_course,this);
        recyclerView.setAdapter(studyAdapter);
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 36));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_game,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_games:
                String value = new Gson().toJson(arrData);
                Intent intent = new Intent(this, GamesPlayActivity.class);
                intent.putExtra("data",value);
                intent.putExtra("id_course",id_course);
                startActivity(intent);

                break;
            case android.R.id.home:
                this.finish();
                break;
            }
        return super.onOptionsItemSelected(item);
    }

}
