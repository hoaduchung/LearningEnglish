package com.eup.learningenglish.activity.game;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.eup.learningenglish.fragment.game.GameFragmentSpeak;
import com.eup.learningenglish.R;
import com.eup.learningenglish.listener.OnCountDownListener;
import com.eup.learningenglish.model.MWord;
import com.eup.learningenglish.prefence.MPrefence;
import com.eup.learningenglish.utils.Combo;
import com.eup.learningenglish.utils.SoundPoolUtil;
import com.eup.learningenglish.waveview.WaveAnimation;
import com.eup.learningenglish.waveview.WaveView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rey.material.app.BottomSheetDialog;

import java.util.Collections;

import java.util.List;


public class GamesPlayActivity extends AppCompatActivity {
    public static TextView tv_point;
    private static Activity activity;
    private static FragmentManager fmGame;
    private static int count_question, combo, point, posGame, count_continue;
    private static AppCompatButton btn_play,btn_back,btn_continuenew;
    public static TextView tv_score1, tv_bandangco, img_newbest,tvTienbo;
    public static LinearLayout btn_continue;
    public static LinearLayout btn_getStone;
    private static ImageView img_star1, img_star2, img_star3;
    private static CardView llNative;
    private static ImageButton btn_home1, btn_replay ;

    private static boolean isPause, isResume = false, isClicked;
    public static boolean isClicked2;
    static WaveView waveView;
    static WaveAnimation anim;
    static final long DEAUTL_TIME = 25000, TIME_GAME_WRITE = 30000, TIME_GAME_MATCH = 35000, TIME_GAME_SPEAK = 50000, duration2 = 6000;
    static int POINT_GAME = 50;
    public static float valueProgress = 100f;
    public static long time;
    public static CountDownTimer countDownTimer, countDownContinue;
    static BottomSheetDialog mDialog2;
    static Dialog mDialog1,mDialog;
    private static boolean isNewBest;
    public static List<MWord> arrData;
    public static int id_course;
    public static MPrefence mPrefence;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games_play);
        mPrefence = new MPrefence(this,MPrefence.NAME_PREF_MAIN);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        activity = this;
        findViewById();
        fmGame = getSupportFragmentManager();

        Intent intent = getIntent();
        String value = intent.getStringExtra("data");
        id_course =  intent.getIntExtra("id_course",1);
        posGame =4;
        arrData =new Gson().fromJson(value ,new TypeToken<List<MWord>>() {
        }.getType());
        if(arrData != null && arrData.size()>3){
            Collections.shuffle(arrData);
        }
        else {
            Toast.makeText(GamesPlayActivity.this, getString(R.string.questionList_null1), Toast.LENGTH_SHORT).show();
        }



        new Handler().post(new Runnable() {
            @Override
            public void run() {
                // prepare time with game
                prepareTime();
                // show prepare
                showDialogPause(true);
            }
        });
        // init sound pool
        new createSoundEF().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }



    private static void prepareTime() {
        time = TIME_GAME_MATCH;
        POINT_GAME = 50;
    }

    private void findViewById() {
        tv_point =  findViewById(R.id.tvPoint);
        waveView =  findViewById(R.id.wave_view);
    }

    public static void startCountDown() {
        final SharedPreferences sharedPreferences = activity.getSharedPreferences("GAME_PLAY", Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean("isTimer", true)) {
            anim = new WaveAnimation(waveView, valueProgress, 0f);
            long duration = Math.round((time * valueProgress) / 100f);
            anim.setDuration(duration);
            anim.reset();
            waveView.clearAnimation();
            waveView.startAnimation(anim);
            if (countDownTimer != null)
                countDownTimer.cancel();
            countDownTimer = new CountDownTimer(duration, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    if (!isPause) {
                            showLayoutFinish();
                    }
                }
            }.start();
        }
    }

    public static void pauseCountDown() {
        if (anim != null) {
            anim.cancel();
            waveView.clearAnimation();
            countDownTimer.cancel();
        }
    }


    public static void showLayoutFinish() {
            showLayoutFinish2();
    }

    public static void showLayoutFinish2() {
        isPause = true;
        showDialogFinish2();
        count_continue = 0;
        tv_score1.setText("Điểm của bạn : " + point);
        if(point == 0){
            img_newbest.setVisibility(View.INVISIBLE);
            tvTienbo.setText("Điểm cao nhất : "+mPrefence.getIntWithName(MPrefence.diemcaonhat,0));
        }
        else {
            if(point > mPrefence.getIntWithName(MPrefence.diemcaonhat,0)){
                mPrefence.setIntWithName(MPrefence.diemcaonhat,point);
                tvTienbo.setText("Điểm cao nhất :"+point);
                img_newbest.setVisibility(View.VISIBLE);
            }
            else{
                img_newbest.setVisibility(View.INVISIBLE);
                tvTienbo.setText("Điểm cao nhất :"+mPrefence.getIntWithName(MPrefence.diemcaonhat,0));
            }
        }
    }

    private static void hideLayoutPause() {
        isClicked = true;
        isPause = false;
        mDialog.dismiss();
    }

    private static void hideLayoutFinish() {
        isPause = false;
        isClicked2 = false;
        mDialog1.dismiss();
    }

    private static void hideLayoutFinish2() {
        isClicked = true;
        isPause = false;
        mDialog2.dismiss();

    }

    private static void replaceFragmentGame(Fragment fragment) {
        int resIn = android.R.anim.slide_in_left;
        int resOut = android.R.anim.slide_out_right;
        try {
            fmGame.beginTransaction()
                    .setCustomAnimations(resIn, resOut, resIn, resOut)
                    .replace(R.id.frameGames, fragment)
                    .commit();
        } catch (Exception e) {
        }
    }



    @Override
    public void onBackPressed() {

            if (isPause) {
                super.onBackPressed();
                reset();
            } else {
                pauseCountDown();
                showDialogPause(false);

        }
    }

    public static void createQuestion() {
            if (arrData != null && arrData.size() > 0) {
                MWord question = arrData.get(0);
                createGame(question);
                isResume = true;
            } else {
                Toast.makeText(activity, activity.getString(R.string.questionList_null), Toast.LENGTH_SHORT).show();
                activity.finish();
        }
    }

    private static void createGame(MWord question) {
        replaceFragmentGame(GameFragmentSpeak.newInstance(question,id_course));
    }

    //dua vao quality de cong diem
    public static void addPoint(int quality) {
        if (quality == 0) {
            combo = 0;
            return;
        }
        int addPoint = 0;
        addPoint = (int) (POINT_GAME - Math.round(POINT_GAME * 0.1 * (5 - quality)));

        if (quality == 5) {
            // neu tra loi chinh xac
            addPoint = Combo.pointCombo(addPoint, combo);
            point += addPoint;
            if (combo != 0)
                tv_point.setTextColor(activity.getResources().getColor(android.R.color.holo_red_dark));
            else
                tv_point.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            combo += 1;
        } else {
            // + diem va gan combo = 0
            point += addPoint;
            combo = 0;
            tv_point.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        }
//        pointNew += addPoint;
        tv_point.setText("+".concat(String.valueOf(addPoint)));

        YoYo.with(Techniques.SlideOutUp)
                .duration(400)
                .delay(400)
                .withListener(new com.nineoldandroids.animation.Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(com.nineoldandroids.animation.Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(com.nineoldandroids.animation.Animator animator) {
                        tv_point.setText(String.valueOf(point));
                        tv_point.setTextColor(activity.getResources().getColor(android.R.color.white));
                        YoYo.with(Techniques.SlideInUp)
                                .duration(400)
                                .interpolate(new AccelerateDecelerateInterpolator())
                                .playOn(tv_point);
                    }

                    @Override
                    public void onAnimationCancel(com.nineoldandroids.animation.Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(com.nineoldandroids.animation.Animator animator) {

                    }
                })
                .interpolate(new AccelerateDecelerateInterpolator())
                .playOn(tv_point);

    }

    public static void showDialogPause(boolean check) {
        // tat nhac nen
        new BackgroundSoundPause().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        isPause = true;
        // custom dialog
        mDialog = new Dialog(activity);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.layout_game_pause);
        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (!isClicked && activity != null) {
                    activity.finish();
                    reset();
                } else isClicked = false;
            }
        });
        //init
        btn_play =  mDialog.findViewById(R.id.btn_play);
        btn_back =  mDialog.findViewById(R.id.btn_back);
        btn_continuenew =  mDialog.findViewById(R.id.btn_continuenew);
        final TextView textView=mDialog.findViewById(R.id.tvTime);
        final ImageView ib_timer =  mDialog.findViewById(R.id.ib_timer);
        LinearLayout linearLayout=mDialog.findViewById(R.id.lnTimedown);
        LinearLayout linearLayoutBack=mDialog.findViewById(R.id.lnBack);
        RadioGroup radioGroup =  mDialog.findViewById(R.id.radioGroup);
        RadioButton rbKanji =  mDialog.findViewById(R.id.rb_kanji);
        RadioButton rbHiragana =  mDialog.findViewById(R.id.rb_hiragana);
        ImageButton imageButton=mDialog.findViewById(R.id.btn_close);


        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        activity.finish();
                        reset();
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
                reset();
            }
        });

        // is count down timer
        final SharedPreferences sharedPreferences = activity.getSharedPreferences("GAME_PLAY", Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean("isTimer", true)) {
            ib_timer.setImageResource(R.mipmap.ic_timedown);
            textView.setText(R.string.batdemnguoc);
        } else {
            ib_timer.setImageResource(R.mipmap.ic_nottimedown);
            textView.setText(R.string.tatdemnguoc);
        }


        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("isTimer", true)) {
                    ib_timer.setImageResource(R.mipmap.ic_nottimedown);
                    textView.setText(R.string.tatdemnguoc);
                    sharedPreferences.edit().putBoolean("isTimer", false).apply();
                } else {
                    ib_timer.setImageResource(R.mipmap.ic_timedown);
                    sharedPreferences.edit().putBoolean("isTimer", true).apply();
                    textView.setText(R.string.batdemnguoc);
                }
            }
        });

        TextView tv_name =  mDialog.findViewById(R.id.tv_name);
        tv_name.setText(R.string.practice_speak);
        ImageView iv_icon =  mDialog.findViewById(R.id.iv_suggest);
        iv_icon.setImageResource(R.drawable.ic_luyennoi);

        // is japanese and show kanji or hiragana
        if ( mPrefence.getIntWithName(MPrefence.language,1)== 104) {
            radioGroup.setVisibility(View.VISIBLE);
            if (sharedPreferences.getBoolean("isKanji", true)) {
                rbKanji.setChecked(true);
            } else {
                rbHiragana.setChecked(true);
            }
            rbKanji.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    sharedPreferences.edit().putBoolean("isKanji", isChecked).apply();
                }
            });
        }



        if(check){
            btn_play.setVisibility(View.VISIBLE);
            linearLayoutBack.setVisibility(View.GONE);
            mDialog.setCancelable(true);
            imageButton.setVisibility(View.VISIBLE);
        }
        else {
            btn_play.setVisibility(View.GONE);
            linearLayoutBack.setVisibility(View.VISIBLE);
            imageButton.setVisibility(View.GONE);
            mDialog.setCancelable(false);
        }

        if (!activity.isFinishing() && !mDialog.isShowing()) {
            mDialog.show();
        }
        // set on click listener
        // onclick pause

        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playGame();
            }
        });

        btn_continuenew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playGame();
            }
        });


    }
    public static void playGame(){
        hideLayoutPause();
        // create and show question
        if (!isResume) {
            createQuestion();
        }
        startCountDown();
    }

    public static void showDialogFinish() {
        // tat nhac nen
        new BackgroundSoundPause().execute();

        isPause = true;
        // custom dialog
        mDialog1 = new Dialog(activity);
        mDialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // khi chiến thắng
        mDialog1.setContentView(R.layout.layout_continue);
        mDialog1.setCancelable(false);

        tv_bandangco=mDialog1.findViewById(R.id.bandangco);
        btn_continue = (LinearLayout) mDialog1.findViewById(R.id.btn_Continue);
        btn_getStone = (LinearLayout) mDialog1.findViewById(R.id.btn_getStone);
        ImageButton btn_close = (ImageButton) mDialog1.findViewById(R.id.btn_close);

        countdownContinue(mDialog1);
        // onclick listener
        int num_diamon = 0;


        final int finalNum_diamon = num_diamon;
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalNum_diamon >= 2) {
                    // tru da
                    Toast.makeText(activity, activity.getString(R.string.tru_da), Toast.LENGTH_SHORT).show();

                    // tiep tuc
                    mContinue();
                } else {
                    Toast.makeText(activity, activity.getString(R.string.not_enough_diamon), Toast.LENGTH_SHORT).show();
                }
            }
        });
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (countDownContinue != null)
                    countDownContinue.cancel();
                hideLayoutFinish();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showLayoutFinish2();
                    }
                }, 300);
            }
        });

        if (!activity.isFinishing() && !mDialog1.isShowing()) {
            mDialog1.show();
        }
    }

    private static void countdownContinue(final Dialog mDialog1) {
        if (countDownContinue != null)
            countDownContinue.cancel();
        countDownContinue = new CountDownTimer(duration2, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(mDialog1.isShowing()||mDialog1!=null)
                        mDialog1.dismiss();
                        showLayoutFinish2();
                    }
                }, 300);
            }
        }.start();
    }

    public static void showDialogFinish2() {
        new BackgroundSoundPause().execute();
        isPause = true;
        mDialog2 = new BottomSheetDialog(activity);
        mDialog2.applyStyle(R.style.Material_App_BottomSheetDialog)
                .contentView(R.layout.layout_game_finish)
                .heightParam(ViewGroup.LayoutParams.MATCH_PARENT)
                .inDuration(300)
                .canceledOnTouchOutside(false)
                .cancelable(false);
        if (!activity.isFinishing() && !mDialog2.isShowing()) {
            mDialog2.show();
        }
        mDialog2.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (!isClicked && activity != null) {
                    activity.finish();
                    reset();
                } else isClicked = false;
            }
        });

        btn_home1 =  mDialog2.findViewById(R.id.btn_home1);
        tv_score1 =  mDialog2.findViewById(R.id.tv_score1);
        tvTienbo=mDialog2.findViewById(R.id.tvTienbo);
        img_newbest = mDialog2.findViewById(R.id.iv_newbest);
        btn_replay =  mDialog2.findViewById(R.id.btn_replay);

        // set on click listener
        // onclick finish
        btn_home1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
                reset();
            }
        });
        btn_replay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideLayoutFinish2();
                reset();
                Collections.shuffle(arrData);
                createQuestion();
                startCountDown();
            }
        });

        llNative =  mDialog2.findViewById(R.id.adCardView);

    }

    public static void mContinue() {
        count_continue++;
        if (countDownContinue != null)
            countDownContinue.cancel();
        hideLayoutFinish();
//        prepareTime();
        valueProgress = 100f;
        isResume = false;
        Collections.shuffle(arrData);
        createQuestion();
        startCountDown();
    }


    public static void reset() {
        prepareTime();
        valueProgress = 100f;
        combo = 0;
        point = 0;
        count_question = 0;
        tv_point.setText("0");
        isResume = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mDialog != null) {
            mDialog.dismiss();
        }
        if (mDialog1 != null) {
            mDialog1.dismiss();
        }
        if (mDialog2 != null) {
            mDialog2.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isClicked2) {
            showLayoutFinish();
        }
    }



    @Override
    protected void onDestroy() {

        SoundPoolUtil.releaseSoundPool();
        new BackgroundSoundPause().execute();
        super.onDestroy();
    }

    public static class BackgroundSound extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            SoundPoolUtil.playMusicGame(activity);
            return null;
        }
    }

    public static class BackgroundSoundPause extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            SoundPoolUtil.pauseMusicGame();
            return null;
        }
    }

    private class createSoundEF extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            SoundPoolUtil.creatSoundEF(GamesPlayActivity.this);
            return null;
        }
    }



    public static long getTime() {
        return time;
    }

    public static void setTime(long time) {
        GamesPlayActivity.time = time;
    }

    public static void setValueProgress(float valueProgress) {
        GamesPlayActivity.valueProgress = valueProgress;
    }

    public static int getCount_question() {
        return count_question;
    }

    public static void setCount_question(int count_question) {
        GamesPlayActivity.count_question = count_question;
    }

    public static boolean isPause() {
        return isPause;
    }

}
