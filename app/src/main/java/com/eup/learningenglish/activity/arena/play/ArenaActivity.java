package com.eup.learningenglish.activity.arena.play;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.firebase.adapter.MemberAdapter;
import com.eup.learningenglish.firebase.controller.FirebaseUtil;
import com.eup.learningenglish.firebase.listener.OnCreateCodeListener;
import com.eup.learningenglish.firebase.listener.OnGameStateListener;
import com.eup.learningenglish.firebase.listener.OnLoadGameContentListener;
import com.eup.learningenglish.firebase.listener.OnLoadGameMemberListener;
import com.eup.learningenglish.firebase.module.GameState;
import com.eup.learningenglish.firebase.module.User;
import com.eup.learningenglish.firebase.module.gamecontent.GameContent;
import com.eup.learningenglish.firebase.util.DialogUtil;
import com.eup.learningenglish.model.Lesson;
import com.eup.learningenglish.model.MWord;
import com.eup.learningenglish.prefence.MPrefence;
import com.eup.learningenglish.utils.NetWork;
import com.eup.learningenglish.utils.SoundPoolUtil;
import com.eup.learningenglish.utils.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;


public class ArenaActivity extends BaseTTSActivity implements
        ArenaWaitingFragment.OnWaitingArenaListener, BaseArenaFragment.OnFragmentListener {
    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 53;

    @BindView(R.id.coordinator)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.recyclerView)
    RecyclerView mRecycler;

    @BindString(R.string.arena_game_content_loading)
    String arena_game_content_loading;
    @BindString(R.string.arena_capture_screen)
    String arena_capture_screen;
    @BindString(R.string.arena_creating_room)
    String arena_creating_room;
    @BindString(R.string.arena_creating_question)
    String arena_creating_question;
    @BindString(R.string.arena_message_confirm_exit)
    String arena_message_confirm_exit;
    @BindString(R.string.yes)
    String yes;
    @BindString(R.string.arena_game_state_canceled)
    String arena_game_state_canceled;
    @BindString(R.string.button_allow)
    String button_allow;
    // list term from set activity
    private List<MWord> currentList = new ArrayList<>();
    //    private int numberShuffle = new Random().nextInt(1000) + 1; // so ngau nhien de tron list
    private boolean isConfirmExit = true, isGameStateCancel, isQuitGameMember; // da bat dau shuffle chua
    private FirebaseUtil firebaseUtil;
    private GameContent gameContent;
    private User userItem;
    private ArenaChoiceFragment arenaChoiceFragment;
    private LinearLayoutManager mManager;

    private MemberAdapter mAdapter;
    private Handler handlerNextQuestion = new Handler();
    private Runnable runnableNextQuestion;
    private int lastGameStatePosition = 0;
    private MenuItem shareItem;
    //    private String setName;
    private String lastGameState = null; // trang thai game state truoc

    //get Image ScreenShot
    public static Bitmap getScreenShot(View view) {
        View screenView = view.getRootView();
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arena);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // custom mau nut back
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.close_white));

        // init firebase util
        firebaseUtil = new FirebaseUtil();

        // init data
        initData();

        // init sound pool
        SoundPoolUtil.creatSoundEF(this);


    }

    /**
     * init data from set activity
     */
    private void initData() {
        // nguoi dung tao dau truong
        final String gameId = getIntent().getStringExtra(Utils.SEND_ID_GAME_CONTENT);

        final String json = getIntent().getStringExtra(Utils.SEND_JSON);
        final Lesson lesson = new Gson().fromJson(json, Lesson.class);

        // tạo game
        if (lesson != null) {
            // tao game
            onCreateArena(lesson);
        } else if (gameId != null) {
            // tham gia game
            // hien thi man hinh dang tai noi dung game
            final Dialog dialog = DialogUtil.showLoadingDialog(this, arena_game_content_loading); // dialog phu hop voi arena

            // tai noi dung game khi nguoi dung tham gia dau truong
            firebaseUtil.loadGameContentWithId(gameId, new OnLoadGameContentListener() {
                @Override
                public void sucess(GameContent gameContent) {
                    // an dialog
                    dialog.dismiss();

                    MPrefence defaultPref = new MPrefence(ArenaActivity.this, MPrefence.NAME_PREF_MAIN);

                    userItem = new User();
                    userItem.username = defaultPref.getStringWithName(MPrefence.username, "");
                    userItem.userId = (int) defaultPref.getLongWithName(MPrefence.id, 1);

                    firebaseUtil.joinGameMember(gameId, userItem);

                    ArenaActivity.this.gameContent = gameContent;

                    // hien thi man hinh waitring voi tu cach la nguoi tham gia
                    showWaitingFragment(gameId, null, null);
                }
            });
        } else {
            // thong bao loi khong the tai arena
            Toast.makeText(this, getString(R.string.arena_creating_room_failed), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * setup max progress bar
     */

    private void setupMaxProgress(int max) {
        // max progress bang vi tri cuoi + voi current list size
        progressBar.setMax(max * 100);

    }

    /**
     * setup flashcard title
     */
    private void setMyTitle(int positionView) {
        // thay doi title
        setTitle((positionView + 1) + "/" + ((progressBar.getMax() / 100)));
    }

    /**
     * smooth progress change
     */
    private void smoothProgress(int positionView) {
        // set smooth progress bar
        // will update the "progress" propriety of seekbar until it reaches progress
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", positionView * 100);
        animation.setDuration(500); // 0.5 second
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }

    /*
   thay doi man hinh fragment
   */
    public void replaceFragment(Fragment fragment, boolean isAnimation) {

        int resIn = R.anim.slide_in_right;
        int resOut = R.anim.slide_out_left;

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (isAnimation)
            fragmentTransaction.setCustomAnimations(resIn, resOut);
        fragmentTransaction.replace(R.id.frame_arena, fragment);
        if (!isFinishing())
            fragmentTransaction.commitAllowingStateLoss();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_arena, menu);

        shareItem = menu.findItem(R.id.action_share_arena);

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * inpliment onOptionItemSelected
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share_arena:

                // check runtime permission
                checkRuntimePermission();

                break;

            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * kiem tra cap quyen
     */
    private void checkRuntimePermission() {

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {


            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        } else {

            // da duoc cap quyen
            new shareImageScreenShot().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    // da duoc cap quyen
                    new shareImageScreenShot().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }

                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void onCreateArena(Lesson lesson) {
        // kiem tra ket noi mang
        if (NetWork.isNetWork(this)) {
            isConfirmExit = false; // bien de check bat dau test, can confirm de thoat
            // chay luong tao cau hoi va phong
            new createRoomAndQuestionTask(lesson).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        } else {
            Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }

    }

    private void showWaitingFragment(String id, String code, Dialog dialog) {
        isConfirmExit = false;

        // an progress bar
        progressBar.setVisibility(View.GONE);

        // hien thi fragment prepare
        replaceFragment(ArenaWaitingFragment.newInstance(id, code, userItem.userId), code != null);

        // dang doi
        if (code != null) {
            isGameStateCancel = true;
        }

        // neu thoat se thoat khoi phong
        isQuitGameMember = true;

        // todo lang nghe game state, moi khi state thay doi, playing, cancel, end, next
        firebaseUtil.addOnStateChangeListener(id, new OnGameStateListener() {
            @Override
            public void stateChange(final GameState gameState) {
                switch (gameState.state) {
                    case GameState.waiting:
                        // khong lam gi ca
                        lastGameState = GameState.waiting;
                        break;
                    case GameState.playing:
                        // neu lan dau thi hien thi count down, lan sau thi next cau hoi
                        if (lastGameState == null || lastGameState.equals(GameState.waiting)) {
                            // hien thi count down xong hien thi cau hoi 1
                            if (userItem != null) {

                                replaceFragment(ArenaCountDownFragment.newInstance(userItem.username), false);
                            }

                        } else {
                            if (gameState != null) {
                                // hien thi dap an dung

                                nextQuestion(gameState, false, lastGameStatePosition == gameState.currentQuestion);

                                handlerNextQuestion.removeCallbacks(runnableNextQuestion);

                                runnableNextQuestion = new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!isFinishing())
                                            showPlayFragment(gameState.currentQuestion);
                                    }
                                };

                                handlerNextQuestion.postDelayed(runnableNextQuestion, 2000);

                                lastGameStatePosition = gameState.currentQuestion;
                            }
                        }

                        lastGameState = GameState.playing;

                        break;

                    case GameState.cancelled:

                        // neu chua play ma huy thi thong bao la phong nay da bi huy va thoat ra
                        if (lastGameState == null || lastGameState.equals(GameState.waiting)) {
                            // hien thi dialog phong da bi huy va thoat
                            if (!isConfirmExit)
                                showCanceledDialog();
                        }

                        lastGameState = GameState.cancelled;

                        break;

                    case GameState.end:

                        // ket thuc hien thi man hinh ket qua

                        if (gameState != null) {

                            nextQuestion(gameState, true, lastGameState.equals(GameState.end));

                            if (lastGameState.equals(GameState.playing)) {
                                smoothProgress(gameState.currentQuestion + 1);
                            }

                            handlerNextQuestion.removeCallbacks(runnableNextQuestion);

                            runnableNextQuestion = new Runnable() {
                                @Override
                                public void run() {
                                    if (!isFinishing()) {
                                        // hien thi man hinh ket qua
                                        if (mAdapter != null) {
                                            showResultFragment(mAdapter.getUserList());
                                        } else {
                                            isConfirmExit = true;
                                            onBackPressed();
                                        }
                                    }
                                }
                            };

                            handlerNextQuestion.postDelayed(runnableNextQuestion, 2000);

                        }

                        lastGameState = GameState.end;

                        break;
                }
            }
        });

        if (dialog != null)
            dialog.dismiss();
    }

    /**
     * hien thi man hinh ket qua
     */
    private void showResultFragment(List<User> users) {

        //set  menuItem visible
        if (shareItem != null)
            shareItem.setVisible(true);

        // thay doi bien check back thi hien dialog confirm exit, vi da choi game xong roi
        isConfirmExit = true;

        mRecycler.setVisibility(View.GONE);

        // an progress bar
        progressBar.setVisibility(View.GONE);

        // set title result
        setTitle(getString(R.string.write_result_title));

        String listUser = new Gson().toJson(users);

        replaceFragment(ArenaResultFragment.newInstance(listUser, userItem.userId, userItem.username), false);

    }

    /**
     * + diem, hien thi dap an dung
     *
     * @param gameState
     */
    private void nextQuestion(GameState gameState, boolean isEnd, boolean isMerge) {

        int user_id = 0;

        // + diem cho nguoi tra loi dung
        if (mAdapter != null) {
            // lay id cau nguoi tra loi dung
            user_id = gameState.answers.get(isEnd ? gameState.currentQuestion : gameState.currentQuestion - 1);

            if (!isMerge) {
                mAdapter.addPointMember(user_id);
            } else {
                mAdapter.mergePointMember(user_id);
            }
        }

        // hien thi dap an dung
        if (arenaChoiceFragment != null) {
            arenaChoiceFragment.showCorrectAnswer(user_id == userItem.userId);
        }

    }

    /**
     * hien thi man hinh cau hoi hien tai
     */
    private void showPlayFragment(int currentPosition) {
        Log.e("test","============"+currentPosition);

        // thiet lap title
        setMyTitle(currentPosition);
        // smooth progress
        smoothProgress(currentPosition);
        String question = new Gson().toJson(gameContent.questions.get(currentPosition));
        String settings = new Gson().toJson(gameContent.settings);
        // hien thi man hinh choice
        arenaChoiceFragment = ArenaChoiceFragment.newInstance(question, settings, currentPosition);

        replaceFragment(arenaChoiceFragment, false);
    }

    /**
     * lay danh sach nguoi choi
     */
    private void queryListUser(String mId, final int mUserId) {
        firebaseUtil.getListUser(mId, new OnLoadGameMemberListener() {
            @Override
            public void success(List<User> list) {
                // khoi tao hoac up date list member
                initRecyclerView(list, mUserId);
            }
        });
    }

    /**
     * khoi tao rv theo firebase adapter
     */
    private void initRecyclerView(List<User> list, int mUserId) {
        if (mAdapter == null) {
            mRecycler.setHasFixedSize(true);

            // Set up Layout Manager, reverse layout
            mManager = new LinearLayoutManager(this);
            mRecycler.setLayoutManager(mManager);

            mAdapter = new MemberAdapter(list, mUserId, this);

            mRecycler.setAdapter(mAdapter);

            progressBar.setVisibility(View.VISIBLE);

            mRecycler.setVisibility(View.VISIBLE);
        } else {
            // thay doi khi co nguoi thoat ra
            mAdapter.updateMember(list);
        }
    }

    /**
     * bat dau bai thi
     */
    @Override
    public void onStartArena() {
        if (firebaseUtil != null && gameContent != null) {
            firebaseUtil.startGame(gameContent.id);
        }
    }

    /**
     * thoat bai thi
     */
    @Override
    public void onQuitArena() {
        onBackPressed();
    }

    /**
     * kiem tra xem co phai la nguoi dau tien tra loi dung
     */
    @Override
    public void onCheckAnswer(int pos) {
        if (firebaseUtil != null) {
            firebaseUtil.checkGameContentAsnwer(gameContent.id, pos, userItem.userId, gameContent.numberQuestion);
        }
    }

    /**
     * on finish count down
     */
    @Override
    public void onFinishCountDown() {
        // cai dat progress max
        setupMaxProgress(gameContent.numberQuestion);

        // khong cho huy game nua
        isGameStateCancel = false;

        // khong cho member thoat nua
        isQuitGameMember = false;

        showPlayFragment(0);

        queryListUser(gameContent.id, userItem.userId);

    }

    /**
     * thoat activity
     */
    @Override
    public void onBackPressed() {

        // neu dang lam bai test thi show confirm
        if (!isConfirmExit) {
            showDialogConfirmExit();
        } else {
            // kiem tra co phai dang doi game thi thoat ra khong, neu dung thi cancel game state
            if (isGameStateCancel && gameContent != null) {
                // thay doi trang thai state cancel
                firebaseUtil.cancelGameState(gameContent.id);
            }

            // thoat khoi phong
            if (isQuitGameMember && userItem != null && gameContent != null) {
                firebaseUtil.quitGameMember(gameContent.id, userItem);
            }

            //todo luu lai trang thai pref match
//            if (prefArena != null) {
//                FlowManager.getModelAdapter(SetsPreferenceArena.class).save(prefArena);
//
//                // convert set pref ve dang json
//                String pref = new Gson().toJson(prefArena);
//
//                // gan vao data
//                Intent data = new Intent();
//
//                data.putExtra(Utils.SEND_PREF_TO_FLASHCARD, pref);
//
//                // set result ok va gan data
//                setResult(RESULT_OK, data);
//            }

            super.onBackPressed();
        }
    }

    private void showDialogConfirmExit() {
        DialogUtil.showAlertDialogExit(this, arena_message_confirm_exit, yes, new DialogUtil.AlertDialogCallBack() {
            @Override
            public void actionDone() {
                // show dialog request password
                isConfirmExit = true;

                // thoat kiem tra
                onBackPressed();
            }
        });
    }

    private void showCanceledDialog() {
        DialogUtil.showAlertDialogNotifyWithCallBacks(this, arena_game_state_canceled, button_allow, new DialogUtil.AlertDialogCallBack() {
            @Override
            public void actionDone() {
                // show dialog request password
                isConfirmExit = true;

                // thoat kiem tra
                onBackPressed();
            }
        }, false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // release soundpool sau khi dung xong
        SoundPoolUtil.releaseSoundPool();
    }

    private class shareImageScreenShot extends AsyncTask<Void, Void, Intent> {
        private ProgressDialog pd;
        private String stringLink;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(ArenaActivity.this);
            pd.setMessage(arena_capture_screen);
            pd.setCancelable(false);
            pd.show();

            View rootView = getWindow().getDecorView().findViewById(android.R.id.content);

            stringLink = MediaStore.Images.Media.insertImage(getContentResolver(), getScreenShot(rootView), null, null);
        }

        @Override
        protected Intent doInBackground(Void... voids) {
            if (stringLink == null) return null;

            Uri uri = Uri.parse(stringLink);
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("image/*");

            intent.putExtra(Intent.EXTRA_SUBJECT, "");
            intent.putExtra(Intent.EXTRA_TEXT, "");
            intent.putExtra(Intent.EXTRA_STREAM, uri);

            return intent;
        }

        @Override
        protected void onPostExecute(Intent intent) {
            super.onPostExecute(intent);

            if (intent != null)
                try {
                    startActivity(Intent.createChooser(intent, getString(R.string.action_share_arena)));
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(ArenaActivity.this, "No App Available", Toast.LENGTH_SHORT).show();
                }

            if (pd != null && pd.isShowing()) {
                pd.dismiss();
            }
        }
    }

    public  List<MWord> pickNRandom(List<MWord> lst, int n) {
        List<MWord> copy = new LinkedList<MWord>(lst);
        Collections.shuffle(copy);
        return copy.subList(0, n);
    }

    /**
     * tao luong tao phong va cau hoi
     */
    private class createRoomAndQuestionTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;
        private Lesson lesson;

        public createRoomAndQuestionTask(Lesson lesson){
            this.lesson = lesson;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // hien thi man hinh dang tao cau hoi
            dialog = DialogUtil.showLoadingDialog(ArenaActivity.this, arena_creating_room); // dialog nen da cam
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // tao ma phong
            firebaseUtil.createRandomGameCode(new OnCreateCodeListener() {
                @Override
                public void success(String code, String id) {
                    // thay doi text loading
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.setMessage(arena_creating_question);
                        }
                    });

                    // lay ra danh sach de test
                    currentList.clear();

                    //random + limit
                    currentList =  pickNRandom(lesson.getArrWord(),lesson.getTotal());


                    MPrefence defaultPref = new MPrefence(ArenaActivity.this, MPrefence.NAME_PREF_MAIN);

                          userItem = new User();
                    userItem.username = defaultPref.getStringWithName(MPrefence.username, "");
                    userItem.userId =  (int) defaultPref.getLongWithName(MPrefence.id, -1);

                    // tạo game content
                    gameContent = firebaseUtil.createGameContent(currentList, userItem, id, code, lesson.getName());

                    if (gameContent != null) {
                        // hien thi man hinh waiting voi tu cach la chu phong
                        showWaitingFragment(gameContent.id, gameContent.code, dialog);

                    } else {
                        // thong bao tao phong loi
                        Toast.makeText(ArenaActivity.this, getString(R.string.arena_creating_room_failed), Toast.LENGTH_SHORT).show();
                    }

                }
            });

            return null;
        }
    }
}
