package com.eup.learningenglish.activity.arena.play;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.firebase.listener.OnTTSEnd;
import com.eup.learningenglish.utils.Language;
import com.eup.learningenglish.utils.NetWork;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by linh on 2017/07/27.
 */

public abstract class BaseTTSActivity extends AppCompatActivity implements TextToSpeech.OnUtteranceCompletedListener {
    public static final String requestFileUrl = "http://dws2.voicetext.jp/ASLCLCLVVS/JMEJSYGDCHMSMHSRKPJL/";
    public static final String requestNameUrl = "http://dws2.voicetext.jp/tomcat/servlet/vt";

    private static final int REQUEST_FRONT = 102;
    private static final int REQUEST_BACK = 103;
    public TextToSpeech ttsFront, ttsBack;
    public int resultFront, resultBack;
    public OnTTSEnd onTTSEndFront, onTTSBack;
    public TextView tvlast;
    public ImageView imgLast;
    public String langFront, langBack;
    public MediaPlayer mp = new MediaPlayer();
    public String result, lastText;
    private boolean isCheckFront, isCheckBack, isToastFront, isToastBack;
    private RequestUrlAudio requestUrlAudio;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // khoi tao tts
    public void initTTS(Activity activity, final String langFront, final String langBack) {
        ttsFront = new TextToSpeech(activity, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    // tao local ung voi ngon ngu
                    Locale locale = new Locale(langFront);

                    // set language mac dinh la tieng anh, thay doi sau
                    resultFront = ttsFront.setLanguage(locale);

                    ttsFront.setOnUtteranceCompletedListener(BaseTTSActivity.this);
                }
            }
        });

        ttsBack = new TextToSpeech(activity, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    // tao local ung voi ngon ngu
                    Locale locale = new Locale(langBack);

                    // set language mac dinh la tieng anh, thay doi sau
                    resultBack = ttsBack.setLanguage(locale);

                    ttsBack.setOnUtteranceCompletedListener(BaseTTSActivity.this);
                }
            }
        });
    }

    /*
    =================================================================================================
                        |       set activity       |
    =================================================================================================
    */

    /************************************************************************************************************/

    /**
     * mat truoc
     */
    // doc va to sang chu, icon loa
    public void speakHightLightFront(final TextView txt, final TextView tvBack, final ImageView img, final boolean isPlay2) {

        final int color = getResources().getColor(R.color.colorTextLv1);

        // khi doc ket thuc
        onTTSEndFront = new OnTTSEnd() {
            @Override
            public void finish() {

                // phai chay trong luong chinh
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // set color luc dang doc
                        resetColor(txt, img, color);

                        if (lastText != null && imgLast != null) {
                            resetColor(txt, img, color);
                        }

                        // chay chu mat sau neu ho tro va click = loa
                        if (isPlay2 && resultBack != TextToSpeech.LANG_MISSING_DATA && resultBack != TextToSpeech.LANG_NOT_SUPPORTED) {
                            speakHightLightBack(tvBack, img);
                        }
                    }
                });

            }
        };

        // kiem tra co mang khong
        if (NetWork.isNetWork(this)) {

            // neu co mang thi play audio online
            if (requestUrlAudio != null) {
                requestUrlAudio.cancel(true);
            }

            requestUrlAudio = new RequestUrlAudio(true, txt, img, color, false);
            requestUrlAudio.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Language.normalizeTextSpeech(langFront, txt.getText().toString()), langFront);

        } else {
            // neu khong co mang thi doc bang tts
            playTTSFront(txt, img, color, false);

        }
    }

    private void playTTSFront(final TextView txt, final ImageView img, int color, boolean isSpeakTest) {
        if (ttsFront != null) {

            // result
            if (resultFront == TextToSpeech.LANG_MISSING_DATA
                    || resultFront == TextToSpeech.LANG_NOT_SUPPORTED) {
                // TODO: 2017/07/27 thiet bi khong ho tro ngon ngu mat truoc (cap nhat adapter)
                // neu chua kiem tra de tai ngon ngu
                if (!isCheckFront) {
                    sendIntentCheckLanguage(REQUEST_FRONT);
                } else if (!isToastFront) {
                    Toast.makeText(this, getString(R.string.tts_language_not_supported)
                            , Toast.LENGTH_SHORT).show();

                    isToastFront = true;
                }

            } else {
                if (isSpeakTest) { // neu la doc trong phan test
                    speakOutTest(txt.getText().toString(), txt, "front", ttsFront, color);
                } else {
                    speakOut(txt.getText().toString(), txt, img, "front", ttsFront, color, langFront);
                }
            }
        }
    }

    /************************************************************************************************************/

    /**
     * mat sau
     */
    // doc va to sang chu, icon loa
    public void speakHightLightBack(final TextView txt, final ImageView img) {

        final int color = getResources().getColor(R.color.colorTextSignUpDesc);

        // khi doc ket thuc
        onTTSBack = new OnTTSEnd() {
            @Override
            public void finish() {
                // phai chay trong luong chinh
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // tra ve mau mac dinh truoc khi phat am
                        resetColor(txt, img, color);

                        if (tvlast != null && imgLast != null) {
                            resetColor(tvlast, imgLast, color);
                        }
                    }
                });
            }
        };

        if (NetWork.isNetWork(this)) {
            // neu co mang thi play audio online
            if (requestUrlAudio != null) {
                requestUrlAudio.cancel(true);
            }

            // neu co mang thi play audio online
            requestUrlAudio = new RequestUrlAudio(false, txt, img, color, false);
            requestUrlAudio.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Language.normalizeTextSpeech(langBack, txt.getText().toString()), langBack);

        } else {

            playTTSBack(txt, img, color, false);
        }
    }

    private void playTTSBack(final TextView txt, final ImageView img, int color, boolean isSpeakTest) {
        if (ttsBack != null) {

            // result
            if (resultBack == TextToSpeech.LANG_MISSING_DATA
                    || resultBack == TextToSpeech.LANG_NOT_SUPPORTED) {
                // TODO: 2017/07/27 thiet bi khong ho tro ngon ngu mat sau
                if (!isCheckBack) {
                    sendIntentCheckLanguage(REQUEST_BACK);
                } else if (!isToastBack) {
                    Toast.makeText(this, getString(R.string.tts_language_not_supported)
                            , Toast.LENGTH_SHORT).show();

                    isToastBack = true; // chi thong bao 1 lan
                }
            } else {
                if (isSpeakTest) {
                    speakOutTest(txt.getText().toString(), txt, "back", ttsBack, color);
                } else {
                    speakOut(txt.getText().toString(), txt, img, "back", ttsBack, color, langBack);
                }
            }
        }
    }

    private void speakOut(String txt, TextView textView, ImageView imageView, String UTT_ID, TextToSpeech tts, int color, String languageCode) {
        if (txt != null && tts != null) {

            HashMap<String, String> myHashAlarm = new HashMap<String, String>();
            myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_ALARM));
            myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, UTT_ID);

            boolean isSpeaking = false;

            if (tts.isSpeaking()) { // neu dang doc thi dung lai
                tts.stop();

                if (tvlast != null && imgLast != null) {
                    resetColor(tvlast, imgLast, color);
                }

                resetColor(textView, imageView, color);

                isSpeaking = true;
            }

            if (tvlast != null && tvlast.getText().equals(textView.getText()) && isSpeaking) {
                // neu dang doc doan text do ma click lai thi ngung
            } else {
                // set color luc dang doc
                textView.setTextColor(getResources().getColor(R.color.colorAccentPress));

                // thay doi icon loa dang doc
                imageView.setImageResource(R.drawable.ic_volume_high_p);

                // doc doan van ban
                tts.speak(Language.normalizeTextSpeech(languageCode, txt), TextToSpeech.QUEUE_FLUSH, myHashAlarm);

                // gan lai vao bien tam
                tvlast = textView;
                imgLast = imageView;
            }
        }
    }

    private void resetColor(TextView txt, ImageView img, int colorText) {
        // neu mat sau thi mau lv2
        txt.setTextColor(colorText);

        // thay doi icon loa dang doc
        img.setImageResource(R.drawable.ic_volume_high);
    }

     /*
    =================================================================================================
                        |       set activity       |
    =================================================================================================
    */

    /************************************************************************************************************/

    /**
     * mat truoc
     */
    // doc va to sang chu, icon loa
    public void speakHightLightFrontTest(final TextView txt, final int color) {
        // khi doc ket thuc
        onTTSEndFront = new OnTTSEnd() {
            @Override
            public void finish() {

                // phai chay trong luong chinh
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // set color luc dang doc
                        resetColorTest(txt, color);

                        if (tvlast != null && imgLast != null) {
                            resetColorTest(tvlast, color);
                        }
                    }
                });

            }
        };

        if (NetWork.isNetWork(this)) {

            // neu co mang thi play audio online
            if (requestUrlAudio != null) {
                requestUrlAudio.cancel(true);
            }

            // neu co mang thi play audio online
            requestUrlAudio = new RequestUrlAudio(true, txt, null, color, true);
            requestUrlAudio.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Language.normalizeTextSpeech(langFront, txt.getText().toString()), langFront);

        } else {

            playTTSFront(txt, null, color, true);
        }
    }

    /************************************************************************************************************/

    /**
     * mat sau
     */
    // doc va to sang chu, icon loa
    public void speakHightLightBackTest(final TextView txt, final int color) {

        // khi doc ket thuc
        onTTSBack = new OnTTSEnd() {
            @Override
            public void finish() {
                // phai chay trong luong chinh
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // tra ve mau mac dinh truoc khi phat am
                        resetColorTest(txt, color);

                        if (tvlast != null && imgLast != null) {
                            resetColorTest(tvlast, color);
                        }
                    }
                });
            }
        };

        if (NetWork.isNetWork(this)) {
            // neu co mang thi play audio online
            if (requestUrlAudio != null) {
                requestUrlAudio.cancel(true);
            }

            // neu co mang thi play audio online
            requestUrlAudio = new RequestUrlAudio(false, txt, null, color, true);
            requestUrlAudio.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Language.normalizeTextSpeech(langBack, txt.getText().toString()), langBack);

        } else {

            playTTSBack(txt, null, color, true);
        }

    }

    private void speakOutTest(String txt, TextView textView, String UTT_ID, TextToSpeech tts, int color) {
        if (txt != null && tts != null) {

            HashMap<String, String> myHashAlarm = new HashMap<String, String>();
            myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_ALARM));
            myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, UTT_ID);

            boolean isSpeaking = false;

            if (tts.isSpeaking()) { // neu dang doc thi dung lai
                tts.stop();

                if (tvlast != null) {
                    resetColorTest(tvlast, color);
                }

                resetColorTest(textView, color);

                isSpeaking = true;
            }

            if (tvlast != null && tvlast.getText().equals(textView.getText()) && isSpeaking) {
                // neu dang doc doan text do ma click lai thi ngung
            } else {
                // set color luc dang doc
                textView.setTextColor(getResources().getColor(R.color.colorAccent));
                // doc doan van ban
                tts.speak(txt, TextToSpeech.QUEUE_FLUSH, myHashAlarm);

                // gan lai vao bien tam
                tvlast = textView;
            }

        }
    }

    private void resetColorTest(final TextView txt, int colorText) {
        // neu mat sau thi mau lv2
        txt.setTextColor(colorText);
    }

    @Override
    protected void onDestroy() {
        // tat tts
        if (ttsFront != null) {
            ttsFront.stop();
            ttsFront.shutdown();
        }

        if (ttsBack != null) {
            ttsBack.stop();
            ttsBack.shutdown();
        }

        if (mp != null && mp.isPlaying()) {
            mp.stop();
        }

        super.onDestroy();
    }

    @Override
    public void onUtteranceCompleted(String s) {
        if (s.equals("front")) { // neu doc xong tu o mat truoc
            if (onTTSEndFront != null) {
                onTTSEndFront.finish();
            }
        } else if (s.equals("back")) { // neu doc xong tu o mat sau
            if (onTTSBack != null) {
                onTTSBack.finish();
            }
        }
    }

    /**
     * check tts language supported
     */
    private void sendIntentCheckLanguage(int REQUEST_CODE) {
        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, REQUEST_CODE);
    }

    /**
     * send request download data tts
     */
    private void sendRequestDownLoad() {
        Intent installIntent = new Intent();
        installIntent.setAction(
                TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
        startActivity(installIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case REQUEST_FRONT:
                case REQUEST_BACK:
                    // neu ngon ngu kha dung thi download
                    sendRequestDownLoad();

                    break;
            }
        } else {
            switch (requestCode) {

                case REQUEST_FRONT:
                    // neu ngon ngu kha dung thi download
                    isCheckFront = true;

                    break;

                case REQUEST_BACK:
                    // neu ngon ngu kha dung thi download
                    isCheckBack = true;

                    break;
            }
        }
    }

    /*
    =================================================================================================
                        |       Play audio online       |
    =================================================================================================
    */

    // request phat am
    private class RequestUrlAudio extends AsyncTask<String, Void, Boolean> {

        private boolean isFront, isTest, isCanceled;
        private TextView textView;
        private ImageView imageView;
        private int color;

        private ImageButton imageButton;
        private String text;

        public RequestUrlAudio(boolean isFront, final TextView textView, final ImageView imageView, int color, boolean isTest) {
            this.isFront = isFront;
            this.textView = textView;
            this.imageView = imageView;
            this.color = color;
            this.isTest = isTest;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

            if (tvlast != null) {
                if (imgLast != null) {
                    resetColor(tvlast, imgLast, color);

                } else {
                    resetColorTest(tvlast, color);
                }
            }

            isCanceled = true;
        }

        @Override
        protected Boolean doInBackground(String... strings) {

            if (isCanceled) return false;

            // neu request lai tu cu thi lay luon link cu
            if (lastText != null && lastText.equals(strings[0])) {
                return true;
            }

            boolean isSuccess = false;

            int talkId;
            Random r = new Random();

            switch (strings[1]) { // language
                case "ja":
                    talkId = 300 + r.nextInt(10);
                    break;

                case "en":
                    do {
                        talkId = 100 + r.nextInt(4);
                    } while (talkId == 102);
                    break;

                case "zh-CN":
                    int option = r.nextInt(2);
                    talkId = option == 0 ? 204 : 205;
                    break;

                case "ko":
                    int option1 = r.nextInt(2);
                    talkId = option1 == 0 ? 14 : 18;
                    break;

                default: // neu ngon ngu khac khong support thi doc may
                    return false;
            }

            try {
                String query = "text=" + URLEncoder.encode(strings[0], "UTF-8") + "&talkID=" + talkId + "&volume=100&speed=100&pitch=100&dict=3";

                OkHttpClient.Builder b = new OkHttpClient.Builder();
                b.connectTimeout(1500, TimeUnit.MILLISECONDS);

                OkHttpClient client = b.build();

                String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/53.2.123 Chrome/47.2.2526.123 Safari/537.36";

                MediaType bodyType = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");

                RequestBody body = RequestBody.create(bodyType, query);

                Request request = new Request.Builder()
                        .url(requestNameUrl)
                        .header("UserChat-Agent", USER_AGENT)
                        .addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();

                String str = response.body().string();

                result = requestFileUrl + str.substring(str.indexOf('=') + 1);

                lastText = strings[0];

                isSuccess = true;

            } catch (Exception e) {

            }

            return isSuccess;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if (!isCanceled) {

                if (aBoolean != null && aBoolean) {
                    if (result != null && result != "")
                        new PlayAudioTask(isFront, textView, imageView, color, result, isTest);
                } else {
                    if (isFront) {
                        playTTSFront(textView, imageView, color, isTest);
                    } else {
                        playTTSBack(textView, imageView, color, isTest);
                    }
                }
            }
        }
    }

    /**
     * play audio mediaPlayer
     */
    private class PlayAudioTask {

        private boolean isFront, isTest;

        private TextView textView;

        private ImageView imageView;

        private boolean isSpeaking = false;

        private int color;

        public PlayAudioTask(boolean isFront, final TextView textView, final ImageView imageView, int color, String url, boolean isTest) {
            this.isFront = isFront;
            this.textView = textView;
            this.color = color;
            this.imageView = imageView;
            this.isTest = isTest;

            playAudio(url);
        }


        private void playAudio(String url) {
            try {

                // neu dang phat thi ngung
                if (mp.isPlaying()) {
                    mp.stop();


                    if (tvlast != null) {
                        if (imgLast != null) {
                            resetColor(tvlast, imgLast, color);

                        } else {
                            resetColorTest(tvlast, color);
                        }
                    }

                    if (textView != null) {
                        if (imageView != null) {
                            resetColor(textView, imageView, color);

                        } else {
                            resetColorTest(textView, color);
                        }
                    }

                    isSpeaking = true;
                }

                mp.reset();
                mp.setDataSource(url);
                mp.prepareAsync();
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {

                        if (isFront) { // neu mat truoc
                            if (onTTSEndFront != null)
                                onTTSEndFront.finish();

                        } else { // neu mat sau
                            if (onTTSBack != null)
                                onTTSBack.finish();
                        }
                    }
                });

                mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        // da chuan bi xong
                        if (tvlast != null && tvlast.getText().equals(textView.getText()) && isSpeaking) {
                            // neu dang doc doan text do ma click lai thi ngung
                        } else {
                            // set color luc dang doc
                            if (textView != null) {
                                if (imageView != null) {
                                    textView.setTextColor(getResources().getColor(R.color.colorAccentPress));
                                    // thay doi icon loa dang doc
                                    imageView.setImageResource(R.drawable.ic_volume_high_p);
                                } else {
                                    textView.setTextColor(getResources().getColor(R.color.colorAccent));
                                }
                            }
                            // doc doan van ban
                            mp.start();

                            // gan lai vao bien tam
                            tvlast = textView;
                            imgLast = imageView;
                        }
                    }
                });

            } catch (Exception e) {
                //todo text to speech
                if (isFront) {
                    playTTSFront(textView, imageView, color, isTest);
                } else {
                    playTTSBack(textView, imageView, color, isTest);
                }
            }
        }
    }
}
