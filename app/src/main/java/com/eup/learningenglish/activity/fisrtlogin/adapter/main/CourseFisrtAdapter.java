package com.eup.learningenglish.activity.fisrtlogin.adapter.main;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.MainActivity;
import com.eup.learningenglish.model.Course;
import com.eup.learningenglish.model.Lesson;
import com.eup.learningenglish.model.MWord;
import com.eup.learningenglish.prefence.MPrefence;
import com.eup.learningenglish.utils.ApiMinder;
import com.eup.learningenglish.utils.Avatar;
import com.eup.learningenglish.utils.OkHttpUtils;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HoaDucHung on 2/1/2018.
 */

public class CourseFisrtAdapter extends RecyclerView.Adapter<CourseFisrtAdapter.MyView>  {
    private List<Course> arrData;
    private Activity activity;
    private ProgressDialog progressDialog;
    private int cout;
    private Course courseItem;
    private Course courseData;
    private List<Lesson> arrLesson = new ArrayList<>();
    private MPrefence mPrefence;

    public CourseFisrtAdapter(List<Course> arrData, Activity activity) {
        this.arrData=arrData;
        this.activity = activity;
    }


    @Override
    public CourseFisrtAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_course_first, parent, false);
        mPrefence = new MPrefence(activity,MPrefence.NAME_PREF_MAIN);
        return new CourseFisrtAdapter.MyView(itemView);
    }

    @Override
    public void onBindViewHolder(CourseFisrtAdapter.MyView holder, final int position) {
        final Course courseItems=arrData.get(position);
        holder.imgItem.setImageBitmap(Avatar.getRoundedCornerBitmap(BitmapFactory.decodeResource(activity.getResources(), Avatar.getIDImage(courseItems.getId() + "")), 70));
        holder.tvName.setText(courseItems.getName());
        holder.tvDes.setText(courseItems.getDesc());
        holder.tvWord.setText(courseItems.getWord()+"");
        holder.tvSubject.setText(courseItems.getSubject()+"");

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                courseItem=courseItems;
                // lấy tất cả subject
                new getSubjectOfCourseFisrt().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,courseItems.getId());
            }
        });

    }
    private class getSubjectOfCourseFisrt extends AsyncTask<Integer, Void, List<Lesson>> {
        @Override
        protected List<Lesson> doInBackground(Integer... voids) {
            // request okhttp
            try {
                return OkHttpUtils.getSujectOfCourse(ApiMinder.URL_SUBJECT_OF_COURSE,voids[0]);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Lesson> result) {
            super.onPostExecute(result);
            // lấy xong dữ liệu lưu vào prefence
//            Toast.makeText(activity, result.size()+"", Toast.LENGTH_SHORT).show();
            Log.e("test","size  "+result.size());
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Đang tải...");
            progressDialog.show();
            cout=result.size();

            for(int i=0;i<result.size();i++){
                new getWordOfSubject(result.get(i),i).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,result.get(i).getId());
            }

        }
    }

    private class getWordOfSubject extends AsyncTask<Integer, Void, List<MWord>> {
        public int index;
        public Lesson lesson;
        public getWordOfSubject(Lesson lesson,int index){
            this.index=index;
            this.lesson=lesson;
        }
        @Override
        protected List<MWord> doInBackground(Integer... voids) {
            // request okhttp
            try {
                return OkHttpUtils.getWordOfSuject(ApiMinder.URL_WORD_OF_SUBJECT,voids[0]);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<MWord> result) {
            super.onPostExecute(result);
            arrLesson.add(new Lesson(lesson.getName(),lesson.getId(),lesson.getId_course(),lesson.getTotal(),result));
            Log.e("test","arrLesson size :"+arrLesson.size());
            Log.e("test","index :"+index);
            if(arrLesson.size()==cout){
                courseData = new Course(courseItem.getId(),courseItem.getName(),courseItem.getDesc(),courseItem.getSubject(),courseItem.getWord(),courseItem.getSrclang(),courseItem.getLevel(),arrLesson);
                Log.e("test", courseData.getArrLesson().size()+"");
                Gson gson=new Gson();
                String courseSave=gson.toJson(courseData);
                mPrefence.setStringWithName(MPrefence.stringSubjectOfCourse,courseSave);
                mPrefence.setBoolenWithName(MPrefence.isFirstLogin,false);

                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                Intent intent =new Intent(activity, MainActivity.class);
                intent.putExtra("id_course",courseItem.getId());
                activity.startActivity(intent);
            }
        }
    }


    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class MyView extends RecyclerView.ViewHolder {

        private TextView tvName,tvDes,tvWord,tvSubject;
        private RelativeLayout relativeLayout;
        private ImageView imgItem;

        public MyView(View view) {
            super(view);
            relativeLayout=view.findViewById(R.id.relativeLayout);
            imgItem=view.findViewById(R.id.imgItem);
            tvName =  view.findViewById(R.id.tvNameCourse);
            tvDes=view.findViewById(R.id.tvDesCourse);
            tvWord=view.findViewById(R.id.tvWord);
            tvSubject=view.findViewById(R.id.tvSubject);

        }
    }
}
