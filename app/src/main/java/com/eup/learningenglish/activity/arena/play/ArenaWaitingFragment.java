package com.eup.learningenglish.activity.arena.play;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.firebase.adapter.CustomFirebaseAdapter;
import com.eup.learningenglish.firebase.controller.FirebaseUtil;
import com.eup.learningenglish.firebase.module.User;
import com.eup.learningenglish.firebase.viewholder.UserViewHolder;
import com.eup.learningenglish.utils.SoundPoolUtil;
import com.google.firebase.database.Query;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ArenaWaitingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArenaWaitingFragment extends Fragment {
    private static final String ARG_ID = "idGameContent";
    private static final String ARG_CODE = "gameCode";
    private static final String ARG_USER_ID = "userId";

    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.tvGameCode)
    TextView tvGameCode;
    @BindView(R.id.tvDesc)
    TextView tvDesc;
    @BindView(R.id.btnStart)
    Button btnStart;
    @BindView(R.id.rvListUser)
    RecyclerView mRecycler;
    @BindString(R.string.arena_game_code_number)
    String arena_game_code_number;
    @BindString(R.string.arena_game_code_desc1)
    String arena_game_code_desc1;

    boolean isStarted;
    private String mIdContent;
    private String mCode;
    private int mUserId;
    private OnWaitingArenaListener mListener;
    private FirebaseUtil firebaseUtil;
    private LinearLayoutManager mManager;
    private CustomFirebaseAdapter<User, UserViewHolder> mAdapter;


    public ArenaWaitingFragment() {
        // Required empty public constructor
    }

    public static ArenaWaitingFragment newInstance(String id, String code, int userId) {
        ArenaWaitingFragment fragment = new ArenaWaitingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ID, id);
        args.putString(ARG_CODE, code);
        args.putInt(ARG_USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCode = getArguments().getString(ARG_CODE);
            mIdContent = getArguments().getString(ARG_ID);
            mUserId = getArguments().getInt(ARG_USER_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_arena_waiting, null);

        ButterKnife.bind(this, v);

        firebaseUtil = new FirebaseUtil();

        initDataAndView();

        // play music waiting
        SoundPoolUtil.playMusicWaiting(getActivity());

        return v;
    }

    private void initDataAndView() {

        if (mCode != null) { // nguoi tao phong

            tvGameCode.setText(String.format(arena_game_code_number, mCode));

            btnStart.setText(getString(R.string.arena_start));

        } else { // nguoi tham gia phong

            tvGameCode.setVisibility(View.GONE);

            tvDesc.setText(arena_game_code_desc1);

            btnStart.setText(getString(R.string.arena_quit));
        }

        // tai danh sach nguoi choi
        initRecyclerView();

    }

    /**
     * khoi tao rv theo firebase adapter
     */
    private void initRecyclerView() {
        mRecycler.setHasFixedSize(true);

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mRecycler.setLayoutManager(mManager);

        // Set up FirebaseRecyclerAdapter with the Query
        Query usersQuery = firebaseUtil.getUsersQuery(mIdContent);

        mAdapter = new CustomFirebaseAdapter<User, UserViewHolder>(User.class, R.layout.item_arena_user,
                UserViewHolder.class, usersQuery) {

//            @Override
//            protected void populateViewHolder(final UserViewHolder viewHolder, final UserChat model, final int position) {
////                final DatabaseReference postRef = getRef(position);
//
//                // Bind Post to ViewHolder, setting OnClickListener for the star button
//                viewHolder.bindToPost(model, mUserId, getActivity());
//
//                if (progressBar.getVisibility() == View.VISIBLE) {
//                    progressBar.setVisibility(View.GONE);
//                }
//            }

            @Override
            protected void populateViewHolder(RecyclerView.ViewHolder viewHolder, Object model, int position) {
                super.populateViewHolder(viewHolder, model, position);
                if (viewHolder == null || model == null) return;

                // Bind Post to ViewHolder, setting OnClickListener for the star button
                ((UserViewHolder) viewHolder).bindToPost((User) model, mUserId, getActivity());

                if (progressBar.getVisibility() == View.VISIBLE) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        };

        mRecycler.setAdapter(mAdapter);
    }

    /**
     * on click listener
     */
    @OnClick({R.id.btnStart})
    public void OnClick(View view) {
        switch (view.getId()) {

            case R.id.btnStart:

                // bat dau kiem tra
                onStartPressed();

                break;
        }
    }

    public void onStartPressed() {
        if (mListener != null && !isStarted) { // chi cho click 1 lan
            // neu cua nguoi tao thi bat dau
            if (mCode != null) {
                mListener.onStartArena();
            } else {
                // neu cua nguoi tham gia thi la thoat
                mListener.onQuitArena();
            }

            isStarted = true;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnWaitingArenaListener) {
            mListener = (OnWaitingArenaListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPrepareArenaListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

        SoundPoolUtil.pauseMusicGame();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnWaitingArenaListener {

        // bat dau thi dau
        void onStartArena();

        // thoat khoi phong
        void onQuitArena();

    }

}
