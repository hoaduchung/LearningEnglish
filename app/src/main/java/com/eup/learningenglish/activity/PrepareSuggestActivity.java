package com.eup.learningenglish.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.eup.learningenglish.R;
import com.eup.learningenglish.fragment.SuggestFragment;
import com.eup.learningenglish.model.MWord;
import com.eup.learningenglish.ui.SmartFragmentStatePagerAdapter;
import com.eup.learningenglish.utils.Convert;
import com.eup.learningenglish.utils.NetWork;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

/**
 * Created by HoaDucHung on 3/11/2018.
 */

public class PrepareSuggestActivity extends AppCompatActivity {
    private int pos;
    private List<MWord> arrData;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private TextToSpeech textToSpeech;
    private MediaPlayer player;
    private int result, id_course;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prepare_suggest);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        Intent intent = getIntent();
        pos = intent.getIntExtra("pos",1);
        arrData=new Gson().fromJson(intent.getStringExtra("listword") ,new TypeToken<List<MWord>>() {}.getType());
        id_course= intent.getIntExtra("id_course",1);

        mPager =  findViewById(R.id.pager);
        OverScrollDecoratorHelper.setUpOverScroll(mPager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pos = position;
                new speakTask().execute();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // init tts
        new ttsTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        mPager.setCurrentItem(pos);

    }


    private class ScreenSlidePagerAdapter extends SmartFragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            return SuggestFragment.newInstance(arrData.get(position), id_course);

        }

        @Override
        public int getCount() {
                return arrData.size();
        }
    }


    private class speakTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            speak();
            return null;
        }
    }

    public void speak() {
       MWord mWord = arrData.get(pos);

        if (mWord == null) return;

        if (player == null) {
            player = new MediaPlayer();
        }
        player.reset();

        checkPlayOnlineOrOffline(mWord);

    }

    private void checkPlayOnlineOrOffline(MWord mWord) {
        if (NetWork.isNetWork(this)){
            try {
                String language = "";
                String prefix = String.valueOf(mWord.getId_subject()).substring(0, 3);
                if (prefix.equals("101")) {
                    language = "English";
                } else if (prefix.equals("102")) {
                    language = "Chinese";
                } else if (prefix.equals("103")) {
                    language = "Korea";
                } else if (prefix.equals("104")) {
                    language = "Japanese";
                }
                String url = "http://data.minder.vn/" + language + "/" + id_course + "/audios/" + mWord.getId() + ".mp3";
                player.setDataSource(url);
                player.prepare();
                player.start();
            } catch (IOException e1) {
                onClickSpeak(mWord.getWord());
            }
        } else{
            onClickSpeak(mWord.getWord());

        }
    }

    private class ttsTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            tryTTS();
            return null;
        }
    }

    private void tryTTS() {
        final MWord mWord = arrData.get(pos);
        if (!mWord.getWord().isEmpty() && textToSpeech == null && !isFinishing())
            textToSpeech = new TextToSpeech(PrepareSuggestActivity.this, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        // set language
                        String id_word = String.valueOf(mWord.getId_subject());
                        if (mWord != null && id_word != null) {
                            int lang = Integer.parseInt(id_word.substring(0, 3));
                            switch (lang) {
                                case 101:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.US);
                                    } catch (Exception e) {

                                    }
                                    break;
                                case 104:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.JAPANESE);
                                    } catch (Exception e) {

                                    }
                                    break;
                                case 103:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.KOREAN);
                                    } catch (Exception e) {

                                    }
                                    break;
                                case 102:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.CHINESE);
                                    } catch (Exception e) {

                                    }
                                    break;
                            }
                        } else result = textToSpeech.setLanguage(Locale.US);
                    }
                }
            });
    }

    private void onClickSpeak(String word) {
        if (textToSpeech != null) {
            textToSpeech.setSpeechRate(1.0f);
            // result
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
            } else {
                speakOut(word);
            }
        }
    }

    private void speakOut(String txt) {
        if (txt != null && textToSpeech != null) {
            textToSpeech.speak(Convert.ignoreSpeak(txt), TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    protected void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }

        super.onDestroy();
    }
}

