package com.eup.learningenglish.activity.arena.prepare;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.login.LoginActivity;
import com.eup.learningenglish.prefence.MPrefence;

import butterknife.ButterKnife;


public class ArenaPrepareActivity extends AppCompatActivity {
    private FragmentManager fragmentManager;
    private MPrefence mPrefence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arena_prepare);
        // kiem tra da dang nhap

        mPrefence = new MPrefence(this,MPrefence.NAME_PREF_MAIN);
        ButterKnife.bind(this);

        if(mPrefence.getStringWithName(MPrefence.username,"").isEmpty()){
            Toast.makeText(this, R.string.chucnangdangnhap, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivityForResult(intent,1);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // enable back button
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // fragment mangaer
        fragmentManager = getSupportFragmentManager();

        BottomNavigationView navigation =  findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // default choose JoinFragment
        if (savedInstanceState==null){
            replaceFragmentPrev(PrepareJoinFragment.newInstance());
        }
    }

    // bottom click select fragment
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = fragmentManager.findFragmentById(R.id.content);
            switch (item.getItemId()) {
                case R.id.nav_arena_join:
                    // replace fragment dashboard
                    if (!(fragment instanceof PrepareJoinFragment)) {
                        replaceFragmentPrev(PrepareJoinFragment.newInstance());
                    }
                    return true;
                case R.id.nav_arena_create:
                    // replace fragment face
                    if (!(fragment instanceof PrepareCreateFragment)) {
                        replaceFragmentNext(PrepareCreateFragment.newInstance());
                    }

                    return true;
            }
            return false;
        }

    };

    private void replaceFragmentPrev(Fragment fragment) {
        if (this != null && !this.isFinishing()) {
            int resIn = android.R.anim.slide_in_left;
            int resOut = android.R.anim.slide_out_right;
            fragmentManager.beginTransaction()
                    .setCustomAnimations(resIn, resOut, resIn, resOut)
                    .replace(R.id.content, fragment)
                    .commitAllowingStateLoss();
        }
    }

    private void replaceFragmentNext(Fragment fragment) {
        if (this != null && !this.isFinishing()) {
            int resIn = R.anim.slide_in_right;
            int resOut = R.anim.slide_out_left;
            fragmentManager.beginTransaction()
                    .setCustomAnimations(resIn, resOut, resIn, resOut)
                    .replace(R.id.content, fragment)
                    .commitAllowingStateLoss();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                Log.e("test","onActivityResult");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MPrefence  mPrefence1 = new MPrefence(ArenaPrepareActivity.this,MPrefence.NAME_PREF_MAIN);
        if(mPrefence1.getStringWithName(MPrefence.username,"").isEmpty()){
            this.finish();
        }
    }

}
