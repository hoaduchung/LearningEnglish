package com.eup.learningenglish.activity.fisrtlogin.fragment.selectlanguage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.fisrtlogin.ListCourseFirstLogin;

/**
 * Created by HoaDucHung on 11/20/2017.
 */

public class FragmentLevel extends BaseBackFragment implements View.OnClickListener {
    public static final String LANG="Lang";
    private LinearLayout lnBD,lnSC,lnTC,lnCC;
    private Toolbar mToolbar;
    private String lang;
    private int level;
    private Activity activity;

    public static FragmentLevel newInstance(String language) {
        FragmentLevel fragmentLevel = new FragmentLevel();
        Bundle bundle = new Bundle();
        bundle.putString(LANG,language);
        fragmentLevel.setArguments(bundle);
        return fragmentLevel;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=getActivity();
        Bundle bundle=getArguments();
        if(bundle!=null){
            lang=bundle.getString(LANG);
            Toast.makeText(activity, lang, Toast.LENGTH_SHORT).show();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_level, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        lnBD = view.findViewById(R.id.lnBD);
        lnSC = view.findViewById(R.id.lnSC);
        lnTC = view.findViewById(R.id.lnTC);
        lnCC = view.findViewById(R.id.lnCC);
        lnBD.setOnClickListener(this);
        lnSC.setOnClickListener(this);
        lnTC.setOnClickListener(this);
        lnCC.setOnClickListener(this);
        mToolbar =  view.findViewById(R.id.toolbar);
        initToolbarNav(mToolbar);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.lnBD:
                level=1;
                getIntent(level);
                break;
            case R.id.lnSC:
                level=2;
                getIntent(level);
                break;
            case R.id.lnTC:
                level=3;
                getIntent(level);
                break;
            case R.id.lnCC:
                level=4;
                getIntent(level);
                break;
        }
    }
    public void getIntent(int level){
        Intent intent=new Intent(activity, ListCourseFirstLogin.class);
        intent.putExtra("lang",lang);
        intent.putExtra("level",level);
        startActivity(intent);
    }
}
