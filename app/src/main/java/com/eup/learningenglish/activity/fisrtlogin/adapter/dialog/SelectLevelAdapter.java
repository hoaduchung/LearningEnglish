package com.eup.learningenglish.activity.fisrtlogin.adapter.dialog;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.fisrtlogin.ListCourseFirstLogin;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by HoaDucHung on 2/1/2018.
 */

public class SelectLevelAdapter extends RecyclerView.Adapter<SelectLevelAdapter.MyView>  {

    private List<String> arrLevel;
    private List<Integer> arrCo;
    private Activity activity;
    private int positions;

    public SelectLevelAdapter(List<String> arrLevel, List<Integer> arrCo, Activity activity,int positions) {
        this.arrLevel=arrLevel;
        this.activity = activity;
        this.arrCo=arrCo;
        this.positions=positions;
    }

    @Override
    public SelectLevelAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_level, parent, false);
        return new SelectLevelAdapter.MyView(itemView);
    }

    @Override
    public void onBindViewHolder(SelectLevelAdapter.MyView holder, final int position) {
        holder.txt.setText(arrLevel.get(position).toString());
        holder.img.setImageResource(arrCo.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int lang = 0;
                switch (positions){
                    case 0:
                        lang=101;
                        break;
                    case 1:
                        lang=104;
                        break;
                    case 2:
                        lang=103;
                        break;
                    case 3:
                        lang=102;
                        break;
                }
                int level=0;
                switch (position){
                    case 0:
                        level=1;
                        break;
                    case 1:
                        level=2;
                        break;
                    case 2:
                        level=3;
                        break;
                    case 3:
                        level=4;
                        break;
                }


                if(ListCourseFirstLogin.getOnClick!=null){
                    ListCourseFirstLogin.getOnClick.getClickLevelLanguage(lang,level);
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return arrLevel.size();
    }


    public class MyView extends RecyclerView.ViewHolder {

        private CircleImageView img;
        private TextView txt;


        public MyView(View view) {
            super(view);
            img=view.findViewById(R.id.img);
            txt=view.findViewById(R.id.textview);

        }
    }
}
