package com.eup.learningenglish.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.arena.prepare.ArenaPrepareActivity;
import com.eup.learningenglish.activity.chatroom.ChatRoomActivity;
import com.eup.learningenglish.activity.fisrtlogin.ListCourseFirstLogin;
import com.eup.learningenglish.activity.fisrtlogin.MainSelectLanguage;
import com.eup.learningenglish.activity.login.LoginActivity;
import com.eup.learningenglish.adapter.LessonAdapter;
import com.eup.learningenglish.chathead.chat.ChatHeadService;
import com.eup.learningenglish.chathead.chat.PermissionChecker;
import com.eup.learningenglish.firebase.module.User;
import com.eup.learningenglish.model.Course;
import com.eup.learningenglish.model.Lesson;
import com.eup.learningenglish.prefence.MPrefence;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Logger;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private int id_course;
    private CircleImageView circleImageView;
    private TextView tvNameLesson;
    private TextView numWord;
    private RecyclerView recyclerView;
    private LessonAdapter lessonAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Lesson> arrData = new ArrayList<>();
    private MPrefence mPrefence;
    public static TextToSpeech textToSpeech;
    public static int resultLanguage;
    private TextView tvName;
    private DatabaseReference database;

    private PermissionChecker mPermissionChecker;

    private void startHeadService() {
        startService(new Intent(this, ChatHeadService.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPermissionChecker = new PermissionChecker(this);
        if(!mPermissionChecker.isRequiredPermissionGranted()){
            Intent intent = mPermissionChecker.createRequiredPermissionIntent();
            startActivityForResult(intent, PermissionChecker.REQUIRED_PERMISSION_REQUEST_CODE);
        }

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo("com.eup.learningenglish", PackageManager.GET_SIGNATURES);
            for(Signature signature: packageInfo.signatures){
                MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                messageDigest.update(signature.toByteArray());
                Log.e("test:", Base64.encodeToString(messageDigest.digest(),Base64.DEFAULT));
            }
        }catch (Exception e){

        }

        mPrefence  = new MPrefence(this,MPrefence.NAME_PREF_MAIN);
        mPrefence.setStringWithName(MPrefence.conversationOpenning,"");

        Log.e("test","open :"+  mPrefence.getStringWithName(MPrefence.my_conversation,""));
//        mPrefence.setStringWithName(MPrefence.conversationOpenning,"");

        if(mPrefence.getBoolenWithName(MPrefence.isFirstLogin,true)){
            Intent intent = new Intent(this,MainSelectLanguage.class);
            startActivity(intent);
            this.finish();
        }
        else {
            if (database == null) {
//                FirebaseDatabase.getInstance().setPersistenceEnabled(true);
                database = FirebaseDatabase.getInstance().getReference();
            }

            initDrawable();
            Intent intent = getIntent();
            id_course = intent.getIntExtra("id_course",0);
            if(id_course != 0){
                String course=String.valueOf(id_course).substring(0,3);
                mPrefence.setIntWithName(MPrefence.language,Integer.valueOf(course));
            }
            initData();
            // text to speech
            new ttsTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            String idUser = mPrefence.getStringWithName(MPrefence.idUser,"");
            if( !idUser.isEmpty() && idUser != null && database != null){
                database.child("UserChat").child(idUser).child("online").setValue(true);
            }


        }

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    private class ttsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            tryTTS();
            return null;
        }
    }

    private void tryTTS() {
        if (!isFinishing())
            textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        // set language
                        switch (mPrefence.getIntWithName(MPrefence.language, -1)) {
                            case 101:
                                try {
                                    resultLanguage = textToSpeech.setLanguage(Locale.US);
                                } catch (Exception e) {

                                }
                                break;
                            case 104:
                                try {
                                    resultLanguage = textToSpeech.setLanguage(Locale.JAPANESE);
                                } catch (Exception e) {

                                }
                                break;
                            case 103:
                                try {
                                    resultLanguage = textToSpeech.setLanguage(Locale.KOREAN);
                                } catch (Exception e) {

                                }
                                break;
                            case 102:
                                try {
                                    resultLanguage = textToSpeech.setLanguage(Locale.CHINESE);
                                } catch (Exception e) {

                                }
                                break;
                        }
                    }
                }
            });
    }



    private void initData() {
        String course = mPrefence.getStringWithName(MPrefence.stringSubjectOfCourse,"");
        Course object=  new Gson().fromJson(course,Course.class);
        arrData = object.getArrLesson();
        lessonAdapter = new LessonAdapter(arrData, this);
        recyclerView.setItemAnimator(new FadeInAnimator());
        ScaleInAnimationAdapter scaleAdapter = new ScaleInAnimationAdapter(lessonAdapter);
        scaleAdapter.setFirstOnly(false);
        scaleAdapter.setDuration(500);
        scaleAdapter.setInterpolator(new OvershootInterpolator(1f));
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(scaleAdapter);
        alphaAdapter.setFirstOnly(false);
        alphaAdapter.setDuration(500);
        alphaAdapter.setInterpolator(new OvershootInterpolator(1f));
        recyclerView.setAdapter(alphaAdapter);
//        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 36));

    }


    private void initDrawable() {

        recyclerView  = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);

        tvNameLesson = findViewById(R.id.tvLessonName);
        numWord = findViewById(R.id.tvInfo);
        circleImageView = findViewById(R.id.ivLesson);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView =  findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        initializeCountDrawer(navigationView);

        View v = navigationView.getHeaderView(0);
        tvName = v.findViewById(R.id.tv_userName);
        String name =mPrefence.getStringWithName(MPrefence.username,"");
        if(!name.isEmpty()){
            tvName.setText(name);
        }
        else {
            tvName.setText(R.string.guest);
        }

        tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name2 =mPrefence.getStringWithName(MPrefence.username,"");
                if(!name2.isEmpty()){
                    showDialogQuit(name2,tvName);
                }
                else {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivityForResult(intent,2);
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2) {
            if(resultCode == Activity.RESULT_OK){
                tvName.setText(mPrefence.getStringWithName(MPrefence.username,""));
            }
        }
        if (requestCode == PermissionChecker.REQUIRED_PERMISSION_REQUEST_CODE) {
            if (!mPermissionChecker.isRequiredPermissionGranted()) {
                Toast.makeText(this, "Required permission is not granted. Please restart the app and grant required permission.", Toast.LENGTH_LONG).show();
            } else {
                startHeadService();
            }
        }

    }

    private void showDialogQuit(String name, final TextView textView) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message = getString(R.string.logout_message)+name;
        builder.setTitle(getString(R.string.logout))
                .setMessage(message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        LoginManager.getInstance().logOut();
                        mPrefence.setStringWithName(MPrefence.username,"");
                        textView.setText(R.string.guest);
                        Toast.makeText(MainActivity.this, "Đăng xuất thành công", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // cancel
                        dialog.dismiss();
                    }
                })
                .show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        String idUser = mPrefence.getStringWithName(MPrefence.idUser,"");
        if( !idUser.isEmpty() && idUser != null&& database != null){
            database.child("UserChat").child(idUser).child("online").setValue(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent =new Intent(this, ListCourseFirstLogin.class);
        intent.putExtra("lang","101");
        intent.putExtra("level",1);
        startActivity(intent);
        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
          int id = item.getItemId();
          if (id == R.id.nav_arena) {
              Intent intent = new Intent(this, ArenaPrepareActivity.class);
              startActivity(intent);
          }
          else if(id == R.id.nav_home){
              Intent intent =new Intent(this, ListCourseFirstLogin.class);
              intent.putExtra("lang","101");
              intent.putExtra("level",1);
              startActivity(intent);
          }
          else if(id == R.id.nav_detail){
              Intent intent = new Intent(this, AboutUsActivity.class);
              startActivity(intent);
          }
          else if(id == R.id.nav_exit){
              finishAffinity();
          }
          else if(id == R.id.nav_chat){
              Intent intent = new Intent(this, ChatRoomActivity.class);
              startActivity(intent);
          }

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(Gravity.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }
        return true;
    }

    // them nhan hieu vao item navigation drawer
    private void initializeCountDrawer(NavigationView navigationView) {
        TextView news = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.nav_arena));

        //Gravity property aligns the text
        news.setGravity(Gravity.CENTER_VERTICAL);
        news.setTypeface(null, Typeface.BOLD);
        news.setTextColor(getResources().getColor(R.color.colorAccent));
        news.setText("HOT");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!mPrefence.getStringWithName(MPrefence.username,"").isEmpty()){
            tvName.setText(mPrefence.getStringWithName(MPrefence.username,""));
        }
    }

    @Override
    public void onBackPressed() {
      ActivityCompat.finishAffinity(this);
    }
}
