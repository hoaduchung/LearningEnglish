package com.eup.learningenglish.activity.fisrtlogin;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.fisrtlogin.base.MySupportActivity;
import com.eup.learningenglish.activity.fisrtlogin.base.MySupportFragment;
import com.eup.learningenglish.activity.fisrtlogin.fragment.selectlanguage.FragmentLanguage;
import com.eup.learningenglish.listener.OnFinishMainSelect;
import com.eup.learningenglish.model.Course;
import com.eup.learningenglish.prefence.MPrefence;
import com.eup.learningenglish.utils.ApiMinder;
import com.eup.learningenglish.utils.OkHttpUtils;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

import me.yokeyword.fragmentation.anim.DefaultHorizontalAnimator;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

/**
 * Created by HoaDucHung on 2/1/2018.
 */

public class MainSelectLanguage extends MySupportActivity {
    public static OnFinishMainSelect onFinishMainSelect;
    private MPrefence mPrefence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainselectlanguage);

        mPrefence = new MPrefence(this,MPrefence.NAME_PREF_MAIN);


        // giao diện chọn khóa học
        MySupportFragment fragment = findFragment(FragmentLanguage.class);
        if (fragment == null) {
            loadRootFragment(R.id.fl_container, FragmentLanguage.newInstance());
        }

        onFinishMainSelect = new OnFinishMainSelect() {
            @Override
            public void getFinish() {
                finish();
            }
        };

        // lấy tất cả khóa học về
        new getCourseFisrt().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    private class getCourseFisrt extends AsyncTask<Void, Void, List<Course>> {
        @Override
        protected List<Course> doInBackground(Void... voids) {
            // request okhttp
            try {
                return OkHttpUtils.getCourseFirst(ApiMinder.URL_COURSE);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Course> result) {
            super.onPostExecute(result);
            // lấy xong dữ liệu lưu vào prefence
            Gson gson=new Gson();
            String saveString=gson.toJson(result );
            mPrefence.setStringWithName(MPrefence.stringCourse,saveString);

        }
    }

    @Override
    public FragmentAnimator onCreateFragmentAnimator() {
        return new DefaultHorizontalAnimator();
    }
}

