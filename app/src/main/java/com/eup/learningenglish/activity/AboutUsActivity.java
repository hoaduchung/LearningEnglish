package com.eup.learningenglish.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.eup.learningenglish.BuildConfig;
import com.eup.learningenglish.R;
import com.eup.learningenglish.adapter.OpenSourceAdapter;


public class AboutUsActivity extends AppCompatActivity {
    public static String FACEBOOK_URL = "https://www.facebook.com/hoaduchung";
    private String[] listOpenSource = new String[]{"AutofitTextview", "CircleImageview", "Material-ripple", "MPAndroidChart", "Shimmer", "SimpleCropView", "SwipeMenuListView", "Recyclerview-animators", "Picasso"};
    private RecyclerView rvOpenSource;
    private TextView tvVersion;
//    private RelativeLayout layout_about;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // enable back button
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //find view by id
        rvOpenSource =  findViewById(R.id.rv_open_source);
        tvVersion =  findViewById(R.id.info_version_detail);
//        layout_about = (RelativeLayout) findViewById(R.id.layout_about);

        FloatingActionButton fab =  findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Intent to facebook fan page
                Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
                String facebookUrl = "fb://facewebmodal/f?href="+FACEBOOK_URL;
                facebookIntent.setData(Uri.parse(facebookUrl));
                startActivity(facebookIntent);
            }
        });

        // version
        tvVersion.setText(BuildConfig.VERSION_NAME);

        // list open source
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvOpenSource.setLayoutManager(layoutManager);

        OpenSourceAdapter adapter = new OpenSourceAdapter(listOpenSource, R.layout.item_list_open_source);
        rvOpenSource.setAdapter(adapter);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
