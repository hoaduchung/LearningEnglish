package com.eup.learningenglish.activity.fisrtlogin.fragment.selectlanguage;

import android.support.v7.widget.Toolbar;
import android.view.View;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.fisrtlogin.base.MySupportFragment;


/**
 * Created by YoKeyword on 16/2/7.
 */
public class BaseBackFragment extends MySupportFragment {

    protected void initToolbarNav(Toolbar toolbar) {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pop();
            }
        });
    }
}
