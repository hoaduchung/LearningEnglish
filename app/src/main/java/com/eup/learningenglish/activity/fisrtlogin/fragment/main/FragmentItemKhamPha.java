package com.eup.learningenglish.activity.fisrtlogin.fragment.main;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.fisrtlogin.adapter.MyDividerItemDecoration;
import com.eup.learningenglish.activity.fisrtlogin.adapter.main.CourseFisrtAdapter;
import com.eup.learningenglish.model.Course;
import com.eup.learningenglish.prefence.MPrefence;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

/**
 * Created by HoaDucHung on 2/1/2018.
 */

public class FragmentItemKhamPha extends Fragment {
    private View parentView;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private int lang;
    private Activity activity;
    private int level;
    private MPrefence mPrefence;


    public static FragmentItemKhamPha newInstance(int lang,int level) {
        FragmentItemKhamPha fragment = new FragmentItemKhamPha();
        Bundle bundle = new Bundle();
        bundle.putInt("lang", lang);
        bundle.putInt("level", level);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("lang")) {
            this.lang = getArguments().getInt("lang", 1);
        }
        if (getArguments() != null && getArguments().containsKey("level")) {
            this.level = getArguments().getInt("level", 1);
        }
        activity = getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_course_page, container, false);
        mRecyclerView =  parentView.findViewById(R.id.rv_course);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        OverScrollDecoratorHelper.setUpOverScroll(mRecyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
        mPrefence = new MPrefence(getActivity(),MPrefence.NAME_PREF_MAIN);
        return parentView;
    }


    @Override
    public void onResume() {
        super.onResume();
        // lọc khóa học theo ngôn ngữ
        new getListCourse().execute(lang);
    }


    private class getListCourse extends AsyncTask<Integer, Void, List<Course>> {
        @Override
        protected List<Course> doInBackground(Integer... params) {
            String stringObject=mPrefence.getStringWithName(MPrefence.stringCourse,"");
            Gson gson=new Gson();
            List<Course> arrData=gson.fromJson(stringObject ,new TypeToken<List<Course>>() {
            }.getType());
            return getData(getLang(arrData,lang),level);

        }

        @Override
        protected void onPostExecute(List<Course> courseItemses) {
            super.onPostExecute(courseItemses);
            if (courseItemses != null && courseItemses.size() > 0) {
                // specify an adapter (see also next example)
                mAdapter = new CourseFisrtAdapter(courseItemses,getActivity());
                mRecyclerView.setItemAnimator(new FadeInAnimator());
                ScaleInAnimationAdapter scaleAdapter = new ScaleInAnimationAdapter(mAdapter);
                scaleAdapter.setFirstOnly(false);
                scaleAdapter.setDuration(500);
                scaleAdapter.setInterpolator(new OvershootInterpolator(1f));
                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(scaleAdapter);
                alphaAdapter.setFirstOnly(false);
                alphaAdapter.setDuration(500);
                alphaAdapter.setInterpolator(new OvershootInterpolator(1f));
                mRecyclerView.setAdapter(alphaAdapter);
                mRecyclerView.addItemDecoration(new MyDividerItemDecoration(activity, DividerItemDecoration.VERTICAL, 36));
            } else {
                Toast.makeText(activity, "Dữ liệu rỗng", Toast.LENGTH_SHORT).show();
            }

        }
    }
    // lấy ra khóa học của ngôn ngũ mk chọn
    public List<Course> getLang(List<Course> list, int lang){
        List<Course> listData=new ArrayList<>();
        for(int i=0;i<list.size();i++){
            if(list.get(i).getSrclang().equals(String.valueOf(lang))){
                listData.add(list.get(i));
            }
        }
        return listData;
    }

    // lọc level của ngôn ngữ đó
    public List<Course> getData(List<Course> list,int level){
        List<Course> listData=new ArrayList<>();
        if(level==1){
            for(int i=0;i<list.size();i++){
                int num=list.get(i).getLevel();
                if(num>=0&&num<25){
                    listData.add(list.get(i));
                }
            }
        }
        else if(level==2){
            for(int i=0;i<list.size();i++){
                int num=list.get(i).getLevel();
                if(num>=25&&num<50){
                    listData.add(list.get(i));
                }
            }
        }
        else if(level==3){
            for(int i=0;i<list.size();i++){
                int num=list.get(i).getLevel();
                if(num>=50&&num<75){
                    listData.add(list.get(i));
                }
            }
        }
        else if(level==4){
            for(int i=0;i<list.size();i++){
                int num=list.get(i).getLevel();
                if(num>=75&&num<100){
                    listData.add(list.get(i));
                }
            }
        }

        return listData;

    }


//    private void showDialog() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setCancelable(false)
//                .setMessage(R.string.download_list_course_faile)
//                .setPositiveButton(R.string.try_to, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                        if (NetWork.isNetWork(getActivity()) && CourseNewActivity.onReloadCourse != null) {
//                            CourseNewActivity.onReloadCourse.reload();
//                        } else showDialog();
//                    }
//
//                });
//        builder.create().show();
//    }

}