package com.eup.learningenglish.activity.chatroom;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.fisrtlogin.adapter.MyDividerItemDecoration;
import com.eup.learningenglish.activity.login.LoginActivity;
import com.eup.learningenglish.adapter.ConversationAdapter;
import com.eup.learningenglish.adapter.UserAdapter;
import com.eup.learningenglish.chathead.chat.ChatHeadService;
import com.eup.learningenglish.listener.OnClickUserChat;
import com.eup.learningenglish.listener.OnGetIntent;
import com.eup.learningenglish.model.chat.ListConveration;
import com.eup.learningenglish.model.chat.MyConversation;
import com.eup.learningenglish.model.chat.UserChat;
import com.eup.learningenglish.prefence.MPrefence;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

/**
 * Created by HoaDucHung on 19/04/2018.
 */


public class ChatRoomActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OnClickUserChat, OnGetIntent {
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private RecyclerView rvUser;
    private RecyclerView.LayoutManager mLayoutManager;
    private UserAdapter userAdapter;
    private DatabaseReference database;
    private List<UserChat> arrUserChat = new ArrayList<>();
    private MPrefence mPrefence;
    private RecyclerView rv_conversation;
    private RecyclerView.LayoutManager mLayoutManagerConver;
    private ConversationAdapter conversationAdapter;
    private List<MyConversation> arrConversation = new ArrayList<>();

    private ChatHeadService chatHeadService;
    private boolean bound;
    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {
         @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ChatHeadService.LocalBinder binder = (ChatHeadService.LocalBinder) service;
            chatHeadService = binder.getService();
            bound = true;
            chatHeadService.minimize();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.e("test","=======error");
            bound = false;
        }
    };





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, ChatHeadService.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        setContentView(R.layout.activity_chatroom);
        database = FirebaseDatabase.getInstance().getReference();
        mPrefence = new MPrefence(this,MPrefence.NAME_PREF_MAIN);

        if(mPrefence.getStringWithName(MPrefence.username,"").isEmpty()){
            Toast.makeText(this, R.string.chucnangdangnhap, Toast.LENGTH_SHORT).show();
            Intent intent2 = new Intent(this, LoginActivity.class);
            startActivityForResult(intent2,1);
        }

        /**
         * lấy danh sách tất cả người dùng*/

        database.child("UserChat").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                arrUserChat .clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String data= postSnapshot.getValue()+"";
                    UserChat userChat=  new Gson().fromJson(data, UserChat .class);
                    // loại bỏ mk ra khỏi list
                    if(!mPrefence.getStringWithName(MPrefence.deviceid,"").equals(userChat.getIdDevice())){
                        arrUserChat.add(userChat);
                    }
                    userAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        drawer = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);
        rvUser = findViewById(R.id.rvUser);
        rv_conversation = findViewById(R.id.rv_conversation);

        rvUser.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ChatRoomActivity.this);
        rvUser.setLayoutManager(mLayoutManager);
        OverScrollDecoratorHelper.setUpOverScroll(rvUser, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
        userAdapter = new UserAdapter(arrUserChat ,ChatRoomActivity.this,this);
        rvUser.setAdapter(userAdapter);
        rvUser.addItemDecoration(new MyDividerItemDecoration(ChatRoomActivity.this, DividerItemDecoration.VERTICAL, 36));

        rv_conversation.setHasFixedSize(true);
        mLayoutManagerConver = new LinearLayoutManager(ChatRoomActivity.this);
        rv_conversation.setLayoutManager(mLayoutManagerConver);
        OverScrollDecoratorHelper.setUpOverScroll(rv_conversation, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
        conversationAdapter = new ConversationAdapter(arrConversation ,ChatRoomActivity.this,this,true);
        rv_conversation.setAdapter(conversationAdapter);
        rv_conversation.addItemDecoration(new MyDividerItemDecoration(ChatRoomActivity.this, DividerItemDecoration.VERTICAL, 36));

        setSupportActionBar(toolbar);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initConversation();
    }

    public void initConversation(){

        /**
         * lấy danh sách các cuộc hội thoại đã tham gia từ prefence*/

        final List<MyConversation> arConversation=new Gson().fromJson(mPrefence.getStringWithName(MPrefence.my_conversation,"")
                ,new TypeToken<List<MyConversation>>() {}.getType());
        if(arConversation != null){
            arrConversation.clear();
            arrConversation.addAll(arConversation);
            conversationAdapter.notifyDataSetChanged();
        }

        // lấy danh sách các cuộc hội thoại mà save = false và mk là member, sẽ lấy cả khi mk ofline và online
        Query query = database.child("ListConversationss").orderByChild("save").equalTo("false");
//        Query query = database.child("ListConversationss").orderByChild("idDeviceMember").equalTo("\""+mPrefence.getStringWithName(MPrefence.deviceid,"")+"\"");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    ListConveration listConveration =  new Gson().fromJson(postSnapshot.getValue()+"", ListConveration.class);
                    String myDevice = mPrefence.getStringWithName(MPrefence.deviceid,"");
                    String myDevice2 = listConveration.getIdDeviceMember();
                    if(myDevice2.equals(myDevice)){
                        MyConversation myConversation = new MyConversation(postSnapshot.getKey(),listConveration.getName(),listConveration.getUrlOwner(),listConveration.getIdDeviceOwner());
                        arrConversation.add(myConversation);
                        conversationAdapter.notifyDataSetChanged();
                        List<MyConversation> arrConversation=new Gson().fromJson(mPrefence.getStringWithName(MPrefence.my_conversation,"")
                                ,new TypeToken<List<MyConversation>>() {}.getType());
                        if(arrConversation == null) arrConversation = new ArrayList<>();
                        arrConversation.add(myConversation);
                        mPrefence.setStringWithName(MPrefence.my_conversation,new Gson().toJson(arrConversation));
                        database.child("ListConversationss").child(postSnapshot.getKey()).child("save").setValue("true");
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chatroom, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_view_user) {
            if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                drawer.closeDrawer(Gravity.RIGHT);
            } else {
                drawer.openDrawer(Gravity.RIGHT);
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (drawer.isDrawerOpen(Gravity.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }
        return true;
    }
//
//    public void setData(String value, String key,String idDevice){
//        ListConveration listConveration = new Gson().fromJson(value,ListConveration.class);
//        List<String> arrIdDevice = new ArrayList<>();
//        List<UserConversation> arrUserConversations2 = listConveration.getArrUser();
//        String urlImage="";
//        for(int i=0;i<arrUserConversations2.size();i++){
//            if(!arrUserConversations2.get(i).getIdDevice().equals(mPrefence.getStringWithName(MPrefence.deviceid,""))){
//                urlImage =  arrUserConversations2.get(i).getUrlImage();
//                arrIdDevice.add(arrUserConversations2.get(i).getIdDevice());
//            }
//        }
//        DataConveration dataConveration = new DataConveration(key,listConveration.getName(),urlImage, arrIdDevice);
//        //lấy dữ liệu từ trước
//        List<DataConveration> arrConver = new Gson().fromJson(mPrefence.getStringWithName(MPrefence.join_conversation, "")
//                , new TypeToken<List<DataConveration>>() {
//                }.getType());
//        if (arrConver == null) {
//            arrConver = new ArrayList<>();
//        }
//        arrConver.add(dataConveration);
//        //sau đó lưu lại
//        mPrefence.setStringWithName(MPrefence.join_conversation, new Gson().toJson(arrConver));
//
//        /**cập nhật lên danh sách*/
//        ListConveration object = new Gson().fromJson(value, ListConveration.class);
//        arrConversation.add(object);
//        arrIdConversation.add(key);
//        conversationAdapter.notifyDataSetChanged();
//
//        if(!idDevice.isEmpty() && idDevice != ""){
////            Intent intent = new Intent(ChatRoomActivity.this,ConversationActivity.class);
////            intent.putExtra("key",key);
////            startActivity(intent);
//            List<UserConversation> arrUserConversations = listConveration.getArrUser();
//            String url="";
//            for(int i=0;i<arrUserConversations.size();i++){
//                if(!arrUserConversations.get(i).getIdDevice().equals(mPrefence.getStringWithName(MPrefence.deviceid,""))){
//                    url= arrUserConversations.get(i).getUrlImage();
//                }
//            }
//            getIntentConver(key,url);
//
//        }
//
//    }
//
//    public void getIntentConver(final String key, final String urlImage){
//        // kiểm tra nếu chưa có thì mở, nếu đã có thì mở ra
//
//        final List<String> arrConOpenning=new ArrayList<>();
//        List<String> test = new Gson().fromJson(mPrefence.getStringWithName(MPrefence.conversationOpenning,"") ,new TypeToken<List<String>>() {
//        }.getType());
//        arrConOpenning.clear();
//        if(test!= null)
//            arrConOpenning.addAll(test);
//
//        if (bound) {
//            if(chatHeadService.getIndex()>1){
//                boolean checkOpened = false;
//                int index=0;
//                for(int i=0;i<arrConOpenning.size();i++){
//                    if(arrConOpenning.get(i).equals(key)){
//                        checkOpened = true;
//                        index = i+2;
//                    }
//                }
//                // chưa mở
//                if(checkOpened == false){
//                    arrConOpenning.add(key);
//                    mPrefence.setStringWithName(MPrefence.conversationOpenning,new Gson().toJson(arrConOpenning));
//                    chatHeadService.addChatHead(key,urlImage);
//                    chatHeadService.toggleArrangement();
//                }
//                else {
//                    // đã mở
//                    chatHeadService.toggleArrangement();
//                    chatHeadService.selectChatHead(index);
//
//                }
//            }
//            else {
//                chatHeadService.addChatHead(key,urlImage);
//                chatHeadService.toggleArrangement();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        chatHeadService.addChatHead(key,urlImage);
//                        if(arrConOpenning.isEmpty()){
//                            arrConOpenning.add(key);
//                            mPrefence.setStringWithName(MPrefence.conversationOpenning,new Gson().toJson(arrConOpenning));
//                        }
//                    }
//                },100);
//            }
//        }
//    }


    @Override
    public void getClickUser(MyConversation myConversation, boolean check) {
        Toast.makeText(ChatRoomActivity.this, myConversation.getIdConVersation()+"", Toast.LENGTH_SHORT).show();
        if ( check == true){
            arrConversation.add(myConversation);
            conversationAdapter.notifyDataSetChanged();
        }
//        Intent intent = new Intent(ChatRoomActivity.this,ConversationActivity.class);
//        intent.putExtra("key",myConversation.getIdConVersation());
//        intent.putExtra("urlImage",myConversation.getUrlImage());
//        startActivity(intent);
        if (drawer.isDrawerOpen(Gravity.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }

        getIntentConver(myConversation.getIdConVersation(),myConversation.getUrlImage(),myConversation.getIdDevicePatner());
    }

    // gọi từ conversationadapter
    @Override
    public void getIntentCon(MyConversation myConversation) {
        Toast.makeText(ChatRoomActivity.this, myConversation.getIdConVersation()+"", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(ChatRoomActivity.this,ConversationActivity.class);
//        intent.putExtra("key",myConversation.getIdConVersation());
//        intent.putExtra("urlImage",myConversation.getUrlImage());
//        startActivity(intent);
        getIntentConver(myConversation.getIdConVersation(),myConversation.getUrlImage(),myConversation.getIdDevicePatner());
    }


    public void getIntentConver(final String key, final String urlImage, final String idDeviceY){
        // kiểm tra nếu chưa có thì mở, nếu đã có thì mở ra

        final List<String> arrConOpenning=new ArrayList<>();
        List<String> test = new Gson().fromJson(mPrefence.getStringWithName(MPrefence.conversationOpenning,"") ,new TypeToken<List<String>>() {
        }.getType());
        arrConOpenning.clear();
        if(test!= null)
            arrConOpenning.addAll(test);

        if (bound) {
            if(chatHeadService.getIndex()>1){
                boolean checkOpened = false;
                int index=0;
                for(int i=0;i<arrConOpenning.size();i++){
                    if(arrConOpenning.get(i).equals(key)){
                        checkOpened = true;
                        index = i+2;
                    }
                }
                // chưa mở
                if(checkOpened == false){
                    arrConOpenning.add(key);
                    mPrefence.setStringWithName(MPrefence.conversationOpenning,new Gson().toJson(arrConOpenning));
                    chatHeadService.addChatHead(key,urlImage,idDeviceY);
                    chatHeadService.toggleArrangement();
                }
                else {
                    // đã mở
                    chatHeadService.toggleArrangement();
                    chatHeadService.selectChatHead(index);

                }
            }
            else {
                chatHeadService.addChatHead(key,urlImage,idDeviceY);
                chatHeadService.toggleArrangement();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        chatHeadService.addChatHead(key,urlImage,idDeviceY);
                        if(arrConOpenning.isEmpty()){
                            arrConOpenning.add(key);
                            mPrefence.setStringWithName(MPrefence.conversationOpenning,new Gson().toJson(arrConOpenning));
                        }
                    }
                },100);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                Log.e("test","onActivityResult");
            }
        }
    }

}
