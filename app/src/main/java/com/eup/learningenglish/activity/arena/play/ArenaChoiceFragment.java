package com.eup.learningenglish.activity.arena.play;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.firebase.module.gamecontent.Question;
import com.eup.learningenglish.firebase.module.gamecontent.Settings;
import com.eup.learningenglish.utils.SoundPoolUtil;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ArenaChoiceFragment extends BaseArenaFragment {
    private static final String ARG_JSON_QUESTION = "question";

//    @BindView(R.id.image)
//    ImageView ivQuestion;
    private static final String ARG_JSON_SETTINGS = "settings";
    private static final String ARG_POS = "position";

//    @BindView(R.id.image1)
//    ImageView ivAnswer1;
    private static final String ARG_ID = "idGame";
    private static final String ARG_ID_USER = "idUser";

//    @BindView(R.id.image2)
//    ImageView ivAnswer2;
    // question
    @BindView(R.id.tvQuestion)
    TextView tvQuestion;
    // answer 1
    @BindView(R.id.answer1)
    View answer1;

//    @BindView(R.id.image3)
//    ImageView ivAnswer3;
    @BindView(R.id.tvAnswer1)
    TextView tvAnswer1;
    // answer 2
    @BindView(R.id.answer2)
    View answer2;

//    @BindView(R.id.image4)
//    ImageView ivAnswer4;
    @BindView(R.id.tvAnswer2)
    TextView tvAnswer2;
    // answer 3
    @BindView(R.id.answer3)
    View answer3;
    @BindView(R.id.tvAnswer3)
    TextView tvAnswer3;
    // answer 4
    @BindView(R.id.answer4)
    View answer4;
    @BindView(R.id.tvAnswer4)
    TextView tvAnswer4;
    // chuyen sang cau tiep theo
    boolean isChecked;
    private String mJsonQuestion;
    private String mJsonSettings;
    private int mPosition;
    private Question question;
    private Settings settings;
    private List<View> listViewAnswer = new ArrayList<>();
    private List<TextView> listTextViewAnswer = new ArrayList<>();

    private boolean isShowCorrectAnswer;

    public ArenaChoiceFragment() {
        // Required empty public constructor
    }

    public static ArenaChoiceFragment newInstance(String quest, String settings, int pos) {
        ArenaChoiceFragment fragment = new ArenaChoiceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_JSON_QUESTION, quest);
        args.putString(ARG_JSON_SETTINGS, settings);
        args.putInt(ARG_POS, pos);
//        args.putString(ARG_ID, idGame);
//        args.putInt(ARG_ID_USER, idUser);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mJsonQuestion = getArguments().getString(ARG_JSON_QUESTION);
            mJsonSettings = getArguments().getString(ARG_JSON_SETTINGS);
            mPosition = getArguments().getInt(ARG_POS);
//            mId = getArguments().getString(ARG_ID);
//            mUserId = getArguments().getInt(ARG_ID_USER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_arena_choice, container, false);

        ButterKnife.bind(this, v);

        // init data
        initData();

        return v;
    }

    // lay du lieu tu activity
    private void initData() {

        // lay cau hoi
        question = new Gson().fromJson(mJsonQuestion, Question.class);

        // lay cau hinh arena
        settings = new Gson().fromJson(mJsonSettings, Settings.class);

        // truyen du lieu vao view
        setDataToView();

    }

    /**
     * lay du lieu, boc tach, roi truyen vao view
     */
    private void setDataToView() {
        // kiem tra neu hien thi mat truoc dau tien (cau hoi)

            /*
            cau hoi
             */
        setDataAnswer(tvQuestion, question.quest);

            /*
            * Dap an 1*/
        if (question.answer.size() > 0) {
            setDataAnswer(tvAnswer1, question.answer.get(0));
            // add vao list de thay doi background
            listViewAnswer.add(answer1);
            listTextViewAnswer.add(tvAnswer1);
        } else {
            answer1.setVisibility(View.INVISIBLE);
        }

               /*
            * Dap an 2*/
        if (question.answer.size() > 1) {
            setDataAnswer(tvAnswer2, question.answer.get(1));
            listViewAnswer.add(answer2);
            // add vao list de thay doi background
            listTextViewAnswer.add(tvAnswer2);
        } else {
            answer2.setVisibility(View.INVISIBLE);
        }
               /*
            * Dap an 3*/
        if (question.answer.size() > 2) {
            setDataAnswer(tvAnswer3, question.answer.get(2));
            listViewAnswer.add(answer3);
            listTextViewAnswer.add(tvAnswer3);
        } else {
            answer3.setVisibility(View.INVISIBLE);
        }
               /*
            * Dap an 4*/
        if (question.answer.size() > 3) {
            setDataAnswer(tvAnswer4, question.answer.get(3));
            listTextViewAnswer.add(tvAnswer4);
            listViewAnswer.add(answer4);
        } else {
            answer4.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * set du lieu cho cac cau hoi
     */
    private void setDataAnswer(TextView tvAnswer, String str) {
        tvAnswer.setText(str);
    }

    /*
    =================================================================================================
                        |       onclick listener view choice      |
    =================================================================================================
    */
    @OnClick({R.id.answer1, R.id.answer2, R.id.answer3, R.id.answer4, R.id.tvAnswer1, R.id.tvAnswer2, R.id.tvAnswer3, R.id.tvAnswer4})
    public void OnClick(View v) {
        switch (v.getId()) {

            case R.id.answer1:
            case R.id.tvAnswer1:

                // chon dap an 1

                // thay doi mau layout duoc chon
                changeBackgroundSelected(0);

//                // phat am cau tra loi
//                speakText(tvAnswer1, false); // false la cau tra loi

                // kiem tra dap an va gui len server firebase xem co phai nguoi dau tien tra loi chinh xac
                showAnswer(0);

                break;

            case R.id.answer2:
            case R.id.tvAnswer2:

                // chon dap an 2

                // thay doi mau layout duoc chon
                changeBackgroundSelected(1);

//                // phat am cau tra loi
//                speakText(tvAnswer2, false); // false la cau tra loi

                // kiem tra dap an va gui len server firebase xem co phai nguoi dau tien tra loi chinh xac
                showAnswer(1);

                break;

            case R.id.answer3:
            case R.id.tvAnswer3:

                // chon dap an 3

                // thay doi mau layout duoc chon
                changeBackgroundSelected(2);

//                // phat am cau tra loi
//                speakText(tvAnswer3, false); // false la cau tra loi

                // kiem tra dap an va gui len server firebase xem co phai nguoi dau tien tra loi chinh xac
                showAnswer(2);

                break;

            case R.id.answer4:
            case R.id.tvAnswer4:

                // chon dap an 4

                // thay doi mau layout duoc chon
                changeBackgroundSelected(3);

//                // phat am cau tra loi
//                speakText(tvAnswer4, false); // false la cau tra loi

                // kiem tra dap an va gui len server firebase xem co phai nguoi dau tien tra loi chinh xac
                showAnswer(3);

                break;
        }
    }

    /**
     * hien thi dap an chinh xac hay khong
     */
    private void showAnswer(int position) {
        if (compare2String(listTextViewAnswer.get(position).getText().toString(), question.result)) { // neu nguoi dung tra loi dung

            // da tra loi dung nhung phai gui sang activity de check co phai nguoi dau tien khong
            onCheckAnswer();

            disableAnswer(true);

        } else { // neu tra loi sai

            SoundPoolUtil.playSoundAnswer(false);

            for (int i = 0; i < listTextViewAnswer.size(); i++) {
                // kiem tra neu dung
                if (i == position) {

                    // set background incorrect
                    listViewAnswer.get(i).setBackgroundResource(R.drawable.shape_rectangle_border_incorrect);

                }
            }

            disableAnswer(true);

            // 2s sau moi lai duoc chon lai
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!isShowCorrectAnswer)
                        disableAnswer(false);
                }
            }, 2000);
        }
    }

    public void onCheckAnswer() {
        if (mListener != null && !isChecked) {

            mListener.onCheckAnswer(mPosition);

            isChecked = true;

        }
    }

//    /**
//     * tts
//     */
//    @OnClick({R.id.tvQuestion})
//    public void OnClick1(View v) {
//        switch (v.getId()) {
//            case R.id.tvQuestion:
//
//                // phat am cau hoi
//                speakText(tvQuestion, true); // true la cau hoi
//
//                break;
//        }
//    }

    /**
     * sau khi da kiem tra thi khong chon lai duoc nua nen diable cac dap an
     */
    private void disableAnswer(boolean disable) {
        for (View v : listViewAnswer) {
            if (disable) {
                v.setClickable(false);
            } else {
                v.setClickable(true);
                v.setBackgroundResource(R.drawable.shape_rectangle_border_default);
            }
        }

        for (View v : listTextViewAnswer) {
            if (disable)
                v.setClickable(false);
            else
                v.setClickable(true);
        }
    }

    /**
     * thay doi mau vi tri chon
     *
     * @param position
     */
    private void changeBackgroundSelected(int position) {
        // mau chon la mau vang, con lai la mau xam
        for (int i = 0; i < listViewAnswer.size(); i++) {

            if (i == position) { // cai duoc chon
                listViewAnswer.get(i).setBackgroundResource(R.drawable.shape_rectangle_border_selected);
            } else {
                listViewAnswer.get(i).setBackgroundResource(R.drawable.shape_rectangle_border_unselected);
            }

        }
    }

    public void showCorrectAnswer(boolean isCorrect) {

        SoundPoolUtil.playSoundAnswer(isCorrect);

        // mau chon la mau vang, con lai la mau xam
        for (int i = 0; i < listViewAnswer.size(); i++) {

            if (compare2String(listTextViewAnswer.get(i).getText().toString(), question.result)) { // cai duoc chon
                listViewAnswer.get(i).setBackgroundResource(R.drawable.shape_rectangle_border_correct);
            }
        }

        disableAnswer(true);

        isShowCorrectAnswer = true;
    }


    /**
     * so sanh 2 string co bang nhau khong
     *
     * @param str1
     * @param str2
     * @return
     */

    public boolean compare2String(String str1, String str2) {
        return str1.trim().equalsIgnoreCase(str2.trim());
    }

//    /**
//     * speak text
//     */
//    @BindColor(R.color.colorTextLv1)
//    int colorLv1;
//
//    private void speakText(TextView textView, boolean isQuestion) {
//        if (!setPref.isSettings2()) return; // neu khong bat phat am thanh thi k doc
//
//        if (isQuestion) { // neu la cau hoi
//            if (mListener != null) {
//                mListener.speakAudio(textView, setPref.isSettings1(), colorLv1);
//            }
//        } else { // neu la cau tra loi
//            if (mListener != null) {
//                mListener.speakAudio(textView, !setPref.isSettings1(), colorLv1);
//            }
//        }
//    }

}
