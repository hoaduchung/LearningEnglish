package com.eup.learningenglish.activity.fisrtlogin.adapter.main;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.fisrtlogin.fragment.main.FragmentItemKhamPha;

/**
 * Created by HoaDucHung on 2/1/2018.
 */

public class CourseMinderAdapter extends FragmentStatePagerAdapter {
    private Context context;
    final int PAGE_COUNT = 4;
    private int lang;

    private String[] arrTitle={
            "Bắt đầu","Sơ cấp","Trung cấp","Cao cấp"
    };

    public CourseMinderAdapter(FragmentManager fm, Context context, int lang) {
        super(fm);
        this.context=context;
        this.lang=lang;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return FragmentItemKhamPha.newInstance(lang,1);
            case 1:
                return FragmentItemKhamPha.newInstance(lang,2);
            case 2 :
                return FragmentItemKhamPha.newInstance(lang,3);
            case 3 :
                return FragmentItemKhamPha.newInstance(lang,4);
            default:
                return FragmentItemKhamPha.newInstance(lang,1);
        }

    }


    public View getTabView(int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
        TextView textView = v.findViewById(R.id.tab_title);
        textView.setText(arrTitle[position]);
        return v;
    }





}

