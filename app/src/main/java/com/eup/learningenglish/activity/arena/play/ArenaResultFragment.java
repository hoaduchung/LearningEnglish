package com.eup.learningenglish.activity.arena.play;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.firebase.adapter.DashboardAdapter;
import com.eup.learningenglish.firebase.module.User;
import com.eup.learningenglish.utils.GsonUtil;
import com.eup.learningenglish.utils.SoundPoolUtil;
import com.eup.learningenglish.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;


public class ArenaResultFragment extends BaseArenaFragment {
    private static final String ARG_LIST = "user";
    private static final String ARG_USER_ID = "userid";
    private static final String ARG_USER_NAME = "username";

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvDesc)
    TextView tvDesc;
    @BindView(R.id.rvListUser)
    RecyclerView mRecycler;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.ivArena)
    ImageView ivArena;
    @BindString(R.string.arena_victory)
    String victory;
    @BindString(R.string.arena_congratulation)
    String arena_congratulation;
    @BindString(R.string.arena_draw)
    String draw;
    @BindString(R.string.arena_losse)
    String losse;
    private String mJsonList;
    private int mUserId;
    private String mUserName;
    private List<User> list;
    private DashboardAdapter mAdapter;
    private LinearLayoutManager mManager;

    public ArenaResultFragment() {
        // Required empty public constructor
    }

    public static ArenaResultFragment newInstance(String jsonList, int mUserId, String userName) {
        ArenaResultFragment fragment = new ArenaResultFragment();
        Bundle args = new Bundle();
        args.putString(ARG_LIST, jsonList);
        args.putString(ARG_USER_NAME, userName);
        args.putInt(ARG_USER_ID, mUserId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mJsonList = getArguments().getString(ARG_LIST);
            mUserName = getArguments().getString(ARG_USER_NAME);
            mUserId = getArguments().getInt(ARG_USER_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_arena_result, container, false);

        ButterKnife.bind(this, v);

        // lay du lieu tu activity
        initData();

        return v;
    }

    /**
     * init data man hinh ket qua
     */
    private void initData() {
        // lay ra list user
        list = GsonUtil.convertToListUser(mJsonList);

        // khoi tao title, thay image, bang xep hang...

        // sorted theo thu tu so diem tu cao den thap
        Collections.sort(list, new Utils.ResultArenaComparator());

        // diem so cao nhat
        List<Integer> hightScore = new ArrayList<>();
        int numberHightScore = 0;
        boolean isUserHightScore = false;

        for (int i = 0; i < list.size(); i++) {
            if (hightScore.isEmpty() || hightScore.get(hightScore.size() - 1) != list.get(i).score) {
                hightScore.add(list.get(i).score);
            }

            if (list.get(i).score == hightScore.get(0)) {
                if (list.get(i).userId == mUserId) {
                    isUserHightScore = true;
                }

                numberHightScore++;
            }
        }

        // adapter khong co ai thang
        if (numberHightScore > 1) {
            hightScore.add(0, 0);
        }

        int rank;

        // kiem tra dieu kien thang, hoa
        if (isUserHightScore) {

            if (numberHightScore == 1) {
                // thang cuoc
                rank = 1;

            } else {
                // hoa
                rank = 2;

            }

        } else {
            // thua cuoc
            rank = 3;
        }

        // play music victory, defeat
        SoundPoolUtil.playSoundResult(rank == 1);

        showTitle(rank);

        // khoi tao bang xep hang
        initRecyclerView(list, mUserId, hightScore);
    }

    /**
     * hien thi title va desc
     *
     * @param rank
     */
    private void showTitle(int rank) {
        switch (rank) {
            case 1:
                // thang
                tvTitle.setText(victory);

                String str = String.format(arena_congratulation, mUserName);
                tvDesc.setText(Html.fromHtml(str));
                tvDesc.setVisibility(View.VISIBLE);

                ivArena.setImageResource(R.drawable.ic_trophy_flat);

                break;

            case 2:
                // hoa
                tvTitle.setText(draw);
                ivArena.setImageResource(R.drawable.ic_sad_flat);

                break;

            case 3:
                // thua
                tvTitle.setText(losse);
                ivArena.setImageResource(R.drawable.ic_crying_flat);

                break;
        }

        showIvArena();

    }

    /**
     * show ripple background
     */
    private void showIvArena() {
        // Load the animation like this
        Animation animSlide = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_in_right);

        // Start the animation like this
        ivArena.startAnimation(animSlide);
    }

    /**
     * khoi tao rv theo firebase adapter
     */
    private void initRecyclerView(List<User> list, int mUserId, List<Integer> rank) {
        if (mAdapter == null) {
            mRecycler.setHasFixedSize(true);

            // Set up Layout Manager, reverse layout
            mManager = new LinearLayoutManager(getActivity());
            mRecycler.setLayoutManager(mManager);

            mAdapter = new DashboardAdapter(list, mUserId, getActivity(), rank);
            mRecycler.setAdapter(mAdapter);

            progressBar.setVisibility(View.GONE);
            mRecycler.setVisibility(View.VISIBLE);
        }
    }
}
