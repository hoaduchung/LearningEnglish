package com.eup.learningenglish.activity.fisrtlogin.fragment.selectlanguage;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.fisrtlogin.base.MySupportFragment;

/**
 * Created by HoaDucHung on 11/20/2017.
 */

public class FragmentLanguage extends MySupportFragment implements View.OnClickListener {

    private Toolbar toolbar;
    private LinearLayout lnEn,lnJa,lnKo,lnCh;
    private Activity activity;

    public static FragmentLanguage newInstance() {
        return new FragmentLanguage();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_language, container, false);
        initView(view);
        activity = getActivity();
        return view;
    }

    private void initView(View view) {
        lnEn = view.findViewById(R.id.lnEn);
        lnJa = view.findViewById(R.id.lnJa);
        lnKo = view.findViewById(R.id.lnKo);
        lnCh = view.findViewById(R.id.lnCh);
        lnEn.setOnClickListener(this);
        lnJa.setOnClickListener(this);
        lnCh.setOnClickListener(this);
        lnKo.setOnClickListener(this);
        toolbar = view.findViewById(R.id.toolbar);
    }

    @Override
    public void onNewBundle(Bundle args) {
        super.onNewBundle(args);
        Toast.makeText(_mActivity, args.getString("from"), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.lnEn:
                start(FragmentLevel.newInstance("101"));
                break;
            case R.id.lnJa:
                start(FragmentLevel.newInstance("104"));
                break;
            case R.id.lnKo:
                start(FragmentLevel.newInstance("103"));
                break;
            case R.id.lnCh:
                start(FragmentLevel.newInstance("102"));
                break;
        }
    }
}

