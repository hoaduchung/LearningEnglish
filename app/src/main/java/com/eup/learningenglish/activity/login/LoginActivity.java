package com.eup.learningenglish.activity.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.model.chat.UserChat;
import com.eup.learningenglish.prefence.MPrefence;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

/**
 * Created by HoaDucHung on 21/03/2018.
 */

public class LoginActivity extends AppCompatActivity {
    private EditText editText;
    private AppCompatButton button;
    private DatabaseReference database;
    private ArrayList<String> arrString = new ArrayList<>();
    private MPrefence mPrefence;
    private long timeStart;
    private AppCompatButton  buttonGg;
    private ProgressDialog progressDialogLogin;
    private CallbackManager callbackManager;
    private static final String TAG = "GoogleActivity";
    private static final int RC_SIGN_IN = 9001;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
        callbackManager = CallbackManager.Factory.create();
        buttonGg = findViewById(R.id.sign_in_button);
        editText =  findViewById(R.id.edtInput);
        button = findViewById(R.id.btnOk);
        mPrefence = new MPrefence(this,MPrefence.NAME_PREF_MAIN);
        database= FirebaseDatabase.getInstance().getReference();

        buttonGg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (progressDialogLogin != null) progressDialogLogin = null;
                progressDialogLogin = new ProgressDialog(LoginActivity.this,R.style.AppTheme_Dark_Dialog);
                progressDialogLogin.setIndeterminate(true);
                progressDialogLogin.setMessage(getString(R.string.dang_xac_thuc));
                progressDialogLogin.setCancelable(false);
                progressDialogLogin.show();
                // Google sign out
                mGoogleSignInClient.signOut().addOnCompleteListener(LoginActivity.this,
                        new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                            }
                        });

                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        timeStart = System.currentTimeMillis();
//        database.child("UserChat Name").addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                String a = dataSnapshot.getValue()+"";
//                if(!a.isEmpty()){
//                    arrString.add(a);
//                }
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = editText.getText().toString();
                if( ! str.isEmpty()){
                    // lất devide id
                    String deviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    UserChat userChat = new UserChat("\""+str+"\"","\""+"empty"+"\"","\""+deviceID+"\"",true);
                    String idUser = database.child("UserChat").push().getKey();
                    mPrefence.setStringWithName(MPrefence.idUser,idUser);
                    database.child("UserChat").child(idUser).setValue(userChat);
                    mPrefence.setStringWithName(MPrefence.username,str);
                    mPrefence.setStringWithName(MPrefence.deviceid,deviceID);
                    mPrefence.setStringWithName(MPrefence.urlphoto,"empty");
                    long time = System.currentTimeMillis() - timeStart;
                    mPrefence.setLongWithName(MPrefence.id,time);
                    Toast.makeText(LoginActivity.this, R.string.dangnhapthanhcong, Toast.LENGTH_SHORT).show();
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }
                else{
                    Toast.makeText(LoginActivity.this, R.string.nhapten, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Toast.makeText(this, "lỗi", Toast.LENGTH_SHORT).show();
                if(progressDialogLogin != null && progressDialogLogin.isShowing()) progressDialogLogin.dismiss();

            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            // lất devide id
                            String deviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                            UserChat userChat = new UserChat("\""+user.getDisplayName()+"\"","\""+"https://lh3.googleusercontent.com/"+user.getPhotoUrl().getPath()+"\"","\""+deviceID+"\"",true);
                            String idUser = database.child("UserChat").push().getKey();
                            Log.e("test","idUser :"+ idUser);
                            mPrefence.setStringWithName(MPrefence.idUser,idUser);
                            database.child("UserChat").child(idUser).setValue(userChat);
                            mPrefence.setStringWithName(MPrefence.username,user.getDisplayName());
                            mPrefence.setStringWithName(MPrefence.deviceid,deviceID);
                            mPrefence.setStringWithName(MPrefence.urlphoto,"https://lh3.googleusercontent.com/"+user.getPhotoUrl().getPath());
                            long time = System.currentTimeMillis() - timeStart;
                            mPrefence.setLongWithName(MPrefence.id,time);
                            Toast.makeText(LoginActivity.this, R.string.dangnhapthanhcong, Toast.LENGTH_SHORT).show();
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK,returnIntent);
                            finish();

                        } else {
                            Toast.makeText(LoginActivity.this, "Lỗi login google", Toast.LENGTH_SHORT).show();
                        }
                        if(progressDialogLogin != null && progressDialogLogin.isShowing()) progressDialogLogin.dismiss();

                    }
                });
    }

}
