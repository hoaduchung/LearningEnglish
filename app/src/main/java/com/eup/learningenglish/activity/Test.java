package com.eup.learningenglish.activity;

/**
 * Created by HoaDucHung on 27/05/2018.
 */

public class Test {
    public String a;
    public String b;

    public Test(){}
    public Test(String a, String b) {
        this.a = a;
        this.b = b;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }
}
