package com.eup.learningenglish.activity.arena.play;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.utils.SoundPoolUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ArenaCountDownFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArenaCountDownFragment extends BaseArenaFragment {
    private static final String ARG_USER_NAME = "username";
    @BindView(R.id.tvCountDown)
    TextView tvCountDown;
    @BindView(R.id.tvMessage1)
    TextView tvMessage1;
    private String mUserName;

    private int count = 5;

    public ArenaCountDownFragment() {
        // Required empty public constructor
    }


    public static ArenaCountDownFragment newInstance(String username) {
        ArenaCountDownFragment fragment = new ArenaCountDownFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USER_NAME, username);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserName = getArguments().getString(ARG_USER_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_arena_countdown, null);

        ButterKnife.bind(this, v);

        initDataAndView();

        return v;
    }

    private void initDataAndView() {

        String goodluck = String.format(getString(R.string.arena_count_down_desc2), mUserName);

        tvMessage1.setText(Html.fromHtml(goodluck));

        final Animation scale_alpha_in = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_alpha_in);

        final Handler handler = new Handler();

        // bat dau dem nguoc
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (count > 0) {
                    tvCountDown.setText(String.valueOf(count));
                    tvCountDown.startAnimation(scale_alpha_in);
                    SoundPoolUtil.playSoundTickTock(true);
                    count--;
                    handler.postDelayed(this, 1000);

                } else {
                    SoundPoolUtil.playSoundTickTock(false);
                    onFinishCountDown();
                }
            }
        });

//        CountDownTimer countDownTimer = new CountDownTimer(5000, 1000) {
//            @Override
//            public void onTick(long l) {
//                tvCountDown.setText(String.valueOf(l / 1000 + 1));
//                tvCountDown.startAnimation(scale_alpha_in);
//
//                SoundPoolUtil.playSoundTickTock(true);
//            }
//
//            @Override
//            public void onFinish() {
//
//                SoundPoolUtil.playSoundTickTock(false);
//
//                onFinishCountDown();
//            }
//        };
//
//        countDownTimer.start();
    }

    private void onFinishCountDown() {
        if (mListener != null) {
            mListener.onFinishCountDown();
        }
    }
}
