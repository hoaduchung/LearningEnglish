//package com.eup.learningenglish.activity.chatroom;
//
//import android.os.Bundle;
//import android.os.Handler;
//import android.support.constraint.ConstraintLayout;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.View;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.ProgressBar;
//
//import com.eup.learningenglish.R;
//import com.eup.learningenglish.adapter.MyAdapter;
//import com.eup.learningenglish.model.chat.MyMessage;
//import com.eup.learningenglish.prefence.MPrefence;
//import com.google.firebase.database.ChildEventListener;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.gson.Gson;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.List;
//
//import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
//import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
//
///**
// * Created by HoaDucHung on 02/05/2018.
// */
//
//public class ConversationActivity extends AppCompatActivity {
//    private MPrefence mPrefence;
//    private DatabaseReference database;
//    private String key;
//    private EmojIconActions emojIconActions;
//    private ConstraintLayout constraintLayout;
//    private EmojiconEditText emojiconEditText;
//    private ImageView imageButton;
//    private ImageView emojiButton;
//    private RecyclerView recyclerView;
//    private List<MyMessage> arrData;
//    private LinearLayoutManager linearLayoutManager;
//    private MyAdapter myAdapter;
//    private String android_id;
//    private String urlImage;
//    private ProgressBar progressBar;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_conversation);
//        mPrefence = new MPrefence(this,MPrefence.NAME_PREF_MAIN);
//        database = FirebaseDatabase.getInstance().getReference();
//        key = getIntent().getStringExtra("key");
//        urlImage = getIntent().getStringExtra("urlImage");
//        initFind();
//
//        // get ID Device
//        android_id="\""+ MPrefence.getIDDevice(this)+"\"";
//
//        database.child("MessageChatBoxss").child(key).addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                MyMessage myMessage = new Gson().fromJson(dataSnapshot.getValue().toString(),MyMessage.class);
//                arrData.add(myMessage);
//                myAdapter.notifyDataSetChanged();
//                progressBar.setVisibility(View.GONE);
//                if(arrData.size()!=0){
//                    recyclerView.smoothScrollToPosition(arrData.size()-1);
//                    recyclerView.scrollToPosition(arrData.size()-1);
//                }
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//        imageButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String message="\""+emojiconEditText.getText().toString()+"\"";
//                if(!message.equals("\"\"")){
//                    Calendar c = Calendar.getInstance();
//                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd-MM-yyyy");
//                    String strDate = "\""+sdf.format(c.getTime())+"\"";
//                    MyMessage myMessage = new MyMessage(message,android_id,strDate);
//                    database.child("MessageChatBoxss").child(key).push().setValue(myMessage);
//                    emojiconEditText.setText("");
//                }
//
//            }
//        });
//    }
//
//    private void initFind() {
//        arrData = new ArrayList<>();
//        progressBar = findViewById(R.id.progress);
//        progressBar.setVisibility(View.VISIBLE);
//        recyclerView=findViewById(R.id.recyclerView);
//        imageButton=findViewById(R.id.imgSend);
//        emojiconEditText=findViewById(R.id.edtInput);
//        emojiButton = findViewById(R.id.ivIcon);
//        constraintLayout=findViewById(R.id.conTraint);
//
//        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        recyclerView.setLayoutManager(linearLayoutManager);
//        myAdapter = new MyAdapter(arrData,urlImage,this);
//        recyclerView.setAdapter(myAdapter);
//        emojIconActions = new EmojIconActions(getApplicationContext(),constraintLayout, emojiconEditText, emojiButton);
//        emojIconActions.ShowEmojIcon();
//
//
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if(arrData.size() == 0) progressBar.setVisibility(View.GONE);
//            }
//        },500);
//
//    }
//}
