package com.eup.learningenglish.model.chat;

/**
 * Created by HoaDucHung on 20/04/2018.
 */
/**
 * firebase*/
public class UserChat {
    public String name;
    public String urlPhoto;
    public String idDevice;
    public boolean online;

    public UserChat(String name, String urlPhoto, String idDevice, boolean online) {
        this.name = name;
        this.urlPhoto = urlPhoto;
        this.idDevice = idDevice;
        this.online = online;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public String getIdDevice() {
        return idDevice;
    }

    public void setIdDevice(String idDevice) {
        this.idDevice = idDevice;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}
