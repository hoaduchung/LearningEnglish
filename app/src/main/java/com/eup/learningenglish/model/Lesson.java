package com.eup.learningenglish.model;

import java.util.List;

/**
 * Created by HoaDucHung on 1/30/2018.
 */

public class Lesson {
    public String name;
    public int id;
    public int id_course;
    public int total;
    public List<MWord> arrWord;

    public Lesson(String name, int id, int id_course, int total, List<MWord> arrWord) {
        this.name = name;
        this.id = id;
        this.id_course = id_course;
        this.total = total;
        this.arrWord = arrWord;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_course() {
        return id_course;
    }

    public void setId_course(int id_course) {
        this.id_course = id_course;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<MWord> getArrWord() {
        return arrWord;
    }

    public void setArrWord(List<MWord> arrWord) {
        this.arrWord = arrWord;
    }
    @Override
    public String toString() {
        return name;
    }
}
