package com.eup.learningenglish.model;

import java.util.List;

/**
 * Created by HoaDucHung on 1/30/2018.
 */

public class Course {
    public int id;
    public String name;
    public String desc;
    public int subject;
    public int word;
    public String srclang;
    public int level;
    public List<Lesson> arrLesson;
    public Course(){}

    public Course(int id, String name, String desc, int subject, int word, String srclang, int level, List<Lesson> arrLesson) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.subject = subject;
        this.word = word;
        this.srclang = srclang;
        this.level = level;
        this.arrLesson = arrLesson;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getSubject() {
        return subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public int getWord() {
        return word;
    }

    public void setWord(int word) {
        this.word = word;
    }

    public String getSrclang() {
        return srclang;
    }

    public void setSrclang(String srclang) {
        this.srclang = srclang;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<Lesson> getArrLesson() {
        return arrLesson;
    }

    public void setArrLesson(List<Lesson> arrLesson) {
        this.arrLesson = arrLesson;
    }
}
