package com.eup.learningenglish.model.chat;

/**
 * Created by HoaDucHung on 30/05/2018.
 */
/***
 * dữ liệu được lưu trong prefence bao gồm :
 * id cuộc trò chuyện
 * tên cuộc trò chuyện
 * ảnh cuộc trò chuyện( lấy ảnh của ng kia)
 * idDevice của người kia để check
 */

public class MyConversation {
    public String idConVersation;
    public String nameConversation;
    public String urlImage;
    public String idDevicePatner;
    public MyConversation(){}

    public MyConversation(String idConVersation, String nameConversation, String urlImage, String idDevicePatner) {
        this.idConVersation = idConVersation;
        this.nameConversation = nameConversation;
        this.urlImage = urlImage;
        this.idDevicePatner = idDevicePatner;
    }

    public String getIdConVersation() {
        return idConVersation;
    }

    public void setIdConVersation(String idConVersation) {
        this.idConVersation = idConVersation;
    }

    public String getNameConversation() {
        return nameConversation;
    }

    public void setNameConversation(String nameConversation) {
        this.nameConversation = nameConversation;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getIdDevicePatner() {
        return idDevicePatner;
    }

    public void setIdDevicePatner(String idDevicePatner) {
        this.idDevicePatner = idDevicePatner;
    }
}
