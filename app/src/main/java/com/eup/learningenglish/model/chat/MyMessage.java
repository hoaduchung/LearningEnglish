package com.eup.learningenglish.model.chat;

/**
 * Created by HoaDucHung on 1/13/2018.
 */
/**
 * firebase*/
public class MyMessage {
    public String tinNhan;
    public String idDevice;
    public String time;

    public MyMessage(){}

    public MyMessage(String tinNhan, String idDevice, String time) {
        this.tinNhan = tinNhan;
        this.idDevice = idDevice;
        this.time = time;
    }

    public String getTinNhan() {
        return tinNhan;
    }

    public void setTinNhan(String tinNhan) {
        this.tinNhan = tinNhan;
    }

    public String getIdDevice() {
        return idDevice;
    }

    public void setIdDevice(String idDevice) {
        this.idDevice = idDevice;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
