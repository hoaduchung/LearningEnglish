package com.eup.learningenglish.model;

/**
 * Created by HoaDucHung on 1/30/2018.
 */

public class MWord {
    public int id;
    public int id_subject;
    public String word;
    public String mean;
    public String phonetic;
    public String des;

    public MWord(int id, int id_subject, String word, String mean, String phonetic, String des) {
        this.id = id;
        this.id_subject = id_subject;
        this.word = word;
        this.mean = mean;
        this.phonetic = phonetic;
        this.des = des;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_subject() {
        return id_subject;
    }

    public void setId_subject(int id_subject) {
        this.id_subject = id_subject;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getMean() {
        return mean;
    }

    public void setMean(String mean) {
        this.mean = mean;
    }

    public String getPhonetic() {
        return phonetic;
    }

    public void setPhonetic(String phonetic) {
        this.phonetic = phonetic;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
}
