package com.eup.learningenglish.model.chat;

import java.util.List;

/**
 * Created by HoaDucHung on 22/04/2018.
 */
/**
 * firebase
 * thông tin bao gồm name : tên cuộc trò chuyện
 *  idDeviceMember : id người dc mời
 *  idDeviceOwner : id người tạo phòng
 *  urlOwner : url image người tạo phòng*/
public class ListConveration {
    public String name;
    public String idDeviceMember;
    public String idDeviceOwner;
    public String urlOwner;
    public String save;
    public ListConveration(){}

    public ListConveration(String name, String idDeviceMember, String idDeviceOwner, String urlOwner , String save) {
        this.name = name;
        this.idDeviceMember = idDeviceMember;
        this.idDeviceOwner = idDeviceOwner;
        this.urlOwner = urlOwner;
        this.save = save;
    }


    public String getSave() {
        return save;
    }

    public void setSave(String save) {
        this.save = save;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdDeviceMember() {
        return idDeviceMember;
    }

    public void setIdDeviceMember(String idDeviceMember) {
        this.idDeviceMember = idDeviceMember;
    }

    public String getIdDeviceOwner() {
        return idDeviceOwner;
    }

    public void setIdDeviceOwner(String idDeviceOwner) {
        this.idDeviceOwner = idDeviceOwner;
    }

    public String getUrlOwner() {
        return urlOwner;
    }

    public void setUrlOwner(String urlOwner) {
        this.urlOwner = urlOwner;
    }
}
