package com.eup.learningenglish.listener;

/**
 * Created by HoaDucHung on 2/1/2018.
 */

public interface OnClickLevel {
    void getClickLevelLanguage(int lang, int level);
}
