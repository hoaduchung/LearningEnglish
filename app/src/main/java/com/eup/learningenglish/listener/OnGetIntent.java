package com.eup.learningenglish.listener;

import com.eup.learningenglish.model.chat.MyConversation;

/**
 * Created by HoaDucHung on 04/05/2018.
 */

public interface OnGetIntent {
    void getIntentCon(MyConversation myConversation);
}
