package com.eup.learningenglish.listener;

import com.eup.learningenglish.model.chat.MyConversation;

/**
 * Created by HoaDucHung on 22/04/2018.
 */

public interface OnClickUserChat {
    void getClickUser(MyConversation myConversation, boolean check);
}
