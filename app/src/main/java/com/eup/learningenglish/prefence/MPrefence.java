package com.eup.learningenglish.prefence;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

/**
 * Created by HoaDucHung on 1/30/2018.
 */

public class MPrefence {
    public static String getIDDevice(Context activity){
        return Settings.Secure.getString(activity.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }
    // mainActivity
    public static final String NAME_PREF_MAIN = "MainActivity";

    /*=======================*/
    public static final String isFirstLogin = "isFirstLogin";
    public static final String stringCourse = "stringCourse";
    public static final String stringSubjectOfCourse = "stringSubjectOfCourse";
    public static final String last_subject = "last_subject";
    public static final String language = "langguage";
    public static final String username = "username";
    public static final String deviceid = "deviceid";
    public static final String idUser = "iduser";
    public static final String urlphoto = "urlphoto";
    public static final String last_number_arena="last_number_arena";
    public static final String id = "id";
    public static final String diemcaonhat = "diemcaonhat";
    // lưu tất cả các cuộc hội thoại mà use tham gia
    public static final String my_conversation = "myconversation";
    public static final String conversationOpenning = "conversationOpenning";


    private SharedPreferences preferences;


    public MPrefence(Context activity, String name) {
        preferences = activity.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    // getter
    public int getIntWithName(String name, int defaultValue) {
        return preferences.getInt(name, defaultValue);
    }

    public boolean getBoolenWithName(String name, boolean defaultValue) {
        return preferences.getBoolean(name, defaultValue);
    }

    public String getStringWithName(String name, String defaultValue) {
        return preferences.getString(name, defaultValue);
    }

    public float getFloatWithName(String name, float defaultValue) {
        return preferences.getFloat(name, defaultValue);
    }

    public long getLongWithName(String name, long defaultValue) {
        return preferences.getLong(name, defaultValue);
    }

    // setter
    public void setIntWithName(String name, int defaultValue) {
        preferences.edit().putInt(name, defaultValue).apply();
    }

    public void setBoolenWithName(String name, boolean defaultValue) {
        preferences.edit().putBoolean(name, defaultValue).apply();
    }

    public void setStringWithName(String name, String defaultValue) {
        preferences.edit().putString(name, defaultValue).apply();
    }

    public void setFloatWithName(String name, float defaultValue) {
        preferences.edit().putFloat(name, defaultValue).apply();
    }

    public void setLongWithName(String name, long defaultValue) {
        preferences.edit().putLong(name, defaultValue).apply();
    }

}
