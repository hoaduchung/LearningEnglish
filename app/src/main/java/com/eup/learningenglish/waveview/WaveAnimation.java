package com.eup.learningenglish.waveview;


import android.graphics.Color;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.eup.learningenglish.activity.game.GamesPlayActivity;


/**
 * Created by LinhNT on 3/18/2016.
 */
public class WaveAnimation extends Animation {
    private WaveView waveView;
    private float from;
    private float to;
    private boolean isSetWave;

    public WaveAnimation(WaveView waveView, float from, float to) {
        super();
        this.waveView = waveView;
        this.from = from;
        this.to = to;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        float value = from + (to - from) * interpolatedTime;
        GamesPlayActivity.valueProgress = value;
        if (value > from * 0.5 && !isSetWave) {
            waveView.setWaveColor(Color.parseColor("#1976D2"), Color.parseColor("#2196F3"));
            isSetWave = true;
        } else if (isSetWave && value <= from * 0.5 && value > from * 0.2) {
            waveView.setWaveColor(Color.parseColor("#FFA000"), Color.parseColor("#FFC107"));
            isSetWave = false;
        } else if (!isSetWave && value <= from * 0.2) {
            waveView.setWaveColor(Color.parseColor("#D32F2F"), Color.parseColor("#F44336"));
            isSetWave = true;
        }
        waveView.setProgress(value);
    }

}

