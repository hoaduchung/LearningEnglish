package com.eup.learningenglish.fragment.learn;

import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.learn.FlashcardActivity;
import com.eup.learningenglish.model.MWord;
import com.eup.learningenglish.utils.Convert;
import com.eup.learningenglish.utils.NetWork;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Locale;

/**
 * Created by HoaDucHung on 2/7/2018.
 */

public class FlashcardBehind extends Fragment {
    private TextView tvMean,tvWord,  tvPhonetic ;
    private int pos = 0;
    private int id_course;
    private ImageView ivSpeak, imageView;
    private TextToSpeech textToSpeech;
    private MediaPlayer player;
    private int result;

    public static FlashcardBehind newInstance(int pos,int id_course) {
        FlashcardBehind fragment = new FlashcardBehind();
        Bundle bundle = new Bundle();
        bundle.putInt("position", pos);
        bundle.putInt("idcourse", id_course);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null && getArguments().containsKey("position")) {
            pos = getArguments().getInt("position");
            try {
                if (FlashcardActivity.arrData != null && FlashcardActivity.arrData.size() > 0)
                    new ttsTask().execute();
            } catch (Exception e) {
                Toast.makeText(getActivity(), getString(R.string.error_flashcard), Toast.LENGTH_SHORT).show();
            }
        }
        if (getArguments() != null && getArguments().containsKey("idcourse")) {
            id_course = getArguments().getInt("idcourse");
        }
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_flashcardbehind, container, false);
        tvWord=v.findViewById(R.id.tv_flashcard);
        tvMean = v.findViewById(R.id.tvWord);
        tvPhonetic =  v.findViewById(R.id.tvPhonetic);
        ivSpeak =  v.findViewById(R.id.ivSpeak);
        imageView =  v.findViewById(R.id.image);
        init();
        return v;
    }

    private void init() {

        if (FlashcardActivity.arrData != null && FlashcardActivity.arrData.size() > 0) {
            MWord word = FlashcardActivity.arrData.get(pos);
            String mWord = word.getWord();

            if (word.getWord().length() > 0)
                tvWord.setText(mWord.substring(0, 1).toUpperCase() + mWord.substring(1));


            ivSpeak.setVisibility(View.VISIBLE);
            ivSpeak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new speakTask().execute(FlashcardActivity.arrData.get(pos));
                }
            });
            if(word.getMean()!= null && !word.getMean().equals("")){
                tvMean.setText(word.getMean());
            }
            else {
                tvMean.setVisibility(View.GONE);
            }
            if(word.getPhonetic()!= null && !word.getPhonetic().equals("")){
                tvPhonetic.setText(word.getPhonetic());
            }
            else {
                tvPhonetic.setVisibility(View.GONE);
            }

            if (NetWork.isNetWork(getActivity()) ) {
                String language = "";
                String prefix = String.valueOf(word.getId_subject()).substring(0, 3);
                if (prefix.equals("101")) {
                    language = "English";
                } else if (prefix.equals("102")) {
                    language = "Chinese";
                } else if (prefix.equals("103")) {
                    language = "Korea";
                } else if (prefix.equals("104")) {
                    language = "Japanese";
                }
                String url = "http://data.minder.vn/" + language + "/" + id_course + "/images/words/" + word.getId() + ".jpg";
                Picasso.with(getContext()).load(url).resize(600, 400)
                        .centerInside() .into(imageView);

                }
                else {
                imageView.setVisibility(View.GONE);
            }
        }

        }


    private class speakTask extends AsyncTask<MWord, Void, Void> {

        @Override
        protected Void doInBackground(MWord... params) {
            onClickSpeak(params[0]);
            return null;
        }
    }

    private void onClickSpeak(MWord word) {
        if (player == null) {
            player = new MediaPlayer();
        }
        player.reset();
        playOnlineOrOffline(word);

    }

    private void playOnlineOrOffline(MWord word) {
        if (NetWork.isNetWork(getActivity())) {
            try {
                String language = "";
                String prefix = String.valueOf(word.getId_subject()).substring(0, 3);
                if (prefix.equals("101")) {
                    language = "English";
                } else if (prefix.equals("102")) {
                    language = "Chinese";
                } else if (prefix.equals("103")) {
                    language = "Korea";
                } else if (prefix.equals("104")) {
                    language = "Japanese";
                }
                String url = "http://data.minder.vn/" + language + "/" + id_course + "/audios/" + word.getId() + ".mp3";
                player.setDataSource(url);
                player.prepare();
                player.start();
            } catch (IOException e1) {
                speakTTS(word);
            }
        } else
            speakTTS(word);
    }

    private class ttsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            tryTTS(FlashcardActivity.arrData.get(pos));
            return null;
        }
    }

    private void tryTTS(final MWord word) {
        if (!TextUtils.isEmpty(word.getWord()) && getActivity() != null && !getActivity().isFinishing()) {
            textToSpeech = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        // set language
                        String id_word = String.valueOf(word.getId_subject());
                        if (word != null && id_word != null) {
                            int lang = Integer.parseInt(id_word.substring(0, 3));
                            switch (lang) {
                                case 101:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.US);
                                    } catch (Exception e) {

                                    }
                                    break;
                                case 104:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.JAPANESE);
                                    } catch (Exception e) {

                                    }
                                    break;
                                case 103:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.KOREAN);
                                    } catch (Exception e) {

                                    }
                                    break;
                                case 102:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.CHINESE);
                                    } catch (Exception e) {

                                    }
                                    break;
                            }
                        } else result = textToSpeech.setLanguage(Locale.US);
                    }
                }
            });
        }
    }

    private void speakTTS(MWord word) {
        if (result == TextToSpeech.LANG_MISSING_DATA
                || result == TextToSpeech.LANG_NOT_SUPPORTED) {
            try {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception e) {
            }
        } else {
            speakOut(word.getWord());
        }
    }

    private void speakTTSExample(String example) {
        if (result == TextToSpeech.LANG_MISSING_DATA
                || result == TextToSpeech.LANG_NOT_SUPPORTED) {
            try {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception e) {
            }
        } else {
            speakOut(example);
        }
    }

    private void speakOut(String txt) {
        if (txt != null && textToSpeech != null) {
            textToSpeech.speak(Convert.ignoreSpeak(txt), TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (player != null && player.isPlaying()) {
            player.stop();
            player.release();
        }
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
