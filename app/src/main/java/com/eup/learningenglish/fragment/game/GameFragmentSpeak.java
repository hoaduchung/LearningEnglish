package com.eup.learningenglish.fragment.game;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.game.GamesPlayActivity;
import com.eup.learningenglish.model.MWord;
import com.eup.learningenglish.prefence.MPrefence;
import com.eup.learningenglish.utils.Animation;
import com.eup.learningenglish.utils.Convert;
import com.eup.learningenglish.utils.NetWork;
import com.eup.learningenglish.utils.SoundPoolUtil;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;



public class GameFragmentSpeak extends Fragment implements View.OnClickListener {
    private View v;
    private TextView tvMean, tvPhoneticss;
    private String correct_answer, correct_phonetic;
    private static final int REQ_CODE_SPEECH_INPUT = 12, MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 13;
    private RelativeLayout layout_check;
    private ImageView ivBackgournd;
    private TextView tvAnswer, tvPhonetic, tvGiveUp;
    private int result;
    private TextToSpeech textToSpeech;
    private MediaPlayer player;
    private String mFileName = null;
    private ImageButton playButton;
    private boolean mStartPlaying = true;
    private MWord mWord;
    private int id_course;
    private MPrefence mPrefence;

    public static GameFragmentSpeak newInstance(MWord question, int id_course) {
        GameFragmentSpeak fragment = new GameFragmentSpeak();
        Gson gson = new Gson();
        String data = gson.toJson(question);
        Bundle bundle = new Bundle();
        bundle.putString("data", data);
        bundle.putInt("id_course",id_course);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("data")) {
            Gson gson = new Gson();
            mWord = gson.fromJson(getArguments().getString("data"), MWord.class);
            checkPermission();
            // init tts
            new ttsTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            // init record
            initRecord();
        }
        if (getArguments() != null && getArguments().containsKey("id_course")) {
           id_course= getArguments().getInt("id_course");
        }

        mPrefence = new MPrefence(getActivity(),MPrefence.NAME_PREF_MAIN);

    }

    private void checkPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_game_speak, container, false);
        findViewById();
        init();
        return v;
    }

    private void findViewById() {
        tvPhoneticss = v.findViewById(R.id.tvPhonetics);
        tvMean = (TextView) v.findViewById(R.id.tvMean);
        layout_check = (RelativeLayout) v.findViewById(R.id.layout_check);
        ivBackgournd = (ImageView) v.findViewById(R.id.ivBackground);
        tvAnswer = (TextView) v.findViewById(R.id.tvAnswer);
        tvPhonetic = (TextView) v.findViewById(R.id.tvPhonetic);

        tvGiveUp = (TextView) v.findViewById(R.id.tvGiveUp);
        tvGiveUp.setPaintFlags(tvGiveUp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvGiveUp.setOnClickListener(this);

        playButton = (ImageButton) v.findViewById(R.id.iv_record);
        playButton.setOnClickListener(this);

        v.findViewById(R.id.ibSpeak).setOnClickListener(this);
        v.findViewById(R.id.iv_speak).setOnClickListener(this);
        v.findViewById(R.id.ivSpeak).setOnClickListener(this);
    }

    private void init() {
        correct_answer = Convert.subStringNonVerb(Convert.ignore(mWord.getWord()));
        correct_phonetic = Convert.subStringNonVerb(Convert.ignore(mWord.getPhonetic()));

        if(!mWord.getPhonetic().isEmpty()){
            tvPhoneticss.setText(mWord.getPhonetic());
        }
        else {
            tvPhoneticss.setVisibility(View.GONE);
        }

        if (correct_answer.length() > 0)
            tvMean.setText(correct_answer.substring(0, 1).toUpperCase() + correct_answer.substring(1));

        new speakTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("GAME_PLAY", Context.MODE_PRIVATE);
//        if (sharedPreferences.getBoolean("isKanji", true)) {
//        tvMean.setText(Convert.subStringNonVerb(word.getWord()));
//        } else tvMean.setText(Convert.subStringNonVerb(word.getPhonetic()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            switch (requestCode) {
                case REQ_CODE_SPEECH_INPUT:
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Log.e("test",result.toString());
                    if (!result.isEmpty()) {
                        // TODO: read audio file from inputstream
                        Uri audioUri = data.getData();
                        ContentResolver contentResolver = getActivity().getContentResolver();
                        InputStream filestream = null;
                        try {
                            filestream = contentResolver.openInputStream(audioUri);
                            File file = new File(mFileName);
                            OutputStream output = new FileOutputStream(file);
                            try {
                                try {
                                    byte[] buffer = new byte[4 * 1024]; // or other buffer size
                                    int read;
                                    while ((read = filestream.read(buffer)) != -1) {
                                        output.write(buffer, 0, read);
                                    }
                                    output.flush();
                                } finally {
                                    output.close();
                                }
                            } catch (Exception e) {
                                e.printStackTrace(); // handle exception, define IOException and others
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                if (filestream != null)
                                    filestream.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        // convert result
                        showResult(result);
                        if (quality >= 4) {
                            next();
                        } else {
                            showDialog();
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ivSpeak:
                // speak example
                new speakTask().execute();
                break;
            case R.id.iv_speak:
                hideDialog();
                speak();
                break;
            case R.id.ibSpeak:
                hideDialog();
                speak();
                break;
            case R.id.layout_check:
                hideDialog();
                speak();
                break;
            case R.id.tvGiveUp:
                tvGiveUp.setTextColor(Color.parseColor("#F44336"));
                next();
                break;
            case R.id.iv_record:
                onPlay(mStartPlaying);
                mStartPlaying = !mStartPlaying;
                break;
        }
    }

    private void speak() {
        if (NetWork.isNetWork(getActivity())){
            try {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        if (mPrefence != null)
            switch (mPrefence.getIntWithName(MPrefence.language, -1)) {
                case 101:
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en-US");
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
                    break;
                case 104:
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "ja");
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ja");
                    break;
                case 103:
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "ko");
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ko");
                    break;
                case 102:
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "zh");
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "zh");
                    break;
            }
        else {
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en-US");
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
        }
        intent.putExtra("android.speech.extra.GET_AUDIO_FORMAT", "audio/AMR");
        intent.putExtra("android.speech.extra.GET_AUDIO", true);
        // time waiting record 2 second
//        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS, 3000);
//        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 1500);
//        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, 1500);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getActivity(),
                    getString(R.string.speech_error),
                    Toast.LENGTH_SHORT).show();
        }
            }
            catch (Exception e){
                Toast.makeText(getActivity(), "Vui lòng kiểm tra kết nối mạng", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(getActivity(), "Vui lòng kiểm tra kết nối mạng", Toast.LENGTH_SHORT).show();
        }
    }

    private void next() {
        if (!GamesPlayActivity.isPause()) {
//            Animation.playSound(true);
            // addpoint
            if (quality < 0) quality = 0;
            GamesPlayActivity.addPoint(quality);

            int count = GamesPlayActivity.getCount_question();
            long time = GamesPlayActivity.getTime();
            time = time - 1000;
            if (time < 5000) time = 5000;

            GamesPlayActivity.setTime(time);
            GamesPlayActivity.setValueProgress(100f);
            if (quality > 3)
                GamesPlayActivity.setCount_question(count + 1);

            Collections.shuffle(GamesPlayActivity.arrData);

            GamesPlayActivity.pauseCountDown();
            GamesPlayActivity.createQuestion();
            GamesPlayActivity.startCountDown();
        }
    }

    private void end() {
        // todo some thing here

        if (!GamesPlayActivity.isPause()) {
            SoundPoolUtil.playSound(getActivity(), false);
            GamesPlayActivity.pauseCountDown();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!GamesPlayActivity.isPause()) {
                    GamesPlayActivity.showLayoutFinish();
                }
            }
        }, 1000);
    }

    private void showDialog() {
        ivBackgournd.setImageResource(R.drawable.background_incorrect);
        tvAnswer.setTextColor(getResources().getColor(R.color.text_chuachinhxac1));
        tvPhonetic.setTextColor(getResources().getColor(R.color.text_chuachinhxac1));
        switch (quality) {
            case 4:
                tvAnswer.setText(getString(R.string.almost_true));
                break;
            case 3:
                tvAnswer.setText(getString(R.string.almost_true1));
                break;
            case 2:
                tvAnswer.setText(getString(R.string.almost_true1));
                break;
            case 1:
                tvAnswer.setText(getString(R.string.not_true));
                break;
            case 0:
                tvAnswer.setText(getString(R.string.not_true));
                break;
        }
        tvPhonetic.setText(getString(R.string.press_continue));
        layout_check.setVisibility(View.VISIBLE);
        Animation.startAnimation(getActivity(), layout_check);
        SoundPoolUtil.playSound(getActivity(), false);
    }

    private void hideDialog() {
        Animation.closeAnimation(getActivity(), layout_check);
    }

    private int quality = 0;

    private void showResult(ArrayList<String> result) {
        float percent = 0;
        for (String str : result) {
            float p = Convert.calculataionPercent(correct_answer, str.trim());
            if (mPrefence != null && mPrefence.getIntWithName(MPrefence.language, -1) == 104) {
                float p1 = Convert.calculataionPercent(correct_phonetic, str.trim());
                p = p1 > p ? p1 : p;
                if (percent <= p) {
                    percent = p;
                    Convert.compareSpeak(tvMean, str);
                }
            } else {
                if (percent <= p) {
                    percent = p;
                    Convert.compareSpeak(tvMean, str);
                }
            }
        }
        //dua vao muc do dung de danh gia chat luong cau tra loi
        //tu dua ra muc khac, day chi la test
        if (percent == 100) {
            quality = 5;
        } else if (83 <= percent && percent <= 99) {
            quality = 4;
        } else if (67 <= percent && percent <= 82) {
            quality = 3;
        } else if (40 <= percent && percent <= 66) {
            quality = 2;
        } else if (1 <= percent && percent <= 39) {
            quality = 1;
        } else {
            quality = 0;
        }
    }

    private class speakTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            speakDefault();
            return null;
        }
    }

    private void speakDefault() {
        if (player == null) {
            player = new MediaPlayer();
        }
        player.reset();
        playOnlineOrOffline();

    }

    private void playOnlineOrOffline() {
        if (NetWork.isNetWork(getActivity())) {
            try {
                String language = "";
                String prefix = String.valueOf(mWord.getId_subject()).substring(0, 3);
                if (prefix.equals("101")) {
                    language = "English";
                } else if (prefix.equals("102")) {
                    language = "Chinese";
                } else if (prefix.equals("103")) {
                    language = "Korea";
                } else if (prefix.equals("104")) {
                    language = "Japanese";
                }
                String url = "http://data.minder.vn/" + language + "/" +id_course + "/audios/" + mWord.getId() + ".mp3";
                player.setDataSource(url);
                player.prepare();
                player.start();
            } catch (Exception e1) {
                onClickSpeak();
            }
        } else
            onClickSpeak();
    }

    private class ttsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            tryTTS();
            return null;
        }
    }

    private void tryTTS() {
        if (!TextUtils.isEmpty(mWord.getWord()) && getActivity() != null && !getActivity().isFinishing()) {
            textToSpeech = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        // set language
                        if (mPrefence != null)
                            switch (mPrefence.getIntWithName(MPrefence.language, -1)) {
                                case 101:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.US);
                                    } catch (Exception e) {

                                    }
                                    break;
                                case 104:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.JAPANESE);
                                    } catch (Exception e) {

                                    }
                                    break;
                                case 103:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.KOREAN);
                                    } catch (Exception e) {

                                    }
                                    break;
                                case 102:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.CHINESE);
                                    } catch (Exception e) {

                                    }
                                    break;
                            }
                        else result = textToSpeech.setLanguage(Locale.US);
                    }
                }
            });
        }
    }

    private void onClickSpeak() {
        // result
        if (result == TextToSpeech.LANG_MISSING_DATA
                || result == TextToSpeech.LANG_NOT_SUPPORTED) {
//            Toast.makeText(getActivity(), getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
        } else {
            speakOut(mWord.getWord());
        }
    }

    private void speakOut(String txt) {
        if (txt != null && textToSpeech != null) {
            textToSpeech.speak(Convert.ignoreSpeak(txt), TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        if (player != null) {
            player.release();
            player = null;
        }
    }



    private void onPlay(boolean start) {
        if (start) {
            startPlaying();
        } else {
            stopPlaying();
        }
    }

    private void startPlaying() {
        player = new MediaPlayer();
        try {
            player.setDataSource(mFileName);
            player.prepare();
            player.start();
            GamesPlayActivity.pauseCountDown();
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    GamesPlayActivity.startCountDown();
                }
            });
        } catch (IOException e) {
            Toast.makeText(getActivity(), getString(R.string.no_record), Toast.LENGTH_SHORT).show();
        }
    }

    private void stopPlaying() {
        player.release();
        player = null;
    }

    public void initRecord() {
        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/audiorecordtest.3gp";
        File file = new File(mFileName);
        if (file.exists()) file.delete();
    }
}

