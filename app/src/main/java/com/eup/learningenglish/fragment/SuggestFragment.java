package com.eup.learningenglish.fragment;

import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.model.MWord;
import com.eup.learningenglish.utils.Convert;
import com.eup.learningenglish.utils.NetWork;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.Locale;

/**
 * Created by HoaDucHung on 3/11/2018.
 */

public class SuggestFragment extends Fragment implements View.OnClickListener {

    private TextToSpeech textToSpeech;
    private MediaPlayer player;
    private int result;
    private MWord mWord;
    private TextView tvWord, tvMean , tvPhonetic;
    private ImageView imageView;
    private int id_course;

    public SuggestFragment() {
        // Required empty public constructor
    }

    public static SuggestFragment newInstance(MWord mWord, int id_course) {
        SuggestFragment fragment = new SuggestFragment();
        Gson gson = new Gson();
        String data = null;
        try {
            data = gson.toJson(mWord);
        } catch (OutOfMemoryError e) {
        }
        Bundle bundle = new Bundle();
        bundle.putString("data", data);
        bundle.putInt("id_course",id_course);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("data")) {
            if (getArguments().getString("data") != null) {
                Gson gson = new Gson();
                mWord = gson.fromJson(getArguments().getString("data"), MWord.class);
            }
        }
        if (getArguments() != null && getArguments().containsKey("id_course")) {
                id_course =getArguments().getInt("id_course");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_suggest, container, false);
        if (mWord == null) return v;
        tvWord =  v.findViewById(R.id.tvWord);
        tvPhonetic =  v.findViewById(R.id.tvPhonetic);
        tvMean =  v.findViewById(R.id.tvMean);
        imageView =  v.findViewById(R.id.ivSuggest);
        int fontSize = 100;
        tvWord.setTextSize(TypedValue.COMPLEX_UNIT_PX, (getActivity().getResources().getDimension(R.dimen.sp24) * fontSize) / 100);
        tvPhonetic.setTextSize(TypedValue.COMPLEX_UNIT_PX, (getActivity().getResources().getDimension(R.dimen.sp18) * fontSize) / 100);
        tvMean.setTextSize(TypedValue.COMPLEX_UNIT_PX, (getActivity().getResources().getDimension(R.dimen.sp20) * fontSize) / 100);
        v.findViewById(R.id.ivSpeak).setOnClickListener(this);
        // ini tts
        new ttsTask().execute();
        if (mWord != null) {
            String str = mWord.getWord();
            if (str.length() > 0)
                tvWord.setText(str.substring(0, 1).toUpperCase() + str.substring(1));
            if (mWord.getPhonetic() != null && !mWord.getPhonetic().equals("")) {
                tvPhonetic.setText(mWord.getPhonetic());
                tvPhonetic.setVisibility(View.VISIBLE);
            } else {
                tvPhonetic.setVisibility(View.GONE);
            }
            tvMean.setText(mWord.getMean());

        }

        Transformation transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(4)
                .oval(false)
                .build();

            if (NetWork.isNetWork(getActivity())){
                String language = "";
                String prefix = String.valueOf(mWord.getId_subject()).substring(0, 3);
                if (prefix.equals("101")) {
                    language = "English";
                } else if (prefix.equals("102")) {
                    language = "Chinese";
                } else if (prefix.equals("103")) {
                    language = "Korea";
                } else if (prefix.equals("104")) {
                    language = "Japanese";
                }
                String url = "http://data.minder.vn/" + language + "/" + id_course + "/images/words/" + mWord.getId() + ".jpg";
                Picasso.with(getContext()).load(url).transform(transformation).into(imageView);
            } else {
                    imageView.setVisibility(View.GONE);
        }

        return v;
    }


    private class speakTask extends AsyncTask<Boolean, Void, Void> {

        @Override
        protected Void doInBackground(Boolean... params) {
            speak(params[0]);
            return null;
        }
    }

    public void speak(boolean isClick) {
        if (player == null) {
            player = new MediaPlayer();
        }
        player.reset();
        playOnlineOrOffline(isClick);

    }

    private void playOnlineOrOffline(boolean isClick) {
        if (NetWork.isNetWork(getActivity())) {
            try {
                String language = "";
                String prefix = String.valueOf(mWord.getId_subject()).substring(0, 3);
                if (prefix.equals("101")) {
                    language = "English";
                } else if (prefix.equals("102")) {
                    language = "Chinese";
                } else if (prefix.equals("103")) {
                    language = "Korea";
                } else if (prefix.equals("104")) {
                    language = "Japanese";
                }
                String url = "http://data.minder.vn/" + language + "/" + id_course + "/audios/" + mWord.getId() + ".mp3";
                player.setDataSource(url);
                player.prepare();
                player.start();
            } catch (Exception e1) {
                onClickSpeak(isClick);
            }
        } else
            onClickSpeak(isClick);
    }

    private class ttsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            tryTTS();
            return null;
        }
    }

    private void tryTTS() {
        if (!TextUtils.isEmpty(mWord.getWord()) && getActivity() != null && !getActivity().isFinishing()) {
            textToSpeech = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        // set language
                        String id_word = String.valueOf(mWord.getId_subject());
                        if (mWord != null && id_word != null) {
                            int lang = Integer.parseInt(id_word.substring(0, 3));
                            switch (lang) {
                                case 101:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.US);
                                    } catch (Exception e) {

                                    }
                                    break;
                                case 104:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.JAPANESE);
                                    } catch (Exception e) {

                                    }
                                    break;
                                case 103:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.KOREAN);
                                    } catch (Exception e) {

                                    }
                                    break;
                                case 102:
                                    try {
                                        result = textToSpeech.setLanguage(Locale.CHINESE);
                                    } catch (Exception e) {

                                    }
                                    break;
                            }
                        } else {
                            result = textToSpeech.setLanguage(Locale.US);
                        }
                    } else {
                        result = TextToSpeech.ERROR;
                    }

                }
            });
        }
    }

    private void onClickSpeak(boolean isClick) {
        if (textToSpeech == null) tryTTS();

        if (result == TextToSpeech.LANG_MISSING_DATA
                || result == TextToSpeech.LANG_NOT_SUPPORTED) {
            if (isClick)
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), getActivity().getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception e) {
                }
        } else {
            if(textToSpeech!=null&&tvWord!=null){
                textToSpeech.setSpeechRate(1.0f);
                speakOut(Convert.subString(tvWord.getText().toString()));
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivSpeak:
                new speakTask().execute(true);
                break;
        }
    }

    private void speakOut(String txt) {
        if (txt != null && textToSpeech != null) {
            textToSpeech.speak(Convert.ignoreSpeak(txt), TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (player != null) {
            player.release();
            player = null;
        }
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }
}