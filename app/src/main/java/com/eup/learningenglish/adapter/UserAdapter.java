package com.eup.learningenglish.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.listener.OnClickUserChat;
import com.eup.learningenglish.model.chat.ListConveration;
import com.eup.learningenglish.model.chat.MyConversation;
import com.eup.learningenglish.model.chat.UserChat;
import com.eup.learningenglish.prefence.MPrefence;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by HoaDucHung on 1/14/2018.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyView>  {

    private OnClickUserChat onClickUserChat;
    private Activity activity;
    private List<UserChat> arrData;
    private DatabaseReference database;
    private MPrefence mPrefence;

    public UserAdapter(List<UserChat> arrData, Activity activity,OnClickUserChat onClickUserChat) {
        this.activity=activity;
        this.arrData=arrData;
        this.onClickUserChat = onClickUserChat;
    }

    @Override
    public UserAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_userchat, parent, false);
        database = FirebaseDatabase.getInstance().getReference();
        mPrefence = new MPrefence(activity,MPrefence.NAME_PREF_MAIN);
        return new UserAdapter.MyView(itemView);
    }

    @Override
    public void onBindViewHolder(UserAdapter.MyView holder, final int position) {
        final UserChat userChat = arrData.get(position);
        holder.tvName.setText(userChat.getName());
        if(userChat.isOnline()){
            holder.imgOnline.setVisibility(View.VISIBLE);
        }
        else {
            holder.imgOnline.setVisibility(View.GONE);
        }
        Picasso.with(activity).load(userChat.getUrlPhoto()).placeholder(activity.getResources().getDrawable(R.drawable.ic_user)).into(holder.imageView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 String b =  mPrefence.getStringWithName(MPrefence.my_conversation,"");
                 if(! b.isEmpty()){
                     List<MyConversation> arrData=new Gson().fromJson(b ,new TypeToken<List<MyConversation>>() {}.getType());
                     boolean checkUser = false;
                     int index = 0;
                     // kiểm tra đã có cuộc hội thoại nào với user này chưa
                     for(int i=0;i<arrData.size();i++){
                         if(arrData.get(i).getIdDevicePatner().equals(userChat.getIdDevice())){
                            checkUser = true;
                            index  = i;
                            break;
                         }
                     }

                     if(checkUser == true){
                        // người này đã có
                         onClickUserChat.getClickUser(arrData.get(index),false);
                     }
                     else {
                         // người này chưa chat
                         getNewConversation(userChat);
                     }
                 }
                 else {
                      Log.e("test","new conversation");
                      getNewConversation(userChat);
                 }


            }
        });
    }

    public void getNewConversation(UserChat userChat){
        /** Tạo hội thoại mới*/
        String[] myName= mPrefence.getStringWithName(MPrefence.username,"").split(" ");
        String[] yourName= userChat.getName().split(" ");
        String nameConversation= myName[0]+", "+yourName[0];
        String urlConversation = userChat.getUrlPhoto();
        String idDevicePatner = userChat.getIdDevice();
        String idConversation =database.child("ListConversationss").push().getKey();
        MyConversation myConversation = new MyConversation(idConversation,nameConversation,urlConversation,idDevicePatner);
        List<MyConversation> arrConversation=new Gson().fromJson(mPrefence.getStringWithName(MPrefence.my_conversation,"")
                ,new TypeToken<List<MyConversation>>() {}.getType());
        if(arrConversation == null) arrConversation = new ArrayList<>();
        arrConversation.add(myConversation);
        mPrefence.setStringWithName(MPrefence.my_conversation,new Gson().toJson(arrConversation));

        ListConveration listConveration = new ListConveration( "\""+nameConversation+"\"","\""+idDevicePatner+"\"",
                "\""+mPrefence.getStringWithName(MPrefence.deviceid,"")+"\"","\""+mPrefence.getStringWithName(MPrefence.urlphoto,"")+"\"","false");
        database.child("ListConversationss").child(idConversation).setValue(listConveration);
        onClickUserChat.getClickUser(myConversation, true);
    }


    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class MyView extends RecyclerView.ViewHolder {
          private CircleImageView imageView;
          private ImageView imgOnline;
          private TextView tvName;
        public MyView(View view) {
            super(view);
                 imageView = view.findViewById(R.id.imgImage);
                 imgOnline = view.findViewById(R.id.imgOnline);
                 tvName = view.findViewById(R.id.tvName);
        }
    }


}
