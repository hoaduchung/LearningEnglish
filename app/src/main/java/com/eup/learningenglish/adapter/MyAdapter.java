package com.eup.learningenglish.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.eup.learningenglish.R;
import com.eup.learningenglish.model.chat.MyMessage;
import com.eup.learningenglish.prefence.MPrefence;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by HoaDucHung on 1/12/2018.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyView>  {
    private Context activity;
    private List<MyMessage> arrData;
    private String device;
    private String url;

    // khi gọi phương thức này thì n sẽ tải lại toàn bộ adapter
    public MyAdapter(List<MyMessage> arrData,String device,String url, Context activity) {
        this.activity=activity;
        this.url= url;
        this.arrData=arrData;
        this.device = device;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        switch (viewType){
            case 0:
                itemView =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ysend, parent, false);
                break;
            case 1:
                // là của mk
                itemView =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_msend, parent, false);
                break;
        }
        return new MyView(itemView);
    }


    @Override
    public void onBindViewHolder(MyView holder, final int position) {
        if(getItemViewType(position)==1){
            holder.tvMe.setText(arrData.get(position).getTinNhan());
            //set time
            if(arrData.size()>0){
                if(position<arrData.size()-1){
                    if (getItemViewType(position + 1) == 0){
                        holder.tvMyTime.setText(arrData.get(position).getTime().split(" ")[0]);
                        holder.tvMyTime.setVisibility(View.VISIBLE);
                    }
                    else holder.tvMyTime.setVisibility(View.GONE);
                }
                if(position == arrData.size()-1){
                    holder.tvMyTime.setText(arrData.get(position).getTime().split(" ")[0]);
                    holder.tvMyTime.setVisibility(View.VISIBLE);
                }
            }
        }
        else {
            // check lỗi của firebase
            if(device.equals(arrData.get(position).getIdDevice())){
                holder.tvYou.setText(arrData.get(position).getTinNhan());
                // set anh
                if(position == 0){
                    holder.imageView.setVisibility(View.VISIBLE);
                    Picasso.with(activity).load(url).placeholder(activity.getResources().getDrawable(R.drawable.ic_user)).into(holder.imageView);
                }
                if(position - 1 >0){
                    if (getItemViewType(position - 1) == 1){
                        holder.imageView.setVisibility(View.VISIBLE);
                        Picasso.with(activity).load(url).placeholder(activity.getResources().getDrawable(R.drawable.ic_user)).into(holder.imageView);
                    }
                    else holder.imageView.setVisibility(View.GONE);
                }
                // set time
                if(arrData.size()>0){
                    if(position<arrData.size()-1){
                        if (getItemViewType(position + 1) == 1){
                            holder.tvYouTime.setText(arrData.get(position).getTime().split(" ")[0]);
                            holder.tvYouTime.setVisibility(View.VISIBLE);
                        }
                        else holder.tvYouTime.setVisibility(View.GONE);
                    }
                    if(position == arrData.size()-1){
                        holder.tvYouTime.setText(arrData.get(position).getTime().split(" ")[0]);
                        holder.tvYouTime.setVisibility(View.VISIBLE);
                    }
                }
            }

        }
    }

    @Override
    public int getItemViewType(int position) {
        int type=0;
        String idDevice= MPrefence.getIDDevice(activity);
        if(position<arrData.size()){
            if(arrData.get(position).getIdDevice().equals(idDevice)){
                type=1;
            }
        }
        return type;

    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class MyView extends RecyclerView.ViewHolder {

       private EmojiconTextView tvMe,tvYou;
       private CircleImageView imageView;
       private TextView tvMyTime,tvYouTime;

        public MyView(View view) {
            super(view);
                tvMyTime = view.findViewById(R.id.tvMTime);
                tvYouTime = view.findViewById(R.id.tvYTime);
                tvMe = view.findViewById(R.id.txtMSend);
                tvYou=view.findViewById(R.id.txtYSend);
                imageView =  view.findViewById(R.id.img_avatar);
        }
    }


}