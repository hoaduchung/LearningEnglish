package com.eup.learningenglish.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.StudyActivity;
import com.eup.learningenglish.model.Lesson;
import com.eup.learningenglish.utils.NetWork;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by HoaDucHung on 2/6/2018.
 */

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.MyView>  {

    private List<Lesson> arrData;
    private Activity activity;

    public LessonAdapter(List<Lesson> arrData, Activity activity) {
        this.arrData=arrData;
        this.activity = activity;
    }


    @Override
    public LessonAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lesson, parent, false);
        return new LessonAdapter.MyView(itemView);
    }

    @Override
    public void onBindViewHolder(LessonAdapter.MyView holder, final int position) {
        final Lesson lesson=arrData.get(position);
        holder.tvName.setText(lesson.getName()+"");
        holder.tvWord.setText(lesson.getTotal()+"");
        if (NetWork.isNetWork(activity)) {
            String language = "";
            String prefix = String.valueOf(lesson.getId_course()).substring(0, 3);
            if (prefix.equals("101")) {
                language = "English";
            } else if (prefix.equals("102")) {
                language = "Chinese";
            } else if (prefix.equals("103")) {
                language = "Korea";
            } else if (prefix.equals("104")) {
                language = "Japanese";
            }
            String url = "http://data.minder.vn/" + language + "/" + lesson.getId_course() + "/images/lessons/" + lesson.getId() + ".jpg";
            Picasso.with(activity).load(url).placeholder(getRandom(getArrayImage())).into(holder.imgAvatar);
        } else{
            Picasso.with(activity).load(getRandom(getArrayImage())).into(holder.imgAvatar);}

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent =new Intent(activity, StudyActivity.class);
                    intent.putExtra("positionSubject",position);
                    intent.putExtra("id_course",lesson.getId_course());
                    activity.startActivity(intent);
                }
            });


    }
    private int[] getArrayImage() {
        int[] arr = new int[]{R.drawable.lesson1, R.drawable.lesson2, R.drawable.lesson3, R.drawable.lesson4};
        return arr;
    }

    private int getRandom(int[] array) {
        int rnd = new Random().nextInt(array.length);
        return array[rnd];
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class MyView extends RecyclerView.ViewHolder {

        private CircleImageView imgAvatar;
        private TextView tvName;
        private TextView tvWord;

        public MyView(View view) {
            super(view);
            imgAvatar = view.findViewById(R.id.ivLesson);
            tvName = view.findViewById(R.id.tvLessonName);
            tvWord =view.findViewById(R.id.tvInfo);
        }
    }
}

