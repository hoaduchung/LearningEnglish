package com.eup.learningenglish.adapter;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eup.learningenglish.R;
import com.eup.learningenglish.activity.MainActivity;
import com.eup.learningenglish.activity.PrepareSuggestActivity;
import com.eup.learningenglish.model.MWord;
import com.eup.learningenglish.prefence.MPrefence;
import com.eup.learningenglish.utils.Convert;
import com.eup.learningenglish.utils.NetWork;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

/**
 * Created by HoaDucHung on 2/7/2018.
 */

public class StudyAdapter extends RecyclerView.Adapter<StudyAdapter.MyView>  {
    private List<MWord> arrData;
    private Activity activity;
    private MPrefence mPrefence;
    private MediaPlayer player;
    private int id_course;

    public StudyAdapter(List<MWord> arrData,int id_course, Activity activity) {
        this.arrData=arrData;
        this.activity = activity;
        this.id_course=id_course;
    }


    @Override
    public StudyAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listword, parent, false);
        mPrefence = new MPrefence(activity,MPrefence.NAME_PREF_MAIN);
        return new StudyAdapter.MyView(itemView);
    }

    @Override
    public void onBindViewHolder(StudyAdapter.MyView holder, final int position) {
         final MWord mWordItem=arrData.get(position);
         holder.tv_word.setText(mWordItem.getWord());
         holder.tv_mean2.setText(mWordItem.getMean());
         holder.tvPhonetic.setText(mWordItem.getPhonetic());
        holder.ivSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new speakTask().execute(position);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson=new Gson();
                String a=gson.toJson(arrData);

                Intent intent = new Intent(activity, PrepareSuggestActivity.class);
                intent.putExtra("pos",position);
                intent.putExtra("listword",a);
                intent.putExtra("id_course",id_course);
                activity.startActivity(intent);


            }
        });
    }

    private class speakTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            checkSpeakOnlineOrOffline(params[0]);
            return null;
        }
    }
    private void checkSpeakOnlineOrOffline(int pos) {
        if (player == null) {
            player = new MediaPlayer();
        }
        if (player.isPlaying())
            player.stop();
        player.reset();

        if (NetWork.isNetWork(activity)) {
            try {
                String language = "";
                String prefix = String.valueOf(arrData.get(pos).getId_subject()).substring(0, 3);
                if (prefix.equals("101")) {
                    language = "English";
                } else if (prefix.equals("102")) {
                    language = "Chinese";
                } else if (prefix.equals("103")) {
                    language = "Korea";
                } else if (prefix.equals("104")) {
                    language = "Japanese";
                }
                String url = "http://data.minder.vn/" + language + "/" +id_course + "/audios/" + arrData.get(pos).getId() + ".mp3";
                player.setDataSource(url);
                player.prepareAsync();
                player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        Log.e("test","online");
                        player.start();
                    }
                });
            } catch (IOException e1) {
                tryTTS(arrData.get(pos),pos);
            }
        } else {

            tryTTS(arrData.get(pos),pos);
        }
    }

    private void tryTTS(MWord word,int index) {
        if (MainActivity.resultLanguage == TextToSpeech.LANG_MISSING_DATA
                || MainActivity.resultLanguage == TextToSpeech.LANG_NOT_SUPPORTED) {
            Log.e("test","resultLanguage :"+MainActivity.resultLanguage );
            try {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, activity.getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception e) {
            }
        } else {
            speakOut(word.getWord());

        }
    }
    private void speakOut(String txt) {
        if (txt != null && MainActivity.textToSpeech != null) {
            Log.e("test","speak");
            MainActivity.textToSpeech.speak(Convert.ignoreSpeak(txt), TextToSpeech.QUEUE_FLUSH, null);
        }
    }



    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class MyView extends RecyclerView.ViewHolder {

        private TextView tv_word,tvPhonetic,tv_mean2;
        private ImageView ivSpeak;

        public MyView(View view) {
            super(view);
            tv_word=view.findViewById(R.id.tv_word2);
            tvPhonetic=view.findViewById(R.id.tvPhonetic);
            tv_mean2 =  view.findViewById(R.id.tv_mean2);
            ivSpeak=view.findViewById(R.id.ivSpeak);
        }
    }
}