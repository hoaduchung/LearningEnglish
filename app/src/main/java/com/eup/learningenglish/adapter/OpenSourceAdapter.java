package com.eup.learningenglish.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eup.learningenglish.R;

/**
 * Created by HoaDucHung on 31/03/2018.
 */

public class OpenSourceAdapter extends RecyclerView.Adapter<OpenSourceAdapter.ViewHolder> {
    private String[] items;
    private int itemLayout;

    public OpenSourceAdapter(String[] items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final String item = items[position];
        holder.textView.setText(item);

        holder.itemView.setTag(item);
    }

    @Override
    public int getItemCount() {
        return items.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.open_source);
        }
    }
}