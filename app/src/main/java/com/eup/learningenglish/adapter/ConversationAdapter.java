package com.eup.learningenglish.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eup.learningenglish.R;
import com.eup.learningenglish.chathead.chat.ChatHeadService;
import com.eup.learningenglish.listener.OnGetIntent;
import com.eup.learningenglish.model.chat.ListConveration;
import com.eup.learningenglish.model.chat.MyConversation;
import com.eup.learningenglish.prefence.MPrefence;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by HoaDucHung on 22/04/2018.
 */

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.MyView> {

    private Context activity;
    private List<MyConversation> arrData;
    private OnGetIntent onGetIntent;
    private MPrefence mPrefence;
    private boolean check;


    public ConversationAdapter(List<MyConversation> arrData, Context activity, OnGetIntent onGetIntent, boolean check) {
        this.arrData = arrData;
        this.activity = activity;
        this.onGetIntent = onGetIntent;
        this.check = check;
    }

    @Override
    public ConversationAdapter.MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cnversation, parent, false);
        mPrefence = new MPrefence(activity, MPrefence.NAME_PREF_MAIN);
        return new ConversationAdapter.MyView(itemView);
    }


    @Override
    public void onBindViewHolder(ConversationAdapter.MyView holder, final int position) {
        final MyConversation converation = arrData.get(position);
        holder.tvName.setText(converation.getNameConversation());
        Picasso.with(activity).load(converation.getUrlImage()).placeholder(activity.getResources().getDrawable(R.drawable.ic_user)).into(holder.imageView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check) {
                    onGetIntent.getIntentCon(converation);

                } else {
                    if (ChatHeadService.onAddChatHead != null) {
                        ChatHeadService.onAddChatHead.getAddChatHead(converation.getIdConVersation(), converation.getUrlImage(),converation.getIdDevicePatner());
                    }
                }
            }
        });


    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }

    public class MyView extends RecyclerView.ViewHolder {
        private CircleImageView imageView;
        private TextView tvName;

        public MyView(View view) {
            super(view);
            imageView = view.findViewById(R.id.imgImage);
            tvName = view.findViewById(R.id.tvName);
        }
    }


}
