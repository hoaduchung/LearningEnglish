package com.eup.learningenglish.utils;

/**
 * Created by HoaDucHung on 2/1/2018.
 */

public class ApiMinder {

    public static final String URL_COURSE="http://minder.vn/api/courses/courses";
    public static final String URL_SUBJECT_OF_COURSE="http://minder.vn/api/subjects/subjects?id_course=";
    public static final String URL_WORD_OF_SUBJECT="http://minder.vn/api/words/words?id_subject=";
}
