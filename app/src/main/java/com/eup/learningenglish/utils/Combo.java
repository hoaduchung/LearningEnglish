package com.eup.learningenglish.utils;

/**
 * Created by LinhNT on 2/25/2016.
 */
public class Combo {
    public static int pointCombo(int addPoint, int combo) {
        if (combo >= 1) {
            return addPoint + (int) (addPoint * combo * 0.1);
        } else return addPoint;
    }
}
