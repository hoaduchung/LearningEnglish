package com.eup.learningenglish.utils;

import com.eup.learningenglish.firebase.module.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;


/**
 * Created by linh on 2017/07/26.
 */

public class GsonUtil {

    /**
     * convert to list temr match activity from set activity
     */
    public static final List<User> convertToListUser(String json) {
        // Now convert the JSON string back to your java object
        Type type = new TypeToken<List<User>>() {
        }.getType();

        final List<User> inpList = new Gson().fromJson(json, type);

        return inpList;
    }
}
