package com.eup.learningenglish.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.MediaRecorder;
import android.speech.RecognizerIntent;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by dovantiep on 19/02/2016.
 */
public class MediaUtil {
    // kiểm tra máy có hỗ trợ speak to text
    public static boolean speakTestAvailable(Context context) {
        return context != null && getMicrophoneExists(context)
                && getMicrophoneAvailable(context)
                && getTTSAvailable(context);
    }

    // kiểm tra máy có hỗ trợ speak to text
    public static boolean speakAvailable(Context context) {
        return context != null && getMicrophoneExists(context)
                && getMicrophoneAvailable(context);
    }


    //returns whether a microphone exists
    public static boolean getMicrophoneExists(Context context) {
        PackageManager packageManager = context.getPackageManager();
        return packageManager.hasSystemFeature(PackageManager.FEATURE_MICROPHONE);
    }

    //returns whether the microphone is available
    public static boolean getMicrophoneAvailable(Context context) {
        MediaRecorder recorder = new MediaRecorder();
        try {
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        } catch (Exception e) {
            return false;
        }
        recorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        recorder.setOutputFile(new File(context.getCacheDir(), "MediaUtil#micAvailTestFile").getAbsolutePath());
        boolean available = true;
        try {
            recorder.prepare();
        } catch (IOException exception) {
            available = false;
        }
        recorder.release();
        return available;
    }

    //returns whether text to speech is available
    public static boolean getTTSAvailable(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent speechIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        List<ResolveInfo> speechActivities = packageManager.queryIntentActivities(speechIntent, 0);
        if (speechActivities.size() != 0) return true;
        return false;
    }
}
