package com.eup.learningenglish.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import com.eup.learningenglish.R;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by HoaDucHung on 2/1/2018.
 */

public class Avatar {

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    // tao list anh
    public static int getIDImage(String id) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("101000001", R.drawable.a101000001);
        hashMap.put("101000002", R.drawable.a101000002);
        hashMap.put("101000003", R.drawable.a101000003);
        hashMap.put("101000004", R.drawable.a101000004);
        hashMap.put("101000005", R.drawable.a101000005);
        hashMap.put("101000007", R.drawable.a101000007);
        hashMap.put("101000008", R.drawable.a101000008);
        hashMap.put("101000009", R.drawable.a101000009);
        hashMap.put("101000010", R.drawable.a101000010);
        hashMap.put("101000012", R.drawable.a101000012);
        hashMap.put("101000013", R.drawable.a101000013);
        hashMap.put("101000014", R.drawable.a101000014);
        hashMap.put("101000015", R.drawable.a101000015);
        hashMap.put("101000016", R.drawable.a101000016);
        hashMap.put("101000017", R.drawable.a101000017);
        hashMap.put("101000018", R.drawable.a101000018);
        hashMap.put("101000019", R.drawable.a101000019);

        hashMap.put("102000001", R.drawable.a102000001);
        hashMap.put("102000002", R.drawable.a102000002);
        hashMap.put("102000003", R.drawable.a102000003);
        hashMap.put("102000005", R.drawable.a102000005);
        hashMap.put("102000008", R.drawable.a102000008);
        hashMap.put("102000009", R.drawable.a102000009);
        hashMap.put("102000010", R.drawable.a102000010);
        hashMap.put("102000011", R.drawable.a102000011);
        hashMap.put("102000012", R.drawable.a102000012);
        hashMap.put("102000013", R.drawable.a102000013);

        hashMap.put("103000001", R.drawable.a103000001);
        hashMap.put("103000002", R.drawable.a103000002);
        hashMap.put("103000003", R.drawable.a103000003);
        hashMap.put("103000004", R.drawable.a103000004);
        hashMap.put("103000005", R.drawable.a103000005);
        hashMap.put("103000006", R.drawable.a103000006);
        hashMap.put("103000009", R.drawable.a103000009);
        hashMap.put("103000010", R.drawable.a103000010);
        hashMap.put("103000011", R.drawable.a103000011);
        hashMap.put("103000012", R.drawable.a103000012);
        hashMap.put("103000013", R.drawable.a103000013);
        hashMap.put("103000021", R.drawable.a103000021);

        hashMap.put("104000001", R.drawable.a104000001);
        hashMap.put("104000002", R.drawable.a104000002);
        hashMap.put("104000003", R.drawable.a104000003);
        hashMap.put("104000004", R.drawable.a104000004);
        hashMap.put("104000005", R.drawable.a104000005);
        hashMap.put("104000006", R.drawable.a104000006);
        hashMap.put("104000007", R.drawable.a104000007);
        hashMap.put("104000008", R.drawable.a104000008);
        hashMap.put("104000009", R.drawable.a104000009);
        hashMap.put("104000011", R.drawable.a104000011);
        hashMap.put("104000014", R.drawable.a104000014);
        hashMap.put("104000015", R.drawable.a104000015);
        hashMap.put("104000016", R.drawable.a104000016);

        if (hashMap.get(id) != null) {
            return hashMap.get(id);
        } else {
            switch (id.substring(0, 3)) {
                case "101":
                    return getRandom(getArrayImage1(), new Random().nextInt(16));
                case "102":
                    return getRandom(getArrayImage2(), new Random().nextInt(9));
                case "103":
                    return getRandom(getArrayImage3(), new Random().nextInt(11));
                case "104":
                    return getRandom(getArrayImage4(), new Random().nextInt(12));
                default:
                    return getRandom(getArrayImage1(), new Random().nextInt(16));
            }
        }
    }

    private static int getRandom(int[] array, int tab) {
        return array[tab];
    }

    private static int[] getArrayImage1() {
        int[] arr = new int[]{  R.drawable.a101000001, R.drawable.a101000002, R.drawable.a101000003, R.drawable.a101000004,
                R.drawable.a101000005, R.drawable.a101000007, R.drawable.a101000008, R.drawable.a101000009,
                R.drawable.a101000010, R.drawable.a101000012, R.drawable.a101000013, R.drawable.a101000014,
                R.drawable.a101000015, R.drawable.a101000016, R.drawable.a101000017, R.drawable.a101000018, R.drawable.a101000019};
        return arr;
    }
    private static int[] getArrayImage2() {
        int[] arr = new int[]{ R.drawable.a102000001, R.drawable.a102000002, R.drawable.a102000003, R.drawable.a102000005,
                R.drawable.a102000008, R.drawable.a102000009, R.drawable.a102000010, R.drawable.a102000011, R.drawable.a102000012,
                R.drawable.a102000013};
        return arr;
    }
    private static int[] getArrayImage3() {
        int[] arr = new int[]{ R.drawable.a103000001, R.drawable.a103000002, R.drawable.a103000003, R.drawable.a103000004,
                R.drawable.a103000005, R.drawable.a103000006, R.drawable.a103000009, R.drawable.a103000010, R.drawable.a103000011,
                R.drawable.a103000012, R.drawable.a103000013, R.drawable.a103000021};
        return arr;
    }
    private static int[] getArrayImage4() {
        int[] arr = new int[]{ R.drawable.a104000001, R.drawable.a104000002, R.drawable.a104000003,
                R.drawable.a104000004, R.drawable.a104000005, R.drawable.a104000006, R.drawable.a104000007, R.drawable.a104000008,
                R.drawable.a104000009, R.drawable.a104000011, R.drawable.a104000014, R.drawable.a104000015, R.drawable.a104000016};
        return arr;
    }
}
