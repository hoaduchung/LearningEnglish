package com.eup.learningenglish.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.preference.PreferenceManager;


import com.eup.learningenglish.R;

import java.util.Random;



/**
 * Created by LinhNT on 3/15/2016.
 */
public class SoundPoolUtil {
    private static MediaPlayer player;
    private static SoundPool soundPool;
    private static AudioManager audioManager;
    private static boolean isLoaded;
    private static int soundIds[] = new int[9];
    private static final String key_amthanh1 = "pref_amthanh1",
            key_amthanh2 = "pref_amthanh2";

    public static void playSoundAnswer(boolean isCorrect) {
        if (isCorrect) {
//                creatSoundEF(activity, R.raw.answer_right);
            playSoundPool(0);
        } else {
//                creatSoundEF(activity, R.raw.answer_wrong);
            playSoundPool(1);
        }
    }

    public static void playSoundResult(boolean isCorrect) {

        if (isCorrect) {
//                creatSoundEF(activity, R.raw.victory);
            playSoundPool(6);
        } else {
//                creatSoundEF(activity, R.raw.defeat);
            playSoundPool(3);
        }
    }

    public static void playSoundTickTock(boolean isTick) {

        if (isTick) {
//                creatSoundEF(activity, R.raw.victory);
            playSoundPool(7);
        } else {
//                creatSoundEF(activity, R.raw.defeat);
            playSoundPool(8);
        }
    }

    public static void creatSoundEF(Activity activity) {
        // media player
//        mp = MediaPlayer.create(activity, source);
//        mp.start();
//        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            public void onCompletion(MediaPlayer mp) {
//                mp.release();
//            }
//        });

        // sound pool
        // Do something for lollipop and above versions
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes attrs = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            soundPool = new SoundPool.Builder()
                    .setMaxStreams(9)
                    .setAudioAttributes(attrs)
                    .build();

        } else {
            // do something for phones running an SDK before lollipop
            soundPool = new SoundPool(7, AudioManager.STREAM_MUSIC, 0);
        }

        // AudioManager audio settings for adjusting the volume
        audioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        // listener
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                isLoaded = true;
            }
        });

        soundIds[0] = soundPool.load(activity, R.raw.answer_right, 1);
        soundIds[1] = soundPool.load(activity, R.raw.answer_wrong, 2);
        soundIds[2] = soundPool.load(activity, R.raw.blop, 3);
        soundIds[3] = soundPool.load(activity, R.raw.defeat, 4);
        soundIds[4] = soundPool.load(activity, R.raw.keyboard, 5);
        soundIds[5] = soundPool.load(activity, R.raw.ting, 6);
        soundIds[6] = soundPool.load(activity, R.raw.victory, 7);

        soundIds[7] = soundPool.load(activity, R.raw.tick, 8);
        soundIds[8] = soundPool.load(activity, R.raw.start, 9);
        //rest of sounds goes here
    }

    private static void playSoundPool(int pos) {
        try {
            float curVol = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            float maxVol = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            float volume = curVol / maxVol;
            if (isLoaded) {
                soundPool.play(soundIds[pos], volume, volume, 1, 0, 1f);
            }
        } catch (Exception e) {
            if (isLoaded) {
                soundPool.play(soundIds[pos], 0.5f, 0.5f, 1, 0, 1f);
            }
        }
    }

    public static void releaseSoundPool() {
        if (soundPool != null)
            soundPool.release();
    }

    public static void playMusicWaiting(Activity activity) {

        if (player != null && player.isPlaying()) {
            player.stop();
            player.release();
        }
        player = MediaPlayer.create(activity, R.raw.waiting);
        player.setLooping(true); // Set looping
        player.setVolume(0.5f, 0.5f);
        player.start();

    }

    public static void pauseMusicGame() {
        if (player != null && player.isPlaying())
            player.stop();
    }


    public static void playSound(Activity activity, boolean isCorrect) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        if (prefs != null && prefs.getBoolean(key_amthanh1, true))
            if (isCorrect) {
//                creatSoundEF(activity, R.raw.answer_right);
                playSoundPool(0);
            } else {
//                creatSoundEF(activity, R.raw.answer_wrong);
                playSoundPool(1);
            }
    }

    public static void playSoundResult(Activity activity, boolean isCorrect) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        if (prefs != null && prefs.getBoolean(key_amthanh1, true))
            if (isCorrect) {
//                creatSoundEF(activity, R.raw.victory);
                playSoundPool(6);
            } else {
//                creatSoundEF(activity, R.raw.defeat);
                playSoundPool(3);
            }
    }

    public static void playSoundLetter(Activity activity, boolean isCorrect) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        if (prefs != null && prefs.getBoolean(key_amthanh1, true)) if (isCorrect) {
//                creatSoundEF(activity, R.raw.ting);
            playSoundPool(5);
        } else {
//                creatSoundEF(activity, R.raw.blop);
            playSoundPool(2);
        }
    }

    public static void playSoundKeyboard(Activity activity) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        if (prefs != null && prefs.getBoolean(key_amthanh1, true)) {
            // creatSoundEF(activity, R.raw.keyboard);
            playSoundPool(4);
        }
    }

    public static void playMusicGame(Activity activity) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        if (prefs != null && prefs.getBoolean(key_amthanh2, true)) {
            int[] arrMusic = new int[]{R.raw.game_sound1, R.raw.game_sound2, R.raw.game_sound3, R.raw.game_sound4, R.raw.game_sound5, R.raw.game_sound6};
            Random random = new Random();
            int pos = random.nextInt(arrMusic.length);
            if (player != null && player.isPlaying()) {
                player.stop();
                player.release();
            }
            player = MediaPlayer.create(activity, arrMusic[pos]);
            player.setLooping(true); // Set looping
            player.setVolume(0.1f, 0.1f);
            player.start();
        }
    }
}
