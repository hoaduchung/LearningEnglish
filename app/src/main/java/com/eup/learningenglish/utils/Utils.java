package com.eup.learningenglish.utils;

import android.content.res.Resources;

import com.eup.learningenglish.firebase.module.User;

import java.util.Comparator;


/**
 * Created by bruce on 14-11-6.
 */
public final class Utils {

    public static final String SEND_ID_GAME_CONTENT = "SEND_ID_GAME_CONTENT";
    public static final String SEND_JSON = "SEND_JSON";

    private Utils() {
    }
    
    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return  dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp){
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

    /**
     * custom comparetor
     */
    public static class ResultArenaComparator implements Comparator<User> {
        @Override
        public int compare(User o1, User o2) {
            return o2.score - o1.score;
        }
    }
}