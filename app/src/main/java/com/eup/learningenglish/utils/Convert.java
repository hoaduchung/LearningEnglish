package com.eup.learningenglish.utils;

import android.text.Html;
import android.text.TextUtils;
import android.widget.TextView;

/**
 * Created by HoaDucHung on 2/7/2018.
 */

public class Convert {// tính % trả lời đúng
    public static float calculataionPercent(String word, String dataWrite) {
        int lengthMax = word.length() > dataWrite.length() ? word.length() : dataWrite.length();
        int distance = Convert.minDistance(word, dataWrite);
        float percent = (lengthMax - distance) * 100.0f / lengthMax;
        return percent < 0 ? 0 : percent;
    }

    // tính khoảng cách Levenshtein
    public static int minDistance(String word1, String word2) {
        if (TextUtils.isEmpty(word1) || TextUtils.isEmpty(word2)) {
            return 0;
        }

        int len1 = word1.length();
        int len2 = word2.length();

        int[][] dp = new int[len1 + 1][len2 + 1];

        for (int i = 0; i <= len1; i++) {
            dp[i][0] = i;
        }

        for (int j = 0; j <= len2; j++) {
            dp[0][j] = j;
        }

        for (int i = 0; i < len1; i++) {
            char c1 = word1.charAt(i);
            for (int j = 0; j < len2; j++) {
                char c2 = word2.charAt(j);

                if (c1 == c2) {
                    dp[i + 1][j + 1] = dp[i][j];
                } else {
                    int replace = dp[i][j] + 1;
                    int insert = dp[i][j + 1] + 1;
                    int delete = dp[i + 1][j] + 1;

                    int min = replace > insert ? insert : replace;
                    min = delete > min ? min : delete;
                    dp[i + 1][j + 1] = min;
                }
            }
        }

        return dp[len1][len2];
    }

    public static String subString(String s) {
        while (s.indexOf("(") != -1 && s.indexOf(")") != -1) {
            int startIndex = s.indexOf("(");
            int endIndex = s.indexOf(")");
            String replacement = "";
            String toBeReplaced = s.substring(startIndex, endIndex + 1);
            s = s.replace(toBeReplaced, replacement);
        }
        while (s.indexOf("[") != -1 && s.indexOf("]") != -1) {
            int startIndex1 = s.indexOf("[");
            int endIndex1 = s.indexOf("]");
            String replacement = "";
            String toBeReplaced = s.substring(startIndex1, endIndex1 + 1);
            s = s.replace(toBeReplaced, replacement);
        }
        while (s.indexOf("「") != -1 && s.indexOf("」") != -1) {
            int startIndex2 = s.indexOf("「");
            int endIndex2 = s.indexOf("」");
            String replacement = "";
            String toBeReplaced = s.substring(startIndex2, endIndex2 + 1);
            s = s.replace(toBeReplaced, replacement);
        }
        return s.trim();
    }

    public static String subStringNonVerb(String s) {
        if (s == null) return "";
        while (s.indexOf("(") != -1 && s.indexOf(")") != -1) {
            int startIndex = s.indexOf("(");
            int endIndex = s.indexOf(")");
            String replacement = "";
            String toBeReplaced = s.substring(startIndex, endIndex + 1);
            s = s.replace(toBeReplaced, replacement);
        }
        while (s.indexOf("[") != -1 && s.indexOf("]") != -1) {
            int startIndex1 = s.indexOf("[");
            int endIndex1 = s.indexOf("]");
            String replacement = "";
            String toBeReplaced = s.substring(startIndex1, endIndex1 + 1);
            s = s.replace(toBeReplaced, replacement);
        }
        while (s.indexOf("「") != -1 && s.indexOf("」") != -1) {
            int startIndex2 = s.indexOf("「");
            int endIndex2 = s.indexOf("」");
            String replacement = "";
            String toBeReplaced = s.substring(startIndex2, endIndex2 + 1);
            s = s.replace(toBeReplaced, replacement);
        }
        if (s.indexOf(",") != -1) {
            s = s.substring(0, s.indexOf(","));
        } else if (s.indexOf(" ") != -1) {
            s = s.substring(0, s.indexOf(" "));
        }
        return s.trim();
    }

    public static void compareSpeak(TextView tvMean, String s) {
        String s1 = tvMean.getText().toString();
        String result = "";
        if (s.isEmpty() || s1.isEmpty()) return;
        s = s.substring(0, 1).toUpperCase() + s.substring(1);
        String[] arr = s.trim().split(" ");
        String[] arr1 = s1.trim().split(" ");

        for (String str : arr1) {
            boolean isMatch = false;
            for (String str1 : arr) {
                if (str.equals(str1)) {
                    isMatch = true;
                }
            }
            if (result.equals("") && str.length() > 0) {
                str = str.substring(0, 1).toUpperCase() + str.substring(1);
            }
            if (isMatch) {
                result += "<font color='green'>" + str + "</font> ";
            } else result += "<font color='red'>" + str + "</font> ";
        }
        tvMean.setText(Html.fromHtml(result), TextView.BufferType.SPANNABLE);
    }

    public static String ignore(String word) {
        if (word != null) {
            word = word.replaceAll(",", "");
            word = word.replaceAll(", ", "");
            word = word.replaceAll("\"", "");
            word = word.replaceAll("（", "");
            word = word.replaceAll("）", "");
            word = word.replaceAll("「", "");
            word = word.replaceAll("」", "");
            word = word.replaceAll("、", "");
            word = word.replaceAll("。", "");
            word = word.replaceAll("~", "");
            word = word.replaceAll("〜", "");
            word = word.replaceAll("～", "");
            word = word.replaceAll("\\?", "");
        }
        return word;
    }

    public static String ignoreSpeak(String word) {
        if (word != null) {
            word = word.replaceAll("~", "");
            word = word.replaceAll("〜", "");
            word = word.replaceAll("～", "");
        }
        return word;
    }
}

