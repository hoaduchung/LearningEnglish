package com.eup.learningenglish.utils;

import com.eup.learningenglish.model.Course;
import com.eup.learningenglish.model.Lesson;
import com.eup.learningenglish.model.MWord;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by HoaDucHung on 2/1/2018.
 */

public class OkHttpUtils {
    private static final OkHttpClient client = new OkHttpClient();
    /*
    * get course fisrt login*/

    public static List<Course> getCourseFirst(String url) throws IOException, JSONException {
        List<Course> list = new ArrayList<>();

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        String result = response.body().string();
        // create json object
        JSONObject object = new JSONObject(result);

        // kiem tra neu co loi thi tra ve loi
        if (object.has("Courses")) {
                list = new Gson().fromJson(object.getString("Courses"), new TypeToken<List<Course>>() {
                }.getType());
        }
        return list;
    }

    // lấy tất cả subject trong 1 khóa học
    public static List<Lesson> getSujectOfCourse(String url,int id_course) throws IOException, JSONException {
        List<Lesson> list = new ArrayList<>();

        String urlSubject=url+id_course;
        Request request = new Request.Builder()
                .url(urlSubject)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        String result = response.body().string();

        // create json object
        JSONObject object = new JSONObject(result);

        // kiem tra neu co loi thi tra ve loi
        if (object.has("Subjects")) {
            list = new Gson().fromJson(object.getString("Subjects"), new TypeToken<List<Lesson>>() {
            }.getType());
        }
        return list;
    }

    // lấy tất cả từ trong 1 subject
    public static List<MWord> getWordOfSuject(String url, int id_subject) throws IOException, JSONException {
        List<MWord> list = new ArrayList<>();

        String urlSubject=url+id_subject;
        Request request = new Request.Builder()
                .url(urlSubject)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        String result = response.body().string();

        // create json object
        JSONObject object = new JSONObject(result);

        // kiem tra neu co loi thi tra ve loi
        if (object.has("Words")) {
            list = new Gson().fromJson(object.getString("Words"), new TypeToken<List<MWord>>() {
            }.getType());
        }
        return list;
    }


}
